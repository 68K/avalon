# Source included by `require` in main.rb is loaded async to starting the game, 
# and the main game loop may start before code has finished loading!
# Avoid this by requiring this file last - this file sets a flag that informs the main
# game that loading is complete
$gtk.args.state.requires_loaded = true 