# Class for an enemy party with 3 ranks
class EnemyParty

    attr_accessor :front_rank, :middle_rank, :rear_rank

    def initialize
        @front_rank = []
        @middle_rank = []
        @rear_rank = []
    end

    def defeated?
        @front_rank.select { |a| a.alive }.empty? &&
        @middle_rank.select { |a| a.alive }.empty? &&
        @rear_rank.select { |a| a.alive }.empty?
    end

    # Return the rank given the symbol that IDs the rank
    def get_rank r
        raise "Rank must be :front :middle :rear" unless [:front, :middle, :rear].include? r
        case r
        when :front
            @front_rank
        when :middle
            @middle_rank
        when :rear
            @rear_rank
        end
    end

    def add_enemy creature, rank = :front
        raise "Can only add Creature to EnemyParty" unless creature.is_a? Creature
        raise "Rank must be :front :middle :rear" unless [:front, :middle, :rear].include? rank
        chosen_rank = case rank
        when :front
            @front_rank
        when :middle
            @middle_rank
        when :rear
            @rear_rank
        end
        raise "Only 3 creatures per rank" unless chosen_rank.length <= 3
        chosen_rank << creature 
    end

    def update_ranks
        return if defeated?
        if @front_rank.select { |a| a.alive }.empty? && @middle_rank.select { |a| a.alive }.any?
            @front_rank = @middle_rank
            @middle_rank = []
        end
        if @middle_rank.select { |a| a.alive }.empty? && @rear_rank.select { |a| a.alive }.any?
            @middle_rank = @rear_rank
            @rear_rank = []
        end
    end

    def first_alive_in_rank rank
        raise "Rank must be :front :middle :rear" unless [:front, :middle, :rear].include? rank
        r = case rank
        when :front
            @front_rank
        when :middle
            @middle_rank
        when :rear
            @rear_rank
        end
        c = r.select { |e| e.alive }.first
        raise "No enemy in rank :#{rank} alive" unless c
        r.find_index(c)
    end

    def rank_and_index_of enemy
        return [:front, @front_rank.index(enemy)] if @front_rank.include? enemy
        return [:middle, @middle_rank.index(enemy)] if @middle_rank.include? enemy
        return [:rear, @rear_rank.index(enemy)] if @rear_rank.include? enemy
        raise "Enemy #{enemy} not found in any rank of the enemy party"
    end

    def all_enemies
        return @front_rank + @middle_rank + @rear_rank
    end

    def screen_position index, rank = :front
        raise "Rank must be :front :middle :rear (not #{rank})" unless [:front, :middle, :rear].include? rank
        case rank
        when :rear
            [(index+1) * (700 / (rear_rank.count + 1)) + 50, 240]
        when :middle
            [(index+1) * (850 / (middle_rank.count + 1)) - 60, 190]
        when :front
            [(index+1) * (1200 / (front_rank.count + 1)) - 256, 160]
        end
    end
end