require "app/dungeon_map_generator.rb"
require "app/dungeon_map.rb"

# Class representing an entire dungeon with several floors
class Dungeon
    attr_reader :id, :level, :style, :title

    def initialize(args, dungeon_generator, opts = {})
        raise "dungeon_generator must be a DungeonMapGenerator" unless (dungeon_generator && dungeon_generator.is_a?(DungeonMapGenerator))
        @id = opts[:id] || -1
        @style = opts[:style] || :underhalls
        @title = opts[:title] || "Where am I?!"
        @depth = opts[:depth] || 1
        @level = opts[:level] || 5
        @floors = []
        @current_floor = opts[:starting_floor] || 0
        @dungeon_generator = dungeon_generator
        @floors_generated = 0
        @generate_delay = 10 # Generate delaying for 10 ticks to give the engine a tick to stop music and display loading screen.
        # Set the dungeon generator seed to something deterministic for this dungeon
        @dungeon_generator.reset_seed(@id)
    end

    # Static method to select a random style, ahead of initializing a Dungeon
    # @return Symbol of a dungeon style
    def self.random_style
        # pick from currently available dungeons so a style later in the main questline isn't spoiled
        $gtk.args.state.dungeons.select { |d| d[:available] }.map { |d| d[:style] }.uniq.sample
    end

    # Static method to generate a random dungeon name matching the style
    # TODO: this should return a variety of names
    # @return String a dungeon name
    def self.random_dungeon_name style
        res = ""
        case style
        when :underhalls
            res = "#{$gtk.args.state.name_generator.common_name} #{%w[Tunnels Undercroft].sample}"
        when :catacombs
            res = "#{$gtk.args.state.name_generator.basic_name} #{%w[Barrow Crypt Catacombs].sample}"
        when :ice
            res = "#{$gtk.args.state.name_generator.common_name} #{%w[Icefields Tundra Glacier].sample}"
        when :citadel
            res = "#{%w[Castle Fort Redoubt].sample} #{$gtk.args.state.name_generator.common_name}"
        when :crystal
            res = "#{$gtk.args.state.name_generator.basic_name} Experiment "+ (1 + rand(9)).to_s + (rand(9)).to_s + (rand(9)).to_s
        else
            raise "Unknown style :#{style} for random_dungeon_name"
        end
        res
    end

    # @return DungeonMap for floor player is currently on
    def current_floor
        @floors[@current_floor]
    end

    # @return 1-indexed number of floor player is currently on
    def current_floor_number
        @current_floor + 1
    end

    def descend a
        raise "Attempt to descend from lowest floor!" if @current_floor == (@depth - 1)
        @current_floor += 1
        # Always face away from the stairs you came down
        @floors[@current_floor].flip_direction if @floors[@current_floor].stairs_up_ahead?
        update_quest_pois_on_floor(a, @current_floor)
        @floors[@current_floor].update_render a
        # Create a new floor on the player map unless it already exists
        a.state.player_maps.new_dungeon_floor(@id, @current_floor, @floors[@current_floor].width, @floors[@current_floor].height) unless a.state.player_maps.has_dungeon_floor?(@id, @current_floor)
        # Initial map update/render (so it doesn't need the player to take a step for the map to be rendered)
        update_player_map a
    end

    def ascend a
        raise "Attempt to ascend from the highest floor!" if @current_floor == 0
        @current_floor -= 1
        # Always face away from the stairs you came up
        @floors[@current_floor].flip_direction if @floors[@current_floor].stairs_down_ahead?
        update_quest_pois_on_floor(a, @current_floor)
        @floors[@current_floor].update_render a
        # Create a new floor on the player map unless it already exists
        a.state.player_maps.new_dungeon_floor(@id, @current_floor, @floors[@current_floor].width, @floors[@current_floor].height) unless a.state.player_maps.has_dungeon_floor?(@id, @current_floor)
        # Initial map update/render (so it doesn't need the player to take a step for the map to be rendered)
        update_player_map a
    end

    def update_quest_pois_on_floor a, floor_number
        return unless quest_goal = a.state.quests.current_goal_for_dungeon_floor(self, :reach_poi)
        return if @floors[floor_number].points_of_interest.select { |p| p.type == :quest && p.quest_id == quest_goal[:quest_id] && p.goal_id == quest_goal[:goal_id] }.any?
        raise "Floor #{floor_number} has no empty POIs to replace with quest POI" unless empty_poi = @floors[floor_number].points_of_interest.select { |p| p.type == :empty }.first
        empty_poi.type = :quest
        empty_poi.quest_id = quest_goal[:quest_id]
        empty_poi.goal_id = quest_goal[:goal_id]
    end

    def ready?
        @floors_generated == @depth
    end

    # Update player map
    # The map is rendered to 2 render targets to allow the compass to be rendered in between them:
    # :player_map_lower has the basic floor layout
    # :player_map_upper has the highlights - walls, exits and points of interest
    def update_player_map a
        player_map = a.state.player_maps.get_dungeon_floor_map @id, @current_floor
        tiles = @floors[@current_floor].tiles
        px = @floors[@current_floor].player_x
        py = @floors[@current_floor].player_y

        @floors[@current_floor].points_of_interest.each do |poi|
            if poi[:x] == px && poi[:y] == py
                case poi[:type]
                when :chest
                    unless a.state.opened_chests.find { |c| c[:x] == poi[:x] && c[:y] == poi[:y] && c[:dungeon] == a.state.dungeon.id && c[:floor] == a.state.dungeon.current_floor_number }
                        player_map[:tiles][py][px] = player_map[:tiles][py][px] | 16 # Closed chest
                        player_map[:tiles][py][px] -= 64 if player_map[:tiles][py][px] & 64 == 64 # Unset the unknown POI bit since this tile is now known
                    else # It's an opened chest, unset the closed chest bit
                        player_map[:tiles][py][px] = player_map[:tiles][py][px] - 16 if player_map[:tiles][py][px] & 16 == 16
                    end
                when :quest
                    player_map[:tiles][py][px] = player_map[:tiles][py][px] | 32 # Quest POI
                    player_map[:tiles][py][px] -= 64 if player_map[:tiles][py][px] & 64 == 64 # Unset the unknown POI bit since this tile is now known
                end
            else
                # If it is a known chest that is now opened, unset the closed chest bit
                # Opened chests should not be marked
                if poi[:type] == :chest && player_map[:tiles][poi[:y]][poi[:x]] & 16 == 16
                    if a.state.opened_chests.find { |c| c[:x] == poi[:x] && c[:y] == poi[:y] && c[:dungeon] == a.state.dungeon.id && c[:floor] == a.state.dungeon.current_floor_number }
                    player_map[:tiles][poi[:y]][poi[:x]] -= 16
                    end
                end
                
                # If the player map tile does not yet have any POI bit for this POI then it is "undiscovered"
                # Consider closed chests and quest POIs, and only set the "undiscovered POI" bit if there is no chest, quest or unknown POI bit already
                # TODO: Revealing POIs like this should be based on party skill
                if [:chest, :quest].include?(poi[:type]) && (player_map[:tiles][poi[:y]][poi[:x]] & (16 + 32 + 64) == 0)
                    unless poi[:type] == :chest && a.state.opened_chests.find { |c| c[:x] == poi[:x] && c[:y] == poi[:y] && c[:dungeon] == a.state.dungeon.id && c[:floor] == a.state.dungeon.current_floor_number }
                        player_map[:tiles][poi[:y]][poi[:x]] = player_map[:tiles][poi[:y]][poi[:x]] | 64 # Unknown POI
                    end
                end
            end
        end

        # If the tile the player now stands on has bit 128 (unknown) update it to the map tile
        if player_map[:tiles][py][px] & 128 == 128
            # Remove the unknown bit
            player_map[:tiles][py][px] -= 128
            # Add the tile bits
            player_map[:tiles][py][px] = player_map[:tiles][py][px] | tiles[py][px]
        end
        
        # If the tile the player now stands on has doorways, add those doorways to the map
        furniture = @floors[@current_floor].furniture[py][px]
        furniture.each_with_index do |f,i|
            case f
            when :doorway_outside
                player_map[:exit_x] = px
                player_map[:exit_y] = py
                player_map[:exit_dir] = i
            when :doorway_stairs_down
                player_map[:stairs_down_x] = px
                player_map[:stairs_down_y] = py
                player_map[:stairs_down_dir] = i
            when :doorway_stairs_up
                player_map[:stairs_up_x] = px
                player_map[:stairs_up_y] = py
                player_map[:stairs_up_dir] = i
            end
        end

        # TODO: Reveal more tiles in a radius
        # TODO: Reveal nearby POIs on the map

        # Update the RT for the player map
        # lower_prims is filled with the colour for "undiscovered" tiles
        lower_prims = [{ x: 0, y: 0, w: 100, h: 100, r: 20, g: 20, b: 100, a: 255, primitive_marker: :solid }]
        # upper_prims is empty
        upper_prims = []
        player_indicator_prim = nil
        left = [1, px - 11].max
        right = [@floors[@current_floor].width - 2, left + 22].min
        top = [1, py - 11].max
        bottom = [@floors[@current_floor].height - 2, top + 22].min
        # Store exit and stair primitives so they can be rendered on top of walls
        exit_prim = nil
        stairs_down_prim = nil
        stairs_up_prim = nil
        ts = 5
        poi_size = ts - 2
        (top..bottom).each do |y|
            (left..right).each do |x|
                t = player_map[:tiles][y][x]
                if t & 128 == 128
                    lower_prims << { x: (x - left) * ts + 1, y: 100 - (y - top + 1) * ts, w: ts, h: ts, path: :pixel, r: 20, g: 20, b: 100, primitive_marker: :sprite }
                    # Undiscovered POI
                    upper_prims << { x: (x - left) * ts + 2, y: 100 - (y - top + 1) * ts + 1, w: poi_size, h: poi_size, path: :pixel, r: 200, g: 200, b: 200, primitive_marker: :sprite } if t & 64 == 64
                    next
                else
                    lower_prims << { x: (x - left) * ts + 1, y: 100 - (y - top + 1) * ts, w: ts, h: ts, path: :pixel, r: 20, g: 20, b: 20, primitive_marker: :sprite }
                end
                # North wall
                upper_prims << { x: (x - left) * ts + 1, y: 100 - (y - top + 1) * ts + 5, w: ts, h: 1, path: :pixel, r: 20, g: 20, b: 255, primitive_marker: :sprite } if t & 1 == 1
                # West wall
                upper_prims << { x: (x - left) * ts, y: 100 - (y - top + 1) * ts, w: 1, h: ts, path: :pixel, r: 20, g: 20, b: 255, primitive_marker: :sprite } if t & 2 == 2
                # South wall
                upper_prims << { x: (x - left) * ts + 1, y: 100 - (y - top + 1) * ts, w: ts, h: 1, path: :pixel, r: 20, g: 20, b: 255, primitive_marker: :sprite } if t & 4 == 4
                # East wall
                upper_prims << { x: (x - left) * ts + 4 + 1, y: 100 - (y - top + 1) * ts, w: 1, h: ts, path: :pixel, r: 20, g: 20, b: 255, primitive_marker: :sprite } if t & 8 == 8
                # Closed chest
                upper_prims << { x: (x - left) * ts + 2, y: 100 - (y - top + 1) * ts + 1, w: poi_size, h: poi_size, path: "sprites/tile/chest.png", primitive_marker: :sprite } if t & 16 == 16
                # Quest POI
                upper_prims << { x: (x - left) * ts + 2, y: 100 - (y - top + 1) * ts + 1, w: poi_size, h: poi_size, path: :pixel, r: 220, g: 220, b: 5, primitive_marker: :sprite } if t & 32 == 32
                # Undiscovered POI
                # TODO: Is this redundant since undiscovered tiles were handled earlier?
                upper_prims << { x: (x - left) * ts + 2, y: 100 - (y - top + 1) * ts + 1, w: 2, h: 2, path: :pixel, r: 200, g: 200, b: 200, primitive_marker: :sprite } if t & 64 == 64
                # Exits and stairs
                if player_map[:exit_x] == x && player_map[:exit_y] == y
                    ex = (x - left) * ts + 2
                    ey = 100 - (y - top + 1) * ts + 4
                    ew = 2
                    eh = 1
                    case player_map[:exit_dir]
                    when 0 # NORTH
                        # Use defaults
                    when 1 # EAST
                        ex = (x - left) * ts + 4
                        ey = 100 - (y - top + 1) * ts + 2
                        ew = 1
                        eh = 2
                    when 2 # SOUTH
                        ex = (x - left) * ts + 1
                        ey = 100 - (y - top + 1) * ts + 1
                    when 3 # WEST
                        ex = (x - left) * ts + 1
                        ey = 100 - (y - top + 1) * ts + 2
                        ew = 1
                        eh = 2
                    end
                    exit_prim = { x: ex, y: ey, w: ew, h: eh, path: :pixel, r: 220, g: 220, b: 255, primitive_marker: :sprite }
                end
                if player_map[:stairs_up_x] == x && player_map[:stairs_up_y] == y
                    ex = (x - left) * ts + 2
                    ey = 100 - (y - top + 1) * ts + 4
                    ew = 2
                    eh = 1
                    case player_map[:stairs_up_dir]
                    when 0 # NORTH
                        # Use defaults
                    when 1 # EAST
                        ex = (x - left) * ts + 4
                        ey = 100 - (y - top + 1) * ts + 2
                        ew = 1
                        eh = 2
                    when 2 # SOUTH
                        ex = (x - left) * ts + 1
                        ey = 100 - (y - top + 1) * ts + 1
                    when 3 # WEST
                        ex = (x - left) * ts + 1
                        ey = 100 - (y - top + 1) * ts + 2
                        ew = 1
                        eh = 2
                    end
                    stairs_up_prim = { x: ex, y: ey, w: ew, h: eh, path: :pixel, r: 220, g: 30, b: 30, primitive_marker: :sprite }
                end
                if player_map[:stairs_down_x] == x && player_map[:stairs_down_y] == y
                    ex = (x - left) * ts + 2
                    ey = 100 - (y - top + 1) * ts + 4
                    ew = 2
                    eh = 1
                    case player_map[:stairs_down_dir]
                    when 0 # NORTH
                        # Use defaults
                    when 1 # EAST
                        ex = (x - left) * ts + 4
                        ey = 100 - (y - top + 1) * ts + 2
                        ew = 1
                        eh = 2
                    when 2 # SOUTH
                        ex = (x - left) * ts + 1
                        ey =100 - (y - top + 1) * ts + 1
                    when 3 # WEST
                        ex = (x - left) * ts + 1
                        ey = 100 - (y - top + 1) * ts + 2
                        ew = 1
                        eh = 2
                    end
                    stairs_down_prim = { x: ex, y: ey, w: ew, h: eh, path: :pixel, r: 255, g: 255, b: 30, primitive_marker: :sprite }
                end
                # Draw party indicator if this is where the player is
                player_indicator_prim = { x: (x - left) * ts + 1, y: 100 - (y - top + 1) * ts, w: ts, h: ts, path: "sprites/white_circle.png", r: 255, g: 20, b: 20, a: 200, primitive_marker: :sprite } if (x == px && y == py)
            end
        end
        # Render exits after wall tiles so the always show on top
        upper_prims << exit_prim if exit_prim
        upper_prims << stairs_up_prim if stairs_up_prim
        upper_prims << stairs_down_prim if stairs_down_prim
        upper_prims << player_indicator_prim if player_indicator_prim

        a.render_target(:player_map_lower).w = 100
        a.render_target(:player_map_lower).h = 100
        a.render_target(:player_map_lower).primitives << lower_prims
        a.render_target(:player_map_upper).w = 100
        a.render_target(:player_map_upper).h = 100
        a.render_target(:player_map_upper).primitives << upper_prims 
    end

    def update a
        return if ready?
        if @generate_delay > 0
            @generate_delay -= 1
            return
        end
        @floors << @dungeon_generator.generate_dungeon_floor(a, first_floor: @floors_generated == 0, last_floor: @floors_generated == (@depth - 1), style: @style)
        a.state.player_maps.new_dungeon_floor(@id, @floors_generated, @floors[@floors_generated].width, @floors[@floors_generated].height) unless a.state.player_maps.has_dungeon_floor?(@id, @floors_generated)
        @floors_generated += 1
        if ready?
            # When the dungeon is being entered, check if the floor being entered should have a quest POI
            update_quest_pois_on_floor(a, @current_floor)

            # Draw of dungeon view. This always needs to be done even if the view was previously rendered,
            # a minor DR bug is that the dungeon's render target is wiped if the player resized the window while in town.
            # Avoid this by re-drawing the dungeon render target now.
            # TODO: Test if this is now not needed
            current_floor.update_render(a)

            # Initial map update/render
            update_player_map a

            a.state.audio.play_dungeon_ambience
        end
    end
end