# Class to measure method timings

class MethodMeasure
    attr_reader :averages, :maxes

    def initialize
        @averages = {}
        @maxes = {}
    end

    def record_time method_name, time
        time = time * 1000
        @maxes[method_name.to_sym] = time if time > (@maxes[method_name.to_sym] || 0)
        @averages[method_name.to_sym] = ((@averages[method_name.to_sym] || 0) * (59) + time) / 60
    end
end