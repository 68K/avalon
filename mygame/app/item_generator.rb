require 'app/item.rb'
require 'app/item_name_generator.rb'

# Class that generates items
class ItemGenerator
    # Make gtk args accessible
    attr_gtk

    attr_reader :item_types

    def initialize
        @item_types = [:junk, :silver, :potion,
                        :weapon, :armour, :shield, 
                        :ring, :amulet, :footwear, :gauntlets, :helmet]
        @weapon_variants = [:sword, :small_arms, :long_arms, :ranged, :blunt]
        @shield_variants = [:light_shield, :medium_shield, :heavy_shield]
        @armour_variants = [:light_armour, :medium_armour, :heavy_armour]
    end

    # @param  level the level of the item to generate, in line with dungeon levels
    # @return Random new instance of Item
    def generate_item level, allow_junk = true
        type = @item_types.sample
        case type
        when :armour
            return generate_armour(level)
        when :junk
            # Return silver if junk is not acceptable
            return generate_junk(level) if allow_junk
            generate_silver(level)
        when :potion
            return generate_potion(level)
        when :shield
            return generate_shield(level)
        when :weapon
            return generate_weapon(level)
        when :amulet
            return generate_amulet(level)
        when :footwear
            return generate_footwear(level)
        when :gauntlets
            return generate_gauntlets(level)
        when :helmet
            return generate_helmet(level)
        when :ring
            return generate_ring(level)
        when :silver
            return generate_silver(level)
        else
            #args.log_error "Unknown item type #{type} - generating weapon instead"
            return generate_weapon(level)
        end
    end

    # Generate an item suitable to sell in the shop
    # @param  level level of the item to generate, caller should handle variance
    # @return Item  An item suitable to sell in the shop
    def generate_shop_item level
        type = @item_types.sample
        # Initial simple implementation, return potion instead of silver or junk
        # This might work OK for ensuring a good number of potions are available
        case type
        when :armour
            return generate_armour(level)
        when :junk
            return generate_potion(level)
        when :potion
            return generate_potion(level)
        when :shield
            return generate_shield(level)
        when :weapon
            return generate_weapon(level)
        when :amulet
            return generate_amulet(level)
        when :footwear
            return generate_footwear(level)
        when :gauntlets
            return generate_gauntlets(level)
        when :helmet
            return generate_helmet(level)
        when :ring
            return generate_ring(level)
        when :silver
            return generate_potion(level)
        else
            #args.log_error "Unknown item type #{type} - generating weapon instead"
            return generate_weapon(level)
        end
    end

    def generate_silver level
        res = Item.new()
        res.type = :silver
        res.value = 1 + 1 * rand(level) + 2 * rand(level)
        res.name = "A pile of #{res.value} silver"
        res
    end

    def generate_armour level, armour_variant = nil
        armour_variant ||= @armour_variants.sample
        raise "Invalid armour variant #{armour_variant}" unless @armour_variants.include? armour_variant
        res = Item.new()
        res.type = :armour
        res.power = (0.5 + (level * 0.5)).ceil
        res.value = 10 * level
        res.variant = armour_variant
        res.weight = case armour_variant
        when :light_armour
            7 + rand(3)
        when :medium_armour
            10 + rand(4)
        when :heavy_armour
            15 + rand(6)
        end
        res.name = ItemNameGenerator.random_name(level, :armour, armour_variant)
        res
    end

    def generate_junk level
        res = Item.new()
        res.name = ItemNameGenerator.random_name(level, :junk)
        res.value = Math.log(level).ceil
        res.weight = 0.5
        res
    end

    def generate_potion level, potion_variant = nil
        potion_variant ||= :healing_potion
        res = Item.new()
        res.type = :potion
        res.name = "Potion of healing"
        res.power = 25
        res.usable_in_inventory = true
        res.value = 50
        res.variant = potion_variant
        res.weight = 0.3
        res
    end

    def generate_shield level, shield_variant = nil
        shield_variant ||= @shield_variants.sample
        raise "Invalid shield variant #{shield_variant}" unless @shield_variants.include? shield_variant
        res = Item.new()
        res.type = :shield
        res.power = (0.5 + (level * 0.5)).ceil
        res.value = 10 * level
        res.variant = shield_variant
        res.weight = case shield_variant
        when :light_shield
            5 + rand(5)
        when :medium_shield
            10 + rand(5)
        when :heavy_shield
            15 + rand(10)
        end
        res.name = ItemNameGenerator.random_name(level, :shield, shield_variant)
        res
    end

    def generate_weapon level, weapon_variant = nil
        weapon_variant ||= @weapon_variants.sample
        raise "Invalid weapon variant #{weapon_variant}" unless @weapon_variants.include? weapon_variant
        res = Item.new()
        res.type = :weapon
        res.power = 5 + (level * 0.6).ceil
        res.value = 10 * level
        res.variant = weapon_variant
        res.weight = 8
        res.name = ItemNameGenerator.random_name(level, :weapon, weapon_variant)
        res
    end

    def generate_amulet level
        res = Item.new()
        res.type = :amulet
        res.power = 5 + (level * 0.6).ceil
        res.value = 10 * level
        res.weight = 0.2
        res.name = ItemNameGenerator.random_name(level, :amulet)
        res
    end

    def generate_footwear level
        res = Item.new()
        res.type = :footwear
        res.power = (0.25 + (level * 0.25)).ceil
        res.value = 10 * level
        res.weight = 2
        res.name = ItemNameGenerator.random_name(level, :footwear)
        res
    end

    def generate_gauntlets level
        res = Item.new()
        res.type = :gauntlets
        res.power = (0.25 + (level * 0.25)).ceil
        res.value = 10 * level
        res.weight = 1
        res.name = ItemNameGenerator.random_name(level, :gauntlets)
        res
    end

    def generate_helmet level
        res = Item.new()
        res.type = :helmet
        res.power = (0.5 + (level * 0.4)).ceil
        res.value = 10 * level
        res.weight = 3
        res.name = ItemNameGenerator.random_name(level, :helmet)
        res
    end

    def generate_ring level
        res = Item.new()
        res.type = :ring
        res.power = 5 + (level * 0.6).ceil
        res.value = 10 * level
        res.weight = 0.1
        res.name = ItemNameGenerator.random_name(level, :ring)
        res
    end

    def from_save_data data
        res = Item.new()
        # TODO: This is elegant but actually every field should be validated and error-handled.
        # So it's better to have repetitive code that states each save file field to load.
        data.keys.each do |k|
            res.instance_variable_set("@#{k}", data[k])
        end
        # Symbols got encoded as strings so need cast back to symbols
        res.type = res.type.to_sym
        res.variant = res.variant.to_sym
        res
    end

    def serialize
        {}
    end

    def inspect
        self.to_s
    end

    def to_s
        "Item Generator"
    end
end