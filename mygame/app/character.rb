# Defines a party character in the game
class Character

    attr_reader :alive, :hp, :max_hp, :mp, :max_mp, :name, :attack,
        :xp, :xp_last, :xp_next, :level,
        :cclass_id, :cclass_name, :stats, :skills,
        :unvested_skill_points, :vested_skill_points,
        :equipped_helmet, :equipped_amulet, :equipped_gauntlets,
        :equipped_footwear, :equipped_ring1, :equipped_ring2, :equipped_ring3, :equipped_ring4,
        :equipped_offhand, :equipped_mainhand, :equipped_armour
    attr_accessor  :appearance, :defending_this_turn, :known_spells

    def initialize(character_name, class_def)
        @hp = 50
        @max_hp = 50
        @mp = 50
        @max_mp = 50
        @defending_this_turn = false
        @xp = 0
        @xp_last = 0 # XP to hit previous level. Used in UI to show progress between levels.
        @xp_next = 20
        @level = 1
        @name = character_name
        @cclass_id = class_def[:id]
        @cclass_name = class_def[:name]
        @stats = class_def[:stats]
        @skills = {}
        class_def[:starting_skill_grades].keys.each do |s|
            @skills[s] = {}
            @skills[s][:grade] = class_def[:starting_skill_grades][s]
            @skills[s][:level] = class_def[:starting_skill_levels][s]
        end
        @unvested_skill_points = 0
        @vested_skill_points = 0
        @appearance = {}
        @alive = true
        @known_spells = []
        @equipped_mainhand = nil
    end

    def serialize
        { hp: @hp, max_hp: @max_hp, mp: @mp, max_mp: @max_mp, xp: @xp, level: @level, xp_last: @xp_last, xp_next: @xp_next, 
        name: @name, appearance: @appearance, alive: @alive, cclass: @cclass_id, stats: @stats, known_spells: @known_spells,
        skills: @skills, unvested_skill_points: @unvested_skill_points, vested_skill_points: @vested_skill_points,
        equipped_helmet: @equipped_helmet&.serialize, equipped_amulet: @equipped_amulet&.serialize, 
        equipped_armour: @equipped_armour&.serialize, equipped_footwear: @equipped_footwear&.serialize,
        equipped_gauntlets: @equipped_gauntlets&.serialize, equipped_mainhand: @equipped_mainhand&.serialize,
        equipped_offhand: @equipped_offhand&.serialize, equipped_ring1: @equipped_ring1&.serialize,
        equipped_ring2: @equipped_ring2&.serialize, equipped_ring3: @equipped_ring3&.serialize,
        equipped_ring4: @equipped_ring4&.serialize }
    end

    def inspect
        self.to_s
    end

    def to_s
        serialize.to_s
    end

    def in_front_rank?
        $gtk.args.state.player_party.members.find_index(self) < $gtk.args.state.player_party.back_rank_count
    end

    def in_rear_rank?
        ! in_front_rank?
    end

    def min_range
        # TODO: Some loadouts may not allow attacking directly in front
        1
    end

    def max_range
        return 1 unless @equipped_mainhand
        return 2 if @equipped_mainhand.variant == :long_arms
        return 4 if @equipped_mainhand.variant == :ranged
        1
    end

    # @return   Numeric sum of all armour item and enchanted protection
    def armour_level
        res = 0
        res += defense_power_with(@equipped_armour) if @equipped_armour
        res += @equipped_gauntlets.power if @equipped_guantlets
        res += @equipped_footwear.power if @equipped_footwear
        res += @equipped_helmet.power if @equipped_helmet
        # TODO: If holding 2 shields, perhaps don't simply add the protection
        res += @equipped_mainhand.power if (@equipped_mainhand && @equipped_mainhand.type == :shield)
        res += @equipped_offhand.power if (@equipped_offhand && @equipped_offhand.type == :shield)

        res
    end

    # @param    d      Numeric raw damage amount
    # @param    cap    Symbol  the capability used to attack e.g. :melee, :ranged, :cast_fire
    # @return          Numeric adjusted damage amount
    def adjusted_damage d, cap
        res = d
        if cap == :melee
            res = [1, res - (0.01 * @stats[:physicality])].max
            res = [1, res - (0.01 * @stats[:acuity])].max
        elsif cap == :ranged
            res = [1, res - (0.01 * @stats[:speed])].max
            res = [1, res - (0.01 * @stats[:acuity])].max
        end
        # TODO: Wiki states that with Master armour, any hits below 5% of max HP do 0 damage
        [1, res.ceil - armour_level].max
    end

    def apply_damage d
        @hp = [0,@hp - d].max
        @alive = false if @hp == 0
    end

    # @return attack for equipped weapon or unarmed skill
    def attack_damage
        # TODO: Handle main weapon being in left hand
        if @equipped_mainhand
            attack_damage_with @equipped_mainhand
        else
            unarmed_attack
        end
    end

    def ranged_weapon_equipped?
        return false unless @equipped_mainhand
        return @equipped_mainhand.variant == :ranged
    end

    # @return Numeric damage if this character attacked with a given weapon
    def attack_damage_with weapon
        # Base damage is the weapon's power slightly boosted by skill level
        base_dmg = [(Math.log(@skills[weapon.variant][:level],20) * weapon.power), weapon.power * 0.8].max.ceil
        # Damage adjusted for skill grade
        case weapon.variant
        when :blunt
            res = base_dmg
            res = res * (1.0 + 0.01 * @stats[:might])
            res = res * 1.4 if [:skilled].include? @skills[:blunt][:grade]
            res.ceil
        when :long_arms
            res = base_dmg
            res = res * (1.0 + 0.01 * @stats[:might])
            res = res * 1.3 if [:skilled, :master].include? @skills[:long_arms][:grade]
            res = res * 1.3 if [:master].include? @skills[:long_arms][:grade]
            res.ceil
        when :small_arms
            res = base_dmg
            res = res * (1.0 + 0.0075 * @stats[:might])
            res = res * (1.0 + 0.01 * @stats[:speed])
            res = res * 1.4 if [:skilled].include? @skills[:small_arms][:grade]
            res.ceil
        when :sword
            res = base_dmg
            res = res * (1.0 + 0.01 * @stats[:might])
            res = res * (1.0 + 0.0075 * @stats[:speed])
            res = res * 1.3 if [:skilled, :expert, :master].include? @skills[:sword][:grade]
            res = res * 1.2 if [:expert, :master].include? @skills[:sword][:grade]
            res = res * 1.1 if [:master].include? @skills[:sword][:grade]
            res.ceil
        else
            base_dmg
        end
    end

    def defense_power_with item
        case item.type
        when :armour
            res = item.power
            if [:expert, :master].include?(@skills[:armour][:grade])
                res += (@skills[:armour][:level] * 1.1).ceil
            end
            res
        else
            raise "Unsupported defense comparison for item type #{item.type}"
        end
    end

    # If unarmed then attack with Unarmed skill, based on Might stat
    def unarmed_attack
        @stats[:might]
    end

    # @return number of levels gained
    def add_xp x
        gained = 0
        remainder = x
        while remainder > 0 do
            if remainder + @xp >= @xp_next
                to_next_level = @xp_next - @xp
                remainder -= to_next_level
                @xp += to_next_level
                level_up!
                gained += 1
            else
                @xp += remainder
                remainder = 0
            end
        end
        gained
    end

    def level_up!
        @level += 1
        @xp_last = @xp_next
        @xp_next += (40 + 10 * @level * Math.log(@level)).round()
        hp_inc = [2,(@max_hp * (0.02)).round()].max
        if @skills[:physique]
            case @skills[:physique][:grade]
            when :novice
                hp_inc = [3,(@max_hp * (0.01 * @skills[:physique][:level])).round()].max
            when :skilled
                hp_inc = [5,(@max_hp * (0.02 * @skills[:physique][:level])).round()].max
            when :expert
                hp_inc = [8,(@max_hp * (0.03 * @skills[:physique][:level])).round()].max
            when :master
                hp_inc = [14,(@max_hp * (0.05 * @skills[:physique][:level])).round()].max
            end
        end
        @max_hp += hp_inc
        @hp += hp_inc
        mp_inc = [2,(@max_mp * 0.02).round()].max
        if @skills[:meditation]
            case @skills[:meditation][:grade]
            when :novice
                mp_inc = [3,(@max_mp * (0.01 * @skills[:meditation][:level])).round()].max
            when :skilled
                mp_inc = [5,(@max_mp * (0.02 * @skills[:meditation][:level])).round()].max
            when :expert
                mp_inc = [8,(@max_mp * (0.03 * @skills[:meditation][:level])).round()].max
            when :master
                mp_inc = [14,(@max_mp * (0.05 * @skills[:meditation][:level])).round()].max
            end
        end
        @max_mp += mp_inc
        @unvested_skill_points += [(Math.log(@level) * 1.8).ceil,2].max
    end

    def resurrect!
        if @alive
            $gtk.log_warn "resurrect! called on #{@name} when they are not dead."
            return
        end
        @alive = true
        @hp = [@hp, @max_hp / 2].max
    end

    def capable?
        @alive
    end
    
    def time_until_turn
        1.0 + (5.0 / (@stats[:speed] || 1)) + (rand(2) / (@stats[:speed] || 1))
    end

    # TODO: More accurate times based on stats etc
    def time_to_act action
        return 3 if action == "Attack"
        return 5 if action == "Spells"
        return 3
    end

    def rest
        @hp = @max_hp
        @mp = @max_mp
        vest_skill_points
    end

    def vest_skill_points
        @vested_skill_points += @unvested_skill_points
        @unvested_skill_points = 0
    end

    def cost_to_increase_skill(skill)
        raise "Character does not have skill #{skill}" unless @skills.keys.include?(skill)
        @skills[skill][:level] + 1
    end

    def can_afford_skill_increase?(skill)
        @vested_skill_points >= cost_to_increase_skill(skill)
    end

    def increase_skill(skill)
        raise "Character does not have skill #{skill}" unless @skills.keys.include?(skill)
        raise "Character cannot afford to increase skill #{skill}" unless can_afford_skill_increase?(skill)
        @vested_skill_points -= cost_to_increase_skill(skill)
        @skills[skill][:level] += 1
    end

    def upgrade_skill(skill, new_grade)
        raise "Character does not have skill #{skill}" unless @skills[skill]
        raise "#{new_grade} is not a symbol" unless new_grade.is_a? Symbol
        current_grade = @skills[skill][:grade]
        raise "Already #{current_grade}!" if current_grade == new_grade
        case new_grade
        when :novice
            raise "Cannot upgrade to novice!"
        when :skilled
            raise ":skilled is a downgrade" if current_grade == :expert || current_grade == :master
        when :expert
            raise ":expert is a downgrade" if current_grade == :master
        else
            raise "Unhandled grade #{new_grade}"
        end
        @skills[skill][:grade] = new_grade
    end

    def heal amount
        @hp = [@max_hp, @hp + amount].min
    end

    # Spend mp. It is assumed that the amount was already checked, this method will allow the spend and leave at least 0 MP
    def spend_mp amount
        @mp = [0, @mp - amount].max
    end

    # It's assumed can_equip? was already checked
    def equip item, slot
        raise "can_equip? returned false and should be checked before calling equip" unless can_equip? item
        raise "Can not equip item type '#{item.type}'" unless [:weapon, :armour, :shield, :ring, :amulet, :footwear, :helmet, :gauntlets].include? item.type
        raise "Not a valid equip slot: #{slot}" unless [:offhand, :mainhand, :amulet, :ring1, :ring2, :ring3, :ring4, :helmet, :gauntlets, :armour, :footwear].include? slot
        # TODO: Handle two-handed items automatically, unequipping other hand
        $gtk.args.state.player_party.inventory << instance_variable_get("@equipped_#{slot}") if instance_variable_get("@equipped_#{slot}")
        instance_variable_set("@equipped_#{slot}", item)
        $gtk.args.state.player_party.inventory.delete(item)
    end

    def item_at_slot slot
        raise "Not a valid equip slot: #{slot}" unless [:offhand, :mainhand, :amulet, :ring1, :ring2, :ring3, :ring4, :helmet, :gauntlets, :armour, :footwear].include? slot
        instance_variable_get("@equipped_#{slot}")
    end

    # TODO: Initially just check for basic skill. Variants like heavy armour should check for a proficiency level
    # TODO: Handle dual wielding
    def can_equip? item
        raise "#{item} is not an Item" unless item.is_a? Item
        raise "Not an equippable item type (#{item.type})" unless [:weapon, :armour, :shield, :ring, :amulet, :footwear, :helmet, :gauntlets].include?(item.type)
        return true if [:ring, :amulet, :footwear, :gauntlets, :helmet].include?(item.type)
        case item.type
        when :weapon
            return @skills.has_key?(item.variant)
        when :shield
            return @skills.has_key?(:shield)
        when :armour
            return @skills.has_key?(:armour)
        end
    end

    # Compare an item with what the player currently has equipped
    # @return Numeric positive if item is better
    # @return Numeric negative if item is worse
    # @return Numeric 0 if item is equally powerfull
    # @return nil     if item is not equippable
    # TODO: Handle dual wielding
    def compare_equipment item
        raise "Not a comparable item type (#{item.type})" unless [:weapon, :armour, :shield].include?(item.type)
        return nil unless can_equip?(item)
        case item.type
        when :weapon
            return attack_damage_with(item) - unarmed_attack unless @equipped_mainhand
            return attack_damage_with(item) - attack_damage_with(@equipped_mainhand)
        when :armour
            return defense_power_with(item) unless @equipped_armour
            return defense_power_with(item) - defense_power_with(@equipped_armour)
        when :shield
            # TODO: Need to consider armour skill level and grade
            # TODO: Need to consider right hand wielding of shield
            return item.power unless (@equipped_offhand && @equipped_offhand.type == :shield)
            return item.power - @equipped_offhand.power
        end
        0
    end

    def set_appearance appearance
        @appearance = appearance
    end

    def set_up_from_save_data data
        @max_hp = data[:max_hp]
        @hp = data[:hp]
        @max_mp = data[:max_mp]
        @mp = data[:mp]
        @alive = data[:alive]
        @stats = data[:stats]
        @appearance = data[:appearance]
        @level = data[:level]
        @xp = data[:xp]
        @xp_last = data[:xp_last]
        @xp_next = data[:xp_next]
        @known_spells = data[:known_spells]
        @skills = data[:skills]
        # JSON has no symbols. Change the skill keys to symbols
        @skills.transform_keys! { |k| k.to_sym }
        # Change the skill grades from strings to symbols
        @skills.keys.each { |k| @skills[k][:grade] = @skills[k][:grade].to_sym }
        @unvested_skill_points = data[:unvested_skill_points]
        @vested_skill_points = data[:vested_skill_points]
        [:equipped_amulet, :equipped_armour, :equipped_footwear, :equipped_gauntlets, :equipped_helmet,
        :equipped_mainhand, :equipped_offhand, :equipped_ring1, :equipped_ring2, :equipped_ring3, :equipped_ring4].each do |e|
            instance_variable_set(('@' + e.to_s).to_sym, $gtk.args.state.item_generator.from_save_data(data[e])) if data[e]
        end
    end
end