class Item
    attr_accessor :name, :power, :type, :usable_in_inventory, :value, :variant, :weight

    def initialize
        @name = "A miserable pile of secrets"
        # Generic "power" field. For a weapon this is attack, for a shield defense, for a potion potency etc.
        @power = 1
        @type = :junk
        @usable_in_inventory = false
        @value = 1
        @variant = :unknown
        @weight = 0.5
    end

    def serialize
        res = {}
        # Get all the ivars and cut the @ off the front
        instance_variables.each { |i| res[i.to_s[1..-1].to_sym] = instance_variable_get(i) }
        res
    end

    def inspect
        self.to_s
    end

    def to_s
        @name
    end
end