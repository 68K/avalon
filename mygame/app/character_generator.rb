require 'app/character.rb'
require 'app/classes.rb'

# Class that generates instances of Character
class CharacterGenerator

    attr_reader :character_classes

    def initialize
        @names = %w{Darien Sara Granst Melkul Uttra Banst Gaenvid Cathal Aife Gwladus Gerard Clodagh}
        @character_classes = ClassDefinitions.new
    end

    def random_name
        # TODO: NameGenerator
        @names[rand(@names.length)]
    end

    def random_character(cclass = :any)
        raise "cclass for character '#{cclass}' must be :any or one of #{@character_classes.classes.keys}" unless (cclass == :any || @character_classes.classes.keys.include?(cclass))
        cclass = @character_classes.classes.keys.sample if cclass == :any
        res = Character.new(random_name(), @character_classes.classes[cclass])

        default_setup_for_character res

        # Set appearance
        res.set_appearance($gtk.args.state.portraits.random_appearance())

        res
    end

    def from_character_editor args
        raise "Cannot generate character, no name set" unless args.state.creating_character_name
        raise "Cannot generate character, no class set" unless args.state.creating_character_class
        raise "Cannot generate character, no appearance set" unless args.state.creating_character_appearance
        # The stored class is a string, find its symbol
        cclass_id = @character_classes.classes.keys.select { |c| c.to_s.capitalize == args.state.creating_character_class }.first
        raise "Unknown class with name '#{args.state.creating_character_class}'" unless cclass_id
        res = Character.new(args.state.creating_character_name, @character_classes.classes[cclass_id])
        res.appearance = args.state.creating_character_appearance
        default_setup_for_character res
        res
    end

    def default_setup_for_character c
        item_generator = $gtk.args.state.item_generator
        spell_controller = $gtk.args.state.spells
        # Equip a base weapon for the class  [:cleric, :cutpurse, :hunter, :knight, :mage, :mercenary, :sage, :vikingr]
        case c.cclass_id
        when :cleric
            c.equip(item_generator.generate_weapon(1, :blunt), :mainhand)
        when :cutpurse
            c.equip(item_generator.generate_weapon(1, :small_arms), :mainhand)
        when :hunter
            c.equip(item_generator.generate_weapon(1, :ranged), :mainhand)
        when :knight
            c.equip(item_generator.generate_weapon(1, :sword), :mainhand)
        when :mage
            c.equip(item_generator.generate_weapon(1, :blunt), :mainhand)
        when :mercenary
            c.equip(item_generator.generate_weapon(1, :sword), :mainhand)
        when :vikingr
            c.equip(item_generator.generate_weapon(1, :sword), :mainhand)
        end
        # Characters that can use shields get a light shield
        c.equip(item_generator.generate_shield(1, :light_shield), :offhand) if c.skills[:shield]
        # Equip light armour
        c.equip(item_generator.generate_armour(1, :light_armour), :armour) if c.skills[:armour]

        # Initial spells
        c.known_spells = spell_controller.initial_spells(c)
    end

    def from_save_data data
        res = Character.new(data[:name], @character_classes.classes[data[:cclass].to_sym])
        res.set_up_from_save_data(data)
        res
    end

    def serialize
        {}
    end

    def inspect
        self.to_s
    end

    def to_s
        "CharacterGenerator"
    end
end