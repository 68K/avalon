# Represents an event in the game e.g. a character appearing to provide some exposition.
# Events have required conditions to become available, and once variable may trigger in a given situation 
class GameEvent
    attr_accessor :id, :requirements, :text, :title, :trigger

    def initialize

    end

    def self.from_spec spec
        res = GameEvent.new()
        [:id, :requirements, :text, :title].each { |k| raise "GameEvent spec missing required key :#{k}" unless spec.has_key? k }
        # For brevity, most events are expected to be when returning to town, so default to that
        # TODO: enter_dungeon should reference a dungeon id for a specific dungeon and not be for _any_ dungeon entry
        if spec.has_key? :trigger
            raise "GameEvent spec has unsupported trigger type :#{spec[:trigger]}" unless [:enter_dungeon, :enter_inn, :rest_at_inn, :return_to_town].include? spec[:trigger]
            res.trigger = spec[:trigger]
        else
            res.trigger = :return_to_town
        end
        res.id = spec[:id]
        res.title = spec[:title]
        raise "GameEvent spec :text is not an array" unless spec[:text].is_a? Array
        res.text = spec[:text]
        spec[:requirements].each do |r, v|
            raise "Unsupported requirement type" unless [:event_passed, :quest_complete, :quest_ended].include? r
        end
        res.requirements = spec[:requirements]
        res
    end

    def serialize
        {}
    end

    def inspect
        self.to_s
    end

    def to_s
        "GameEvent"
    end
end