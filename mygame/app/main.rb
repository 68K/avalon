# Work-around test files getting auto-loaded
module GTK
  class Runtime
    module AutoTest
      def tick_auto_test
      end
    end
  end
end

require 'app/controllers/animation_controller.rb'
require 'app/controllers/audio_controller.rb'
require 'app/controllers/calendar_controller.rb'
require 'app/controllers/creature_controller.rb'
require 'app/controllers/event_controller.rb'
require 'app/controllers/log_controller.rb'
require 'app/controllers/magic_controller.rb'
require 'app/controllers/panel_controller.rb'
require 'app/controllers/player_maps_controller.rb'
require 'app/controllers/portrait_controller.rb'
require 'app/controllers/quest_controller.rb'
require 'app/controllers/save_controller.rb'
require 'app/controllers/scene_controller.rb'
require 'app/controllers/guild_controller.rb'
require 'app/controllers/shop_controller.rb'
require 'app/controllers/combat_controller.rb'
require "app/character.rb"
require "app/character_generator.rb"
require "app/creature.rb"
require "app/dungeon.rb"
require "app/dungeon_map_generator.rb"
require "app/enemy_party.rb"
require "app/item_generator.rb"
require "app/method_measure.rb"
require 'app/name_generator.rb'
require "app/particle_effect.rb"
require "app/party.rb"
# This must be last!
require 'app/finalise.rb'

def defaults state
    state.method_measures ||= MethodMeasure.new()
    state.logs ||= LogController.new()
    state.calendar ||= CalendarController.new()
    state.animations ||= AnimationController.new()
    state.scenes ||= SceneController.new()
    state.name_generator ||= NameGenerator.new()
    state.dungeon_map_generator ||= DungeonMapGenerator.new()
    state.player_maps ||= PlayerMaps.new()
    state.combat ||= CombatController.new()
    state.combat_turn_duration ||= 50
    state.save_slot ||= 0
    state.save_slot_summaries ||= [nil, nil, nil]

    state.bestiary ||= CreatureController.new()
    unless state.creature_sprites
        state.creature_sprites = {}
        state.creature_sprites[:goblin] = "sprites/creatures/goblin3.png"
        state.creature_sprites[:skeleton] = "sprites/creatures/skeleton3.png"
        state.creature_sprites[:ratkin] = "sprites/creatures/ratkin2.png"
        state.creature_sprites[:bat] = "sprites/creatures/bat.png"
        state.creature_sprites[:human_archer] = "sprites/creatures/human_archer.png"
        state.creature_sprites[:human_blunt] = "sprites/creatures/human_blunt.png"
        state.creature_sprites[:human_mage] = "sprites/creatures/human_mage.png"
        state.creature_sprites[:human_spear] = "sprites/creatures/human_spear.png"
        state.creature_sprites[:human_sword] = "sprites/creatures/human_sword.png"
        state.creature_sprites[:kobold] = "sprites/creatures/kobold.png"
        state.creature_sprites[:lizard] = "sprites/creatures/lizard.png"
        state.creature_sprites[:orc_melee] = "sprites/creatures/orc_melee.png"
        state.creature_sprites[:orc_thrower] = "sprites/creatures/orc_thrower.png"
        state.creature_sprites[:orc_mage] = "sprites/creatures/orc_mage.png"
        state.creature_sprites[:rat] = "sprites/creatures/rat.png"
        state.creature_sprites[:slime] = "sprites/creatures/slime.png"
        state.creature_sprites[:snake] = "sprites/creatures/snake.png"
        state.creature_sprites[:spider] = "sprites/creatures/spider.png"
        state.creature_sprites[:zombie] = "sprites/creatures/zombie.png"
    end

    state.item_generator ||= ItemGenerator.new()

    state.spells ||= MagicController.new()

    state.character_generator ||= CharacterGenerator.new()
    state.player_party ||= Party.new()
    state.panels ||= PanelController.new()

    state.quests ||= QuestController.new()
    state.events ||= EventController.new()

    # Random combat chance
    state.step_count ||= 0
    state.steps_to_next_encounter ||= 4
    state.grace_steps_between_fights ||= 2
    state.grace_steps_remaining ||= 0

    # View transitioning 
    state.moving_duration ||= 16
    state.step_duration ||= 2
    state.moving_counter ||= 0
    state.turning_duration ||= 90
    state.turning_counter ||= 0

    # compass direction
    state.compass_direction ||= 0

    # Opened chests
    state.opened_chests ||= []

    # Main dungeons
    # TOOD: Should this be a in a controller?
    state.dungeons ||= initial_dungeons 

    # Town service details
    state.cost_to_rest ||= 4
    state.cost_to_raise ||= 100
    state.shop ||= ShopController.new(state)
    state.guild ||= GuildController.new

    state.particle_effects ||= []

    state.is_initialised = true
end

def initial_dungeons
    res = []
    res << { id: 1, depth: 3, base_level: 5, name: "The Underhalls", style: :underhalls, available: true }
    res << { id: 2, depth: 4, base_level: 20, name: "Catacombs Of The Saint", style: :catacombs, available: true }
    res << { id: 3, depth: 5, base_level: 35, name: "Sáhangréine", style: :ice, available: true }
    res << { id: 4, depth: 5, base_level: 45, name: "Gold Rick's Citadel", style: :citadel, available: false }
    res << { id: 5, depth: 5, base_level: 60, name: "Crystal Frontier", style: :crystal, available: false }
    res
end

# Player input indicates confirmation
def confirm_pressed a
    a.inputs.keyboard.key_down.space || a.inputs.keyboard.key_down.e || a.inputs.keyboard.key_down.j
end

# Player input indicates cancellation
def cancel_pressed a
    a.inputs.keyboard.key_down.escape || a.inputs.keyboard.key_down.q
end

# Player input indicates toggling menu
def menu_pressed a
    a.inputs.keyboard.key_down.tab
end

def process_input a
    start_time = Time.now()
    # Toggle mute at any time
    a.state.audio.toggle_mute if a.inputs.keyboard.key_down.m

    case a.state.scenes.current
    when :title_scene
        a.state.scenes.switch_to a, :save_slot_scene if confirm_pressed a
    when :save_slot_scene
        if a.inputs.keyboard.key_down.a
            a.state.save_slot -= 1 if a.state.save_slot > 0
        elsif a.inputs.keyboard.key_down.d
            a.state.save_slot += 1 if a.state.save_slot < 2
        elsif confirm_pressed a
            if a.state.save_slot_summaries[a.state.save_slot]
                # TODO: Need to handle loading taking some time
                a.state.save_controller.load(a.state.save_slot)
                a.state.scenes.switch_to a, :town_scene
            else
                a.state.scenes.switch_to a, :party_creation_scene
            end
        elsif a.inputs.keyboard.key_down.delete
            # TODO: Confirm deletion!
            a.state.save_controller.delete_save_slot(a.state.save_slot)
        end
    when :party_creation_scene
        # Process input on open dialog
        # TODO: Error handle no panels being open
        if a.inputs.keyboard.key_down.s
            a.state.panels.down_input a
        elsif a.inputs.keyboard.key_down.w
            a.state.panels.up_input a
        elsif a.inputs.keyboard.key_down.a
            a.state.panels.left_input a
        elsif a.inputs.keyboard.key_down.d
            a.state.panels.right_input a
        elsif confirm_pressed a
            a.state.panels.confirm_input a
        elsif cancel_pressed a
            a.state.panels.cancel_input a
        elsif menu_pressed a
            a.state.panels.menu_input a
        end
    when :town_scene
        if a.state.panels.any_dialogs_open?
            # Process input on open dialog
            if a.inputs.keyboard.key_down.s
                a.state.panels.down_input a
            elsif a.inputs.keyboard.key_down.w
                a.state.panels.up_input a
            elsif a.inputs.keyboard.key_down.a
                a.state.panels.left_input a
            elsif a.inputs.keyboard.key_down.d
                a.state.panels.right_input a
            elsif confirm_pressed a
                a.state.panels.confirm_input a
            elsif cancel_pressed a
                a.state.panels.cancel_input a
            end
        else
            if confirm_pressed a
                a.gtk.log_warn "Player is in town without any menu to interact with..."
                a.state.scenes.switch_to a, :dungeon_scene
            end
        end
    when :dungeon_scene
        return unless a.state.dungeon.ready?
        
        # For brevity, a reference to the dungeon
        d = a.state.dungeon.current_floor
        if a.state.panels.any_dialogs_open?
            # Process input on open dialog
            if a.inputs.keyboard.key_down.s
                a.state.panels.down_input a
            elsif a.inputs.keyboard.key_down.w
                a.state.panels.up_input a
            elsif a.inputs.keyboard.key_down.a
                a.state.panels.left_input a
            elsif a.inputs.keyboard.key_down.d
                a.state.panels.right_input a
            elsif confirm_pressed a
                a.state.panels.confirm_input a
            elsif cancel_pressed a
                a.state.panels.cancel_input a
            elsif menu_pressed a
                a.state.panels.menu_input a
            end
        elsif a.state.in_combat
            # skip to next turn if at least a little time has passed
            if a.inputs.keyboard.key_down.e && a.state.combat_turn_delay < 45
                a.state.combat_turn_delay = 0
            end
        else
            # Cheats
            if a.inputs.keyboard.key_down.pageup
                if a.state.cheat_no_encounters == true
                    a.state.cheat_no_encounters = false
                    a.state.steps_to_next_encounter = 2 + rand(4)
                else
                    a.state.cheat_no_encounters = true
                end
            end
            a.state.cheat_debug_info = (! a.state.cheat_debug_info) if a.inputs.keyboard.key_down.pagedown

            return if a.state.moving || a.state.turning
            if menu_pressed a
                a.state.panels.menu_input a
            else
                moved = false
                view_changed = false
                if a.inputs.keyboard.key_down.a
                    d.player_dir -= 1
                    d.player_dir = 3 if d.player_dir < 0
                    a.state.turning = true
                    a.state.turning_counter = 0
                    a.state.turning_clockwise = false
                    view_changed = true
                elsif a.inputs.keyboard.key_down.d
                    d.player_dir += 1
                    d.player_dir = 0 if d.player_dir > 3
                    a.state.turning = true
                    a.state.turning_counter = 0
                    a.state.turning_clockwise = true
                    view_changed = true
                elsif a.inputs.keyboard.key_down.w
                    # Move ahead into empty space
                    if d.player_dir == 0 && d.valid_move?(a, d.player_x, d.player_y, d.player_x, d.player_y-1)
                        d.player_y -= 1
                        moved = true
                        view_changed = true
                    elsif d.player_dir == 1 && d.valid_move?(a, d.player_x, d.player_y, d.player_x + 1, d.player_y)
                        d.player_x += 1
                        moved = true
                        view_changed = true
                    elsif d.player_dir == 2 && d.valid_move?(a, d.player_x, d.player_y, d.player_x, d.player_y+1)
                        d.player_y += 1
                        moved = true
                        view_changed = true
                    elsif d.player_dir == 3 && d.valid_move?(a, d.player_x, d.player_y, d.player_x - 1, d.player_y)
                        d.player_x -= 1
                        moved = true
                        view_changed = true
                    end
                    # Press into dungeon exit
                    if !moved && d.exit_ahead?
                        a.state.panels.open_return_to_town_panel a
                    end
                    if !moved && d.stairs_down_ahead?
                        a.state.panels.open_stairs_down_panel a
                    end
                    if !moved && d.stairs_up_ahead?
                        a.state.panels.open_stairs_up_panel a
                    end
                elsif a.inputs.keyboard.key_down.e
                    if p = a.state.dungeon.current_floor.poi_underfoot
                        if p.type == :chest && !(a.state.opened_chests.find{ |c| c.dungeon == a.state.dungeon.id && c.floor == a.state.dungeon.current_floor_number && c.x == p.x && p.y == c.y})
                            a.state.panels.open_chest_confirm_panel a
                        elsif p.type == :quest && a.state.quests.progress[p.quest_id][:state] == :started && a.state.quests.current_goal_of_quest(p.quest_id).goal_id == p.goal_id
                            a.state.panels.open_quest_update_panel a, a.state.quests.current_goal_of_quest(p.quest_id)
                        end
                    end
                end
                if moved
                    a.state.moving = true
                    a.state.moving_counter = 0
                    a.state.step_count += 1
                    a.state.grace_steps_remaining = [0, a.state.grace_steps_remaining - 1].max
                end
                if view_changed
                    # We moved, update the view of the dungeon
                    a.state.dungeon.current_floor.update_render(a)
                end
            end
        end

        if (! a.state.in_combat) &&
        (! a.state.cheat_no_encounters) &&
        (! a.state.moving && ! a.state.turning) &&
        a.state.grace_steps_remaining == 0 &&
        a.state.step_count > 0 &&
        a.state.step_count % a.state.steps_to_next_encounter == 0
            a.state.combat.begin_combat a
        end
    when :game_over_scene
        a.state.scenes.switch_to(a, :title_scene) if confirm_pressed(a) && a.state.ticks_on_game_over_screen > 240
    end
    
    a.state.method_measures.record_time(:process_input, Time.now - start_time)
end

# Method to update timers and process events based on times running out
def process_timers args
    start_time = Time.now()
    if args.state.scenes.current == :game_over_scene
        args.state.ticks_on_game_over_screen += 1
    end
    return unless args.state.scenes.in_dungeon?
    unless (args.state.dungeon.ready? && args.state.player_party.ready)
        args.state.dungeon.update args
        args.state.player_party.update args
        return
    end
    if args.state.moving
        args.state.moving_counter += 2
        args.state.dungeon.current_floor.update_render args
        if args.state.moving_counter >= args.state.moving_duration
            args.state.moving = false
            args.state.moving_counter = 0
            args.state.dungeon.current_floor.update_render args
            args.state.dungeon.update_player_map args
            args.state.calendar.add_seconds(10)
        end
    end
    if args.state.turning
        args.state.turning_counter += 8
        args.state.dungeon.current_floor.update_render(args)
        if args.state.turning_counter >= args.state.turning_duration
            args.state.turning = false
            args.state.turning_counter = 0
            args.state.dungeon.current_floor.update_render args
        end
    end
    if args.state.in_combat
        if args.state.combat.waiting_on_animations
            args.state.combat.stop_waiting_on_animations(args) if args.state.animations.combat_animations_done?
        end
    end
    args.state.method_measures.record_time(:update_timers, Time.now() - start_time)
end

def update_particle_effects a
    a.state.particle_effects.each { |pe| pe.update }
    a.state.particle_effects.map! { |pe| pe.done? ? nil : pe }
    a.state.particle_effects.compact!
end

def render args, outs
    start_time = Time.now()
    primitives_to_render = []
    case args.state.scenes.current
    when :title_scene
        primitives_to_render << { path: "sprites/title.png", x: 0, y: 0, w: 1280, h: 720, primitive_marker: :sprite }
    when :save_slot_scene
        primitives_to_render << { path: :pixel, r: 0, g: 0, b: 0, x: 0, y: 0, w: 1280, h: 720, primitive_marker: :sprite }
        primitives_to_render << { x: 640, y: 680, text: "Choose a slot", r: 0, g: 255, b: 33, alignment_enum: 1, size_enum: 4, primitive_marker: :label }
        3.times do |i|
            primitives_to_render << { path: :pixel, r: 80, g: 80, b: 80, x: 128 + 384 * i, y: 100, w: 256, h: 500, primitive_marker: :sprite }
            if summary = args.state.save_slot_summaries[i]
                summary[:members].each_with_index do |m, j|
                    primitives_to_render << { x: 140 + 384 * i, y: 500 - j * 32, text: "#{m[:name]}, level #{m[:level]} #{m[:cclass]}", r: 0, g: 255, b: 33, alignment_enum: 0, size_enum: -1, primitive_marker: :label }
                end
                primitives_to_render << { x: 140 + 384 * i, y: 360, text: "#{summary[:silver]} silver", r: 0, g: 255, b: 33, alignment_enum: 0, size_enum: 2, primitive_marker: :label }
            else
                primitives_to_render << { x: 256 + 384 * i, y: 360, text: "New Game", r: 0, g: 255, b: 33, alignment_enum: 1, size_enum: 2, primitive_marker: :label }
            end
            if i == args.state.save_slot
                tint = 32 + 32 * (args.tick_count % 20)
                primitives_to_render << { path: :pixel, r: tint, g: tint, b: tint, x: 124 + 384 * i, y: 96, w: 264, h: 508, primitive_marker: :border }
            end
        end
    when :party_creation_scene
        primitives_to_render << { path: :pixel, r: 0, g: 0, b: 0, x: 0, y: 0, w: 1280, h: 720, primitive_marker: :sprite }
    when :town_scene
        bg = "sprites/town.png"
        if args.state.dialogs_that_are_in_inn[args.state.panels.focused_dialog_type]
            bg = "sprites/inn.png"
        elsif args.state.dialogs_that_are_in_guild[args.state.panels.focused_dialog_type]
            bg = "sprites/guild.png"
        elsif args.state.dialogs_that_are_in_temple[args.state.panels.focused_dialog_type]
            bg = "sprites/temple.png"
        elsif args.state.dialogs_that_are_in_shop[args.state.panels.focused_dialog_type]
            bg = "sprites/shop.png"
        end
        primitives_to_render << { path: bg, x: 0, y: 0, w: 1280, h: 720, primitive_marker: :sprite }
        primitives_to_render << { x: 1260, y: 700, text: args.state.calendar.date_time_string, r: 20, g: 20, b: 20, alignment_enum: 2, size_enum: 1, primitive_marker: :label}
    when :dungeon_scene
        if args.state.dungeon.ready? && args.state.player_party.ready
            render_dungeon args, outs
        else
            args.outputs.solids << { x: 0, y: 0, w: 1280, h: 720, r: 1, g: 1, b: 1, a: 255 }
            args.outputs.labels << [ 550, 360, "Loading...", 0, 0, 180, 180, 180]
        end
    when :game_over_scene
        primitives_to_render << { path: "sprites/game_over.png", x: 0, y: 0, w: 1280, h: 720, primitive_marker: :sprite }
    end
    primitives_to_render << args.state.panels.primitives_to_render(args, outs)
    primitives_to_render << args.state.panels.status_message_primitives(args) if args.state.panels.status_message_open?
    primitives_to_render.push(*args.state.animations.get_sprites())
    args.state.particle_effects.each { |pe| primitives_to_render << pe.render }
    outs.primitives << primitives_to_render
    args.state.method_measures.record_time(:render, Time.now() - start_time)
end

def render_dungeon args, outs
    primitives_to_render = []
    # Render the dungeon_view render_target
    primitives_to_render << { x: 0, y: 0, w: 1280, h: 720, path: :dungeon_view, primitive_marker: :sprite }

    if args.state.in_combat
        primitives_to_render << args.state.combat.render_primitives_for_timeline(args)
        
        args.state.combat.enemy_party.rear_rank.each_with_index do |e,i|
            if e.alive
                x,y = args.state.combat.enemy_party.screen_position i, :rear
                primitives_to_render << { path: args.state.creature_sprites[e.spec.type], x: x, y: y, w: 180, h: 180, a: 255, r: 190, g: 190, b: 190, primitive_marker: :sprite}
                if args.state.combat.choosing_target && args.state.combat.targeted_rank == :rear && i == args.state.combat.chosen_target
                    alpha = 64 + 4 * (args.state.tick_count % 32) 
                    blend_mode = 2
                    # TODO: Try to silhouette using blend_mode 3 and a render target
                    4.times { primitives_to_render << { path: args.state.creature_sprites[e.spec.type], x: x, y: y, w: 180, h: 180, a: alpha, blendmode_enum: blend_mode, primitive_marker: :sprite } }
                end
            end
        end
        args.state.combat.enemy_party.middle_rank.each_with_index do |e,i|
            if e.alive
                x,y = args.state.combat.enemy_party.screen_position i, :middle
                primitives_to_render << { path: args.state.creature_sprites[e.spec.type], x: x, y: y, w: 220, h: 220, a: 255, r: 220, g: 220, b: 220, primitive_marker: :sprite}
                if args.state.combat.choosing_target && args.state.combat.targeted_rank == :middle && i == args.state.combat.chosen_target
                    alpha = 64 + 4 * (args.state.tick_count % 32) 
                    blend_mode = 2
                    # TODO: Try to silhouette using blend_mode 3 and a render target
                    4.times { primitives_to_render << { path: args.state.creature_sprites[e.spec.type], x: x, y: y, w: 220, h: 220, a: alpha, blendmode_enum: blend_mode, primitive_marker: :sprite } }
                end
            end
        end
        args.state.combat.enemy_party.front_rank.each_with_index do |e,i|
            if e.alive
                x,y = args.state.combat.enemy_party.screen_position i, :front
                primitives_to_render << { path: args.state.creature_sprites[e.spec.type], x: x, y: y, w: 256, h: 256, a: 255, primitive_marker: :sprite}
                if args.state.combat.choosing_target && args.state.combat.targeted_rank == :front && i == args.state.combat.chosen_target
                    alpha = 64 + 4 * (args.state.tick_count % 32) 
                    blend_mode = 2
                    # TODO: Try to silhouette using blend_mode 3 and a render target
                    4.times { primitives_to_render << { path: args.state.creature_sprites[e.spec.type], x: x, y: y, w: 256, h: 256, a: alpha, blendmode_enum: blend_mode, primitive_marker: :sprite } }
                end
            end
        end
    end

    # Panels around viewport
    primitives_to_render << { path: 'sprites/black_square.png', x: 1024, y: 0, w: 256, h: 720, primitive_marker: :sprite, primitive_marker: :sprite }
    primitives_to_render << { path: 'sprites/black_square.png', x: 0, y: 0, w: 1280, h: 142, primitive_marker: :sprite, primitive_marker: :sprite }

    # Party
    args.state.player_party.members.each_with_index do |m,i|
        # Portrait
        primitives_to_render << { x: 1024, y: 626 - 84 * i,  w: 256,   h: 84, path: 'sprites/white_square.png', a: 255,  r: 40,  g: 40,  b: 40, primitive_marker: :sprite }
        primitives_to_render << { x: 1024, y: 626 - 84 * i,  w: 84,    h: 84, path: 'sprites/white_square.png', a: 255,  r: 200, g: 200, b: 200, primitive_marker: :sprite }
        primitives_to_render << { x: 1026, y: 626 - 84 * i,  w: 82,    h: 82, path: 'sprites/white_square.png', a: 255,  r: 50,  g: 50, b: 50, primitive_marker: :sprite }
        rt = case i
        when 0
            :portrait1
        when 1
            :portrait2
        when 2
            :portrait3
        when 3
            :portrait4
        end
        primitives_to_render << { path: m.alive ? rt : "sprites/dead.png", x: 1026, y: 628 - 84 * i, w: 80, h: 80, primitive_marker: :sprite }

        # Rank icon
        primitives_to_render << { path: "sprites/white_square.png", x: 1090, y: 628 - 84 * i, w: 16, h: 16, a: 200, r: 0, g: 0, b: 0, primitive_marker: :sprite }
        primitives_to_render << { path: i < args.state.player_party.back_rank_count ? "sprites/icons/player_pegs_2_front.png" : "sprites/icons/player_pegs_2_back.png", x: 1090, y: 628 - 84 * i, w: 16, h: 16, primitive_marker: :sprite }

        # HP bar
        primitives_to_render << { x: 1112, y: 692 - 84 * i, w: 162, h: 8, path: 'sprites/white_square.png', angle: 0, a: 255,  r: 0,  g: 0, b: 0, primitive_marker: :sprite }
        primitives_to_render << { x: 1112, y: 696 - 84 * i, w: 162 * (m.hp / m.max_hp), h: 4, path: 'sprites/white_square.png', angle: 0, a: 255, r: 100, g: 200,  b: 100, primitive_marker: :sprite }
        primitives_to_render << { x: 1112, y: 692 - 84 * i, w: 162 * (m.hp / m.max_hp), h: 4, path: 'sprites/white_square.png', angle: 0, a: 255, r: 40, g: 150, b: 40, primitive_marker: :sprite }

        # MP bar
        primitives_to_render << { x: 1112, y: 678 - 84 * i, w: 162, h: 8, path: 'sprites/white_square.png', angle: 0, a: 255, r: 0, g: 0, b: 0, primitive_marker: :sprite }
        primitives_to_render << { x: 1112, y: 682 - 84 * i, w: 162 * (m.mp / m.max_mp), h: 4, path: 'sprites/white_square.png', angle: 0, a: 255, r:  100, g:  100, b: 200, primitive_marker: :sprite }
        primitives_to_render << { x: 1112, y: 678 - 84 * i, w: 162 * (m.mp / m.max_mp), h: 4, path: 'sprites/white_square.png', angle: 0, a: 255, r:  40, g:  40, b: 150, primitive_marker: :sprite }

        # XP bar
        primitives_to_render << { x: 1112, y: 670 - 84 * i, w: 162, h: 4, path: 'sprites/white_square.png', angle: 0, a: 255, r: 0, g: 0, b: 0, primitive_marker: :sprite }
        primitives_to_render << { x: 1112, y: 672 - 84 * i, w: 162 * ((m.xp - m.xp_last) / (m.xp_next - m.xp_last)), h: 2, path: 'sprites/white_square.png', angle: 0, a: 255, r: 150, g: 150, b: 20, primitive_marker: :sprite }
        primitives_to_render << { x: 1112, y: 670 - 84 * i, w: 162 * ((m.xp - m.xp_last) / (m.xp_next - m.xp_last)), h: 2, path: 'sprites/white_square.png', angle: 0, a: 255, r: 70, g: 70, b: 10, primitive_marker: :sprite }
    end
    # Highlight the character
    if args.state.panels.focused_dialog_type == :character_combat_choice
        primitives_to_render << { x: 1024, y: 626 - 84 * args.state.combat.character_making_choice, w: 256, h: 84, path: 'sprites/white_square.png', angle: 0, a: 2 * (args.state.tick_count % 32), r: 200, g: 200, b: 200, primitive_marker: :sprite }
    end
    if args.state.choosing_item_target
        primitives_to_render << { x: 1024, y: 626 - 84 * args.state.item_target_character, w: 256, h: 84, path: 'sprites/white_square.png', angle: 0, a: 2 * (args.state.tick_count % 32), r: 200, g: 200, b: 200, primitive_marker: :sprite }
    elsif args.state.choosing_spell_target
        primitives_to_render << { x: 1024, y: 626 - 84 * args.state.spell_target_character, w: 256, h: 84, path: 'sprites/white_square.png', angle: 0, a: 2 * (args.state.tick_count % 32), r: 200, g: 200, b: 200, primitive_marker: :sprite }
    elsif args.state.choosing_character_to_equip
        primitives_to_render << { x: 1024, y: 626 - 84 * args.state.chosen_character_to_equip, w: 256, h: 84, path: 'sprites/white_square.png', angle: 0, a: 2 * (args.state.tick_count % 32), r: 200, g: 200, b: 200, primitive_marker: :sprite }
    elsif args.state.combat.choosing_ally
        primitives_to_render << { x: 1024, y: 626 - 84 * args.state.combat.chosen_ally, w: 256, h: 84, path: 'sprites/white_square.png', angle: 0, a: 2 * (args.state.tick_count % 32), r: 200, g: 200, b: 200, primitive_marker: :sprite }
    end

    # Silver amount
    primitives_to_render << { x: 1032, y: 340, w: 32, h: 32, path: 'sprites/icons/silver.png', primitive_marker: :sprite }
    primitives_to_render << { x: 1070, y: 366, text: args.state.player_party.silver, size_enum: 2, alignment_enum: 0, r: 200, g: 200, b: 50, primitive_marker: :label }

    # Current floor
    primitives_to_render << { x: 1032, y: 336, text: "Dungeon Floor #{args.state.dungeon.current_floor_number}", r: 180, g: 180, b: 180, primitive_marker: :label }

    # Current time
    primitives_to_render << { x: 1032, y: 316, text: args.state.calendar.day_time_string, r: 180, g: 180, b: 180, primitive_marker: :label }

    # Minimap
    primitives_to_render << { x: 1024, y: 0, w: 256, h: 256, path: :player_map_lower, primitive_marker: :sprite }
    primitives_to_render << { x: 1024, y: 0, w: 256, h: 256, a: 120, path: "sprites/compass_surround.png", primitive_marker: :sprite }
    compass_angle = case args.state.dungeon.current_floor.player_dir
    when 0
        0
    when 1
        270
    when 2
        180
    when 3
        90
    end
    target_angle = compass_angle
    target_angle = 360 if compass_angle == 0 && args.state.compass_direction > 180 
    target_angle = -90 if compass_angle == 270 && args.state.compass_direction == 0
    if args.state.compass_direction < target_angle
        args.state.compass_direction = [args.state.compass_direction + 4, target_angle].min
    else
        args.state.compass_direction = [args.state.compass_direction - 4, target_angle].max
    end
    args.state.compass_direction = args.state.compass_direction % 360
    primitives_to_render << { x: 1024, y: 0, w: 256, h: 256, angle: args.state.compass_direction, a: 80, path: "sprites/compass_arrow.png", primitive_marker: :sprite }
    primitives_to_render << { x: 1024, y: 0, w: 256, h: 256, path: :player_map_upper, primitive_marker: :sprite }

    #Log lines
    args.state.logs.entries.each_with_index do |s, i|
        primitives_to_render << { x: 5, y: 140 - 14 * i, text: s, size_enum: -2, alignment_enum: 0, r: 0, g: 100, b: 0, primitive_marker: :label }
    end

    outs.primitives << primitives_to_render
end

def tick args
    unless args.state.requires_loaded
        args.outputs.sprites << { x: 0, y: 0, w: 1280, h: 720, path: "sprites/icons/check.png" }
        return
    end
    start_time = Time.now()
    # Only log during unit testing
    if args.state.tick_count == 0
        # Only log during unit testing
        $gtk.log_level = :off
        # Note if we are HTML5 or not
        args.state.is_html = $gtk.platform == "Emscripten"

        # Default booleans
        args.state.in_combat = false
        args.state.moving = false
        args.state.turning = false
        args.state.turning_clockwise = false
        # Cheat defaults
        args.state.cheat_no_encounters = false
        args.state.cheat_debug_info = false

        # Hash keys are faster to check than running .include? on an array
        args.state.dialogs_that_are_in_guild = {}
        args.state.dialogs_that_are_in_guild[:guild_options] = true
        args.state.dialogs_that_are_in_guild[:guild_quest_list] = true
        args.state.dialogs_that_are_in_guild[:guild_training_list] = true
        args.state.dialogs_that_are_in_guild[:ack_already_doing_tome_quest] = true
        args.state.dialogs_that_are_in_guild[:ack_skill_not_enough_to_train] = true
        args.state.dialogs_that_are_in_inn = {}
        args.state.dialogs_that_are_in_inn[:inn_options] = true
        args.state.dialogs_that_are_in_inn[:confirm_rest_at_inn] = true
        args.state.dialogs_that_are_in_inn[:ack_not_enough_silver_to_rest] = true
        args.state.dialogs_that_are_in_inn[:innkeeper_chat] = true
        args.state.dialogs_that_are_in_temple = {}
        args.state.dialogs_that_are_in_temple[:temple_options] = true
        args.state.dialogs_that_are_in_temple[:choose_who_to_raise] = true
        args.state.dialogs_that_are_in_temple[:ack_noone_to_raise] = true
        args.state.dialogs_that_are_in_temple[:ack_cant_afford_raise] = true
        args.state.dialogs_that_are_in_temple[:confirm_raise] = true
        args.state.dialogs_that_are_in_shop = {}
        args.state.dialogs_that_are_in_shop[:shop_options] = true
        args.state.dialogs_that_are_in_shop[:shop_buy_panel] = true
        args.state.dialogs_that_are_in_shop[:confirm_buy_panel] = true
        args.state.dialogs_that_are_in_shop[:ack_not_enough_silver_to_buy_item] = true
        args.state.dialogs_that_are_in_shop[:confirm_sell_panel] = true
    end
    # SaveController is not in `defaults` because it needs access to args, not just state
    args.state.save_controller ||= SaveController.new(args)
    # Same for portraits
    args.state.portraits ||= PortraitController.new(args)
    args.state.audio ||= AudioController.new(args)
    defaults args.state unless args.state.is_initialised
    process_input args
    args.state.combat.update args if args.state.in_combat
    process_timers args
    update_particle_effects args
    args.state.audio.update
    render args, args.outputs
    args.state.animations.update

    # TODO: quests don't need updated every single frame
    args.state.quests.update(args) if args.tick_count % 32 == 0 && !args.state.moving && !args.state.turning

    # Debug output 
    if args.state.cheat_debug_info
        args.outputs.labels << [20, 200, "#{args.state.dungeon.current_floor.player_x} #{args.state.dungeon.current_floor.player_y} #{args.state.dungeon.current_floor.player_dir}", 3, 0, 100, 100, 100]
        args.outputs.debug << args.gtk.framerate_diagnostics_primitives
    end
    args.outputs.labels << [5, 715, $gtk.current_framerate, 2, 0, 100, 100, 100]
    args.state.method_measures.record_time(:tick, Time.now() - start_time)
end
