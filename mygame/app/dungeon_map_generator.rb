require 'app/dungeon_map.rb'

class DungeonMapGenerator

    NORTH = 0
    EAST = 1
    SOUTH = 2
    WEST = 3

    def initialize rand_seed = 1234
        @rnd = Random.new(rand_seed)
    end

    def reset_seed seed
        @rnd = Random.new(seed)
    end

    # Generate a dungeon floor 
    # @param  a          DragonRuby args
    # @param  opts       Hash of options
    # @return DungeonMap object with generated walls and furniture, and player location with their back to the entrance
    def generate_dungeon_floor a, opts = {}
        # Expected opts:
        # :first_floor true if this is the first floor (entrance instead of stairs back up)
        # :last_floor  true if this is the last floor (no stairs further down)
        # :width       width of the map.
        # :height      height of the map
        first_floor = opts[:first_floor] || false
        last_floor = opts[:last_floor] || false
        style = opts[:style] || :underhalls
        width = opts[:width] || 16 + @rnd.rand(12)
        height = opts[:height] || 16 + @rnd.rand(12)
        raise "Width #{width} is less than min 12" unless width >= 12
        raise "Height #{height} is less than min 12" unless height >= 12

        #a.log_info "Generating dungeon with width #{width} and height #{height}"
        tiles = []
        furniture = []
        # Initialize all tiles as 15 (all walls)
        height.times do |i|
            tiles[i] = []
            furniture[i] = []
            width.times { tiles[i] << 15; furniture[i] << [nil, nil, nil, nil] }
        end

        points_of_interest = []

        # Pick a random starting location (respecting margin of 1 outer tiles on the map)
        player_x = 1 + @rnd.rand(width - 2)
        player_y = 1 + @rnd.rand(height - 2)
        # Position player and place entrance
        player_dir = @rnd.rand(4)
        furniture[player_y][player_x][opposite(player_dir)] = first_floor ? :doorway_outside : :doorway_stairs_up

        # Dig out tiles starting from the player starting position
        # TODO: This works fairly well but there should be alternate algorithms e.g. to generate corridors and rooms.
        # Probably keep this code so there's a variety of dungeon styles.
        # Pick a number of tiles to attempt to dig out e.g. 1/3 of tiles in the map
        dig_out_count = (width * height * 0.3).ceil
        cur_x = player_x
        cur_y = player_y
        # This array will be a stack of dug out tiles. This lets the method track the current dig spot,
        # and backtrack if no more valid dig spots are possible.
        dug_tiles = [[cur_y, cur_x]]
        dig_out_count.times do
            direction_to_dig = pick_valid_direction(tiles, furniture, cur_x, cur_y)
            if direction_to_dig == -1
                if dug_tiles.length > 1
                    back = dug_tiles.pop
                    cur_y = back[0]
                    cur_x = back[1]
                else
                    a.gtk.log_error "No valid direction to dig and no previous tiles to backtrack to!"
                end
            else
                dug_tiles << dig(tiles, cur_x, cur_y, direction_to_dig)
                cur_x = dug_tiles.last[1]
                cur_y = dug_tiles.last[0]
            end
        end

        # Place the stairs down
        place_stairs_down(tiles, furniture, dug_tiles) unless last_floor

        # Add decorative furniture
        # Only consider dug-out tiles
        dug_tiles.each do |dug_tile|
            i = dug_tile[0]
            j = dug_tile[1]
            case style
            when :underhalls
                # 50% chance of no furniture
                if @rnd.rand(2) == 0
                    furniture_choice = @rnd.rand(3)
                    furniture_direction = random_undecorated_direction(tiles, furniture, j, i)
                    next if furniture_direction == -1
                    # 30% chance of ivy in a random direction
                    if furniture_choice == 0
                        furniture[i][j][furniture_direction] = :ivy
                    end
                    # 30% of weeds in a random direction
                    if furniture_choice == 1
                        furniture[i][j][furniture_direction] = :weeds
                    end
                    # 30% of columns on all walls
                    if furniture_choice == 2
                        # Place on all possible walls
                        # North
                        if (tiles[i][j]&1 == 1 || tiles[i - 1][j]&4 == 4) && furniture[i][j][NORTH].nil?
                            furniture[i][j][NORTH] = :column
                        end
                        # West
                        if (tiles[i][j]&2 == 2 || tiles[i][j - 1]&8 == 8) && furniture[i][j][WEST].nil?
                            furniture[i][j][WEST] = :column
                        end
                        # South
                        if (tiles[i][j]&4 == 4 || tiles[i + 1][j]&1 == 1) && furniture[i][j][SOUTH].nil?
                            furniture[i][j][SOUTH] = :column
                        end
                        # East
                        if (tiles[i][j]&8 == 8 || tiles[i][j + 1]&2 == 2) && furniture[i][j][EAST].nil?
                            furniture[i][j][EAST] = :column
                        end
                    end
                end
            when :catacombs
                # 50% chance of graves
                if @rnd.rand(2) == 0
                    # Try two different directions
                    2.times do
                        furniture_direction = random_undecorated_direction(tiles, furniture, j, i)
                        if furniture_direction != -1
                            furniture[i][j][furniture_direction] = @rnd.rand(2) == 0 ? :graves_1 : :graves_2
                        end
                    end
                # else 25% chance of another furniture
                elsif @rnd.rand(4) == 0
                    furniture_direction = random_undecorated_direction(tiles, furniture, j, i)
                    if furniture_direction != -1
                        furniture[i][j][furniture_direction] = [:skull_pile, :earth_crack, :supporting_bricks].sample
                    end
                end
            when :ice
                if @rnd.rand(3) == 0
                    3.times do
                        furniture_direction = random_undecorated_direction(tiles, furniture, j, i)
                        if furniture_direction != -1
                            furniture[i][j][furniture_direction] = [:icicles_1, :icicles_1, :icicles_2, :icicles_2, :fossil].sample
                        end
                    end
                end
            end
        end

        # Add some chests
        3.times { place_point_of_interest(points_of_interest, dug_tiles) }
        # Add some empty POIs to be re-used e.g. for quests
        3.times { place_point_of_interest(points_of_interest, dug_tiles, :empty) }

        furniture_count = 0
        furniture.each { |y| y.each { |tile| furniture_count += tile.compact.length } }
        a.log_info "Created dungeon with #{dug_tiles.count} spaces generated and #{furniture.count} furniture pieces."
        DungeonMap.new({tiles: tiles, furniture: furniture, points_of_interest: points_of_interest, style: style }, player_x, player_y, player_dir)
    end

    # Determine opposite direction
    # @param  dir int representing NORTH/WEST/SOUTH/EAST
    # @return int representing opposite direction of input
    def opposite dir
        (dir + 2) % 4
    end

    # Determine number of walls around a tile location.
    # Includes that tile plus the 4 adjacent to it
    # @return count of walls surrounding tiles[y][x]
    def num_walls tiles, x, y
        count = 0
        # 4 walls for this tile
        count += 1 if tiles[y][x]&1 == 1
        count += 1 if tiles[y][x]&2 == 2
        count += 1 if tiles[y][x]&4 == 4
        count += 1 if tiles[y][x]&8 == 8
        # West and east walls of tile to the north
        count += 1 if tiles[y - 1][x]&2 == 2
        count += 1 if tiles[y - 1][x]&8 == 8
        # North and south walls of tile to the west
        count += 1 if tiles[y][x - 1]&1 == 1
        count += 1 if tiles[y][x - 1]&3 == 3
        # West and east walls of tile to the south
        count += 1 if tiles[y + 1][x]&2 == 2
        count += 1 if tiles[y + 1][x]&8 == 8
        # North and south walls of tile to the east
        count += 1 if tiles[y][x + 1]&1 == 1
        count += 1 if tiles[y][x + 1]&3 == 3

        count
    end

    # For a given tile, pick a valid direction to dig.
    # A direction is not valid if:
    # - The given tile already has no wall in that direction
    # - It cuts into the edge of the map (maps have an unused 1 tile border)
    # - It would leave furniture without a wall
    # - It leaves any tile on the map with less than 4 surrounding walls
    # @return a random valid direction NORTH, WEST, SOUTH, EAST
    # @return -1 if no directions are valid
    def pick_valid_direction tiles, furniture, x, y
        valid = []
        # TODO: Need a nicer way to generate "rooms" and "corridors"
        # Initially just ensure tiles have at least 4 walls nearby
        min_walls = 8
        # Return immediately if the tile being considered has too few walls already
        #return -1 if num_walls tiles, x, y < min_walls

        # Consider North
        valid << NORTH if y > 1 &&
            tiles[y][x]&1 == 1 &&
            furniture[y][x][NORTH].nil? &&
            furniture[y - 1][x][SOUTH].nil? &&
            num_walls(tiles, x, y - 1) >= min_walls

        # Consider South
        valid << SOUTH if (y < tiles.length - 2) &&
            tiles[y][x]&4 == 4 &&
            furniture[y + 1][x][NORTH].nil? &&
            furniture[y][x][SOUTH].nil? &&
            num_walls(tiles, x, y + 1) >= min_walls

        # Consider West
        valid << WEST if x > 1 &&
            tiles[y][x]&2 == 2 &&
            furniture[y][x][WEST].nil? &&
            furniture[y][x - 1][EAST].nil? &&
            num_walls(tiles, x - 1, y) >= min_walls

        # Consider East
        valid << EAST if (x < tiles[0].length - 2) &&
            tiles[y][x]&8 == 8 &&
            furniture[y][x][EAST].nil? &&
            furniture[y][x + 1][WEST].nil? &&
            num_walls(tiles, x + 1, y) >= min_walls

        return -1 if valid.empty?
        valid[@rnd.rand(valid.length)]
    end

    # @return a direction picked at random that wall furniture could be placed
    # @return -1 if no direction is possible
    def random_undecorated_direction tiles, furniture, x, y
        valid = []
        # Consider North
        valid << NORTH if (tiles[y][x]&1 == 1 || tiles[y - 1][x]&4 == 4) &&
            furniture[y][x][NORTH].nil?
        # West
        valid << WEST if (tiles[y][x]&2 == 2 || tiles[y][x - 1]&8 == 8) &&
            furniture[y][x][WEST].nil?
        # South
        valid << SOUTH if (tiles[y][x]&4 == 4 || tiles[y + 1][x]&1 == 1) &&
            furniture[y][x][SOUTH].nil?
        # East
        valid << EAST if (tiles[y][x]&8 == 8 || tiles[y][x + 1]&2 == 2) &&
            furniture[y][x][EAST].nil?

        return -1 if valid.empty?
        valid[@rnd.rand(valid.length)]
    end

    # Dig into tiles at x,y at direction
    # It's assumed array boundaries are already checked at this point
    # Note that ^ is the XOR operator
    # @return [y,x] the new x,y reached by digging this direction
    def dig(tiles, x, y, direction)
        if direction == NORTH
            tiles[y][x] = tiles[y][x] ^ 1
            tiles[y - 1][x] = tiles[y - 1][x] ^ 4
            return [y - 1, x]
        end
        if direction == WEST
            tiles[y][x] = tiles[y][x] ^ 2
            tiles[y][x - 1] = tiles[y][x - 1] ^ 8
            return [y, x - 1]
        end
        if direction == SOUTH
            tiles[y][x] = tiles[y][x] ^ 4
            tiles[y + 1][x] = tiles[y + 1][x] ^ 1
            return [y + 1, x]
        end
        if direction == EAST
            tiles[y][x] = tiles[y][x] ^ 8
            tiles[y][x + 1] = tiles[y][x + 1] ^ 2
            return [y, x + 1]
        end
        raise "Unsupported dig direction #{direction}"
    end

    # Place stairs down
    # @param tiles     set of tiles
    # @param furniture set of existing furniture
    # @param tiles_dug list of dug tiles, the stairs will be placed as close to the end as possible
    def place_stairs_down(tiles, furniture, tiles_dug)
        raise "No possible tiles to place stairs down in!" if tiles_dug.nil? || tiles_dug.empty?
        dir = random_undecorated_direction tiles, furniture, tiles_dug[-1][1], tiles_dug[-1][0]
        if dir == -1
            # Call recursively with one less dug tile
            place_stairs_down(tiles, furniture, tiles_dug[0...-1])
        else
            furniture[tiles_dug[-1][0]][tiles_dug[-1][1]][dir] = :doorway_stairs_down
        end
    end

    def place_point_of_interest(points_of_interest, tiles_dug, type = :chest)
        free_spaces = tiles_dug[1...-1].select { |t| points_of_interest.select { |p| p.x != t[1] || p.y != t[0] } }
        raise "Want to add points of interest to the dungeon but dungeon has 0 free spaces" if free_spaces.empty?
        location = free_spaces[@rnd.rand(free_spaces.length)]
        points_of_interest << { x: location[1], y: location[0], type: type }
    end

    def serialize
        {}
    end

    def inspect
        self.to_s
    end

    def to_s
        "Dungeon Map Generator"
    end
end