class Creature

    attr_accessor :current_priority, :spec, :name, :tied_goal
    attr_reader :alive, :attack, :capabilities, :faction, :hp, :max_hp, :level, :priorities, :stats, :xp

    def initialize
        @name = "MISSINGNO"
        @alive = true
        @level = 1
        # TODO: Need full set of stats, not just "attack"
        @attack = 5
        @stats = {}
        @capabilities = []
        @priorities = []
        @current_priority = 0
        @faction = :monster
        # The quest goal this creature is tied to, or nil
        @tied_goal = nil
    end

    def serialize
        {}
    end

    def inspect
        self.to_s
    end

    def to_s
        "Creature #{@name} (level #{@level})"
    end

    def set_max_hp hp
        @max_hp = hp
        @hp = hp
    end

    def set_level l
        @level = l
    end

    def set_xp x
        @xp = x
    end

    def set_attack a
        @attack = a
    end

    def set_stats stats
        raise "#{stats} is not a hash" unless stats.is_a?(Hash)
        @stats = stats
    end

    def set_stat stat, amount
        raise "#{stat} is not a symbol" unless stat.is_a?(Symbol)
        @stats[stat] = amount
    end

    def apply_damage n
        @hp = [0, @hp - n].max
        @alive = false if @hp == 0
    end

    def time_until_turn
        1.5 + (5.0 / @stats[:speed]) + (rand(4) /  @stats[:speed])
    end

    # TODO: More accurate times based on stats etc
    def time_to_act action
        return 1 if action == :nothing
        return 5 if action == :magic
        return 3
    end

    def level_up!
        # Increase HP without necessarily refilling HP
        new_max_hp = [(@max_hp * 1.07).floor, @max_hp + 3].max
        old_max_hp = @max_hp
        @max_hp = new_max_hp
        @hp += new_max_hp - old_max_hp
        @attack = [(@attack * 1.05), @attack + 5].max
        @xp = [(@xp * 1.05), @xp + 5].max
        @level += 1
    end

    # Make this creature a boss
    # TODO: Should yield more silver and better loot
    def bossify!
        set_max_hp((@max_hp * 1.3).floor)
        set_attack((@attack * 1.5).floor)
        set_xp((@xp * 2.5).floor)
    end

    def set_faction faction
        @faction = faction
    end

    def set_capabilities caps
        @capabilities = caps
    end

    def set_priorities priorities
        @priorities = priorities
    end

    def next_priority
        raise "No priorities" if @priorities.nil? || @priorities.empty?
        @current_priority += 1
        @current_priority = 0 if @current_priority >= @priorities.length
    end

    def self.from_spec spec
        res = Creature.new
        res.spec = spec
        res.name = spec.name
        res.set_faction(spec.faction)
        res.set_max_hp(spec.base_hp)
        res.set_level(spec.level)
        res.set_xp(spec.base_xp)
        res.set_attack(spec.attack)
        res.set_stats({ might: 5, physicality: 5, speed: 5, personality: 5, mind: 5, acuity: 5 })
        
        return res
    end
end