# Define character classes
class ClassDefinitions

    attr_reader :classes

    def initialize
        @classes = {}
        [:cleric, :cutpurse, :hunter, :knight, :mage, :mercenary, :sage, :vikingr].each do |c|
            @classes[c] = { id: c, name: c.to_s.capitalize }
            # TODO: Add description strings
            @classes[c][:stats] = base_stats(c)
            @classes[c][:starting_skill_grades] = base_skill_grades(c)
            @classes[c][:starting_skill_levels] = {}
            # For each known skill, set level of that skill to 1
            # TODO: If character can start as experienced or master, level needs to be higher
            @classes[c][:starting_skill_grades].keys.each { |g| @classes[c][:starting_skill_levels][g] = 1 }
            # TODO: Add max skill grades per class
        end
    end

    def class_stats(cclass)
        @classes[cclass][:stats]
    end

    def base_stats(cclass)
        res = {}
        res[:might] = 5
        res[:physicality] = 5
        res[:speed] = 5
        res[:personality] = 5
        res[:mind] = 5
        res[:acuity] = 5
        case cclass
        when :cleric
            res[:mind] = 6
            res[:acuity] = 6
        when :cutpurse
            res[:might] = 4
            res[:speed] = 7
            res[:acuity] = 6
        when :hunter
            res[:physicality] = 6
            res[:speed] = 6
            res[:personality] = 6
            res[:acuity] = 6
        when :knight
            res[:might] = 7
            res[:physicality] = 6
            res[:mind] = 4
            res[:acuity] = 4
        when :mage
            res[:might] = 3
            res[:physicality] = 4
            res[:mind] = 8
            res[:acuity] = 6
        when :mercenary
            res[:might] = 6
            res[:physicality] = 6
            res[:speed] = 6
            res[:acuity] = 6
        when :sage
            res.keys.each { |s| res[s] = 4 }
        when :vikingr
            res[:might] = 6
            res[:physicality] = 6
            res[:speed] = 6
            res[:personality] = 6
            res[:mind] = 4
            res[:acuity] = 6
        end
        res
    end

    # Base skill grades for each skill for a class
    # Absence or :unskilled means no ability!
    def base_skill_grades(cclass)
        res = {}
        # All characters are a little bit adventurous
        res[:adventure] = :novice
        # All characters can punch!
        res[:unarmed] = :novice
        extra_novice_skills = case cclass
        when :cleric
            [:armour, :bio, :blunt, :dodge, :energy, :kinetics, :meditation, :neural, :ranged, :small_arms, :sword, :void]
        when :cutpurse
            [:armour, :blunt, :dodge, :leadership, :ranged, :small_arms, :sword]
        when :hunter
            [:armour, :dodge, :exploration, :ranged, :small_arms, :tactitian]
        when :knight
            [:armour, :bio, :blunt, :dodge, :leadership, :neural, :physique, :ranged, :shield, :small_arms, :sword, :tactitian]
        when :mage
            [:blunt, :energy, :kinetics, :meditation, :neural, :void]
        when :mercenary
            [:armour, :blunt, :dodge, :exploration, :leadership, :long_arms, :physique, :ranged, :shield, :small_arms, :sword, :tactitian]
        when :sage
            [:armour, :bio, :dodge, :energy, :exploration, :kinetic, :meditation, :neural, :void]
        when :vikingr
            [:armour, :bio, :blunt, :dodge, :energy, :exploration, :meditation, :physique, :neural, :ranged, :shield, :small_arms, :sword ]
        else
            []
        end
        extra_novice_skills.each { |s| res[s] = :novice }
        # TODO: Some classes should start with "skilled" skills not just novice
        res
    end

    # Max skill grades for each skill for a class
    # Absence or :unskilled means no upgrade from current ability!
    # :novice implies that the character can learn the skill from absence or unskilled
    def max_skill_grades(cclass)
        res = {}
        # All characters can get good at punching
        res[:unarmed] = :skilled
        case cclass
        when :cleric
            res[:bio] = :expert
            res[:energy] = :expert
            res[:kinetics] = :expert
            res[:meditation] = :expert
            res[:neural] = :expert
            res[:void] = :expert
        when :cutpurse
            res[:adventure] = :expert
            res[:dodge] = :expert
            res[:small_arms] = :master
        when :hunter
            res[:adventure] = :master
            res[:ranged] = :master
            res[:exploration] = :expert
            res[:tactitian] = :expert
        when :knight
            res[:adventure] = :expert
            res[:physique] = :expert
            res[:sword] = :master
            res[:shield] = :master
        when :mage
            res[:armour] = :novice
            res[:blunt] = skilled
            [:energy, :kinetics, :meditation, :neural, :void].each { |s| res[s] = :skilled}
        when :mercenary
            [:blunt, :dodge, :exploration, :long_arms, :physique, :ranged, :shield, :small_arms, :sword, :tactitian].each { |s| res[s] = :expert}
            res[:armour] = :master
            res[:leadership] = :master
        when :sage
            res[:dodge] = :skilled
            res[:exploration] = :expert
            res[:leadership] = :expert
            res[:tactitian] = :expert
            res[:void] = :skilled
            [:bio, :energy, :kinetic, :meditation, :neural].each { |s| res[s] = :master }
        when :vikingr
            res[:adventure] = :skilled
            res[:exploration] = :expert
            res[:long_arms] = :master
            res[:physique] = :expert
            res[:ranged] = :expert
            res[:shield] = :expert
            res[:small_arms] = :expert
            res[:sword] = :master
        end
        res
    end
end