# Class to chain syllables together given a table
# The table is a hash of hashes that maps each syllable to each possible next syllable
# Each "next possible syllable" has an integer percent probability, and all possible symbols have an integer sum of 100
class SyllableChain
    def initialize
        # TODO: Have an instance of Random here so the same words generate each game.
        @syllable_table = nil
    end

    def set_table table
        @syllable_table = table
    end

    # This will be expensive and should be used in tests, not in build
    # @return   String  Empty string if no issue, else string describing the first issue found
    def validate_table
        return "table is nil" unless @syllable_table
        return "table is not a hash" unless @syllable_table.is_a?(Hash)
        return "table is empty" if @syllable_table.empty?
        return "table is not a hash of only hashes" unless @syllable_table.select { |_,v| !v.is_a?(Hash)}.empty?
        return "table has no starter syllable" unless @syllable_table.has_key?(:start)
        # TODO: Test there are reachable :end syllables
        @syllable_table.keys.each do |k|
            sum = 0
            @syllable_table[k].keys.each do |j|
                sum += @syllable_table[k][j]
            end
            return "Syllable #{k} does not have options totalling to 100 (has #{sum})" unless sum == 100

            @syllable_table[k].keys.each do |j|
                next if j == :end
                raise "Syllable #{k} has an option #{j} that isn't listed in the table (#{k} has #{@syllable_table[k]})" unless @syllable_table[j]
            end
        end

        ""
    end

    def pick_next_syllable current_syllable, prefer_end = false, prefer_lengthen = false
        choices = {}
        # If we prefer to end (near preferred length) then select from possible next syllables that have :end as an option
        # The `select` here excludes :end as a key because this does not exist as a key in the table.
        # TODO: Ideally never end on a syllable that isn't supposed to end a word
        raise "No such syllable #{current_syllable}" unless @syllable_table.has_key?(current_syllable)
        choices = @syllable_table[current_syllable].select { |k,_| k != :end && @syllable_table[k].has_key?(:end) } if prefer_end
        # If we prefer to lengthen the word then do not pick :end if possible
        choices = @syllable_table[current_syllable].reject { |k,_| k == :end }
        choices = @syllable_table[current_syllable] if choices.empty?
        raise "No choice for #{current_syllable}" if (choices.nil? || choices.empty?)
        next_choice_roll = 0.25 + rand(100.0) # Allow for syllables with 0.25 precision
        next_choice_considered = 0
        next_syllable = nil
        choices.each do |k,v|
            next_syllable = k
            raise "Hash: #{v}" if v.is_a?(Hash)
            next_choice_considered += v
            break if next_choice_considered > next_choice_roll
        end
        next_syllable || raise("No valid next syllable for current syllable #{current_syllable} with number roll #{next_choice_roll}")
    end

    def generate_word preferred_length = 10
        res = ""
        next_syllable = pick_next_syllable(:start)
        while next_syllable != :end && res.length <= preferred_length
            raise "#{next_syllable} is not a String" unless next_syllable.is_a?(String)
            res += next_syllable
            next_syllable = pick_next_syllable(next_syllable, preferred_length - res.length < 2, res.length < preferred_length + 2 )
        end
        res
    end
end
