class Quest

    attr_accessor :id, :title, :dungeon, :requirements, :goals, :rewards, :is_skill_promotion_quest

    def initialize

    end

    def serialize
        {}
    end

    def inspect
        self.to_s
    end

    def to_s
        "Quest"
    end

    # Static method to generate a Quest object from a spec
    # spec expected to be like
    # { 
    #     id: 1, 
    #     title: "Kill the big ratkin",
    #     requirements: {event_passed: 3},
    #     goals: [
    #       {
    #           phase: 1,
    #           type: :kill,
    #           desc: "Find and kill the ratkin",
    #           dungeon_id: 1, 
    #           floor: 1,
    #           base_level: 5,
    #           bestiary_id: 5,
    #           name: "Azdpar the biting"
    #       }
    #     ],
    #     rewards: { silver: 50, xp: 100 }
    # },
    # {
    #     id: 2,
    #     title: "Rescue the villager",
    #     requirements: {event_passed: 4, quest_complete: 1},
    #     goals: [
    #       {
    #           phase: 1,
    #           type: :reach_poi,
    #           desc: "Find the lost villager",
    #           dungeon_id: 1, 
    #           floor: 2,
    #           name: "Lost Villager"
    #       }
    #     ],
    #     rewards: { silver: 100, xp: 150 }
    # }
    def self.from_spec spec
        [:id, :title, :requirements, :goals, :rewards].each { |r| raise "Required key :#{r} missing from Quest spec" unless spec.key? r}
        raise ":requirements must be a hash" unless spec[:requirements].is_a? Hash
        spec[:requirements].each do |r, v|
            raise "Unsupported requirement type" unless [:event_passed, :quest_complete, :quest_ended].include? r
        end
        res = Quest.new
        res.id = spec[:id]
        res.title = spec[:title]
        res.requirements = spec[:requirements]
        res.goals = spec[:goals]
        res.rewards = spec[:rewards]
        res.is_skill_promotion_quest = spec.keys.include?(:is_skill_promotion_quest) && spec[:is_skill_promotion_quest] == true
        res.dungeon = spec[:dungeon]

        res
    end
end