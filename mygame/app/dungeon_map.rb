# Class representing a single-floor dungeon map including tiles array, rendering and collision
#
# Tiles have property bits:
# 1 indicates a north wall
# 2 indicates a west wall
# 4 indicates a south wall
# 8 indicates an east wall
#
# Directions are stored as int as:
# 0 - North
# 1 - East
# 2 - South
# 3 - West

DEGREES_TO_RADIANS = Math::PI / 180.0

class DungeonMap

    attr_reader :furniture, :tiles, :width, :height, :style
    attr_accessor :player_x, :player_y, :player_dir, :points_of_interest

    NORTH = 0
    EAST = 1
    SOUTH = 2
    WEST = 3

    # TODO: This would expect to be passed a file ref to load as the map instead of specific args
    def initialize(data=nil, player_x=nil, player_y=nil, player_dir=nil)
        @distance_dimming ||= 70

        if data
            @tiles = data[:tiles]
            @style = data[:style] || :underhalls
            @wall_sprite = case @style
            when :underhalls
                'sprites/walls/brick_wall64.png'
            when :catacombs
                'sprites/walls/catacomb_wall64.png'
            when :ice
                'sprites/walls/ice64.png'
            end
            @furniture = data[:furniture]
            @points_of_interest = data[:points_of_interest]
            @height = tiles.length
            @width =  tiles[0].length
            raise ArgumentError "DungeonMap initialised with tiles but without player_x" unless player_x
            @player_x = player_x
            raise ArgumentError "DungeonMap initialised with tiles but without player_y" unless player_y
            @player_y = player_y
            @player_dir = player_dir || 0
        else # Default map
            @width = 7
            @height = 7
            @tiles = [[0xF,0x4,0x4,0x5,0x4,0x4,0xF],
                      [0x4,0x6,0x8,0x6,0x2,0x4,0x2],
                      [0x4,0x2,0x1,0x1,0x3,0x0,0x2],
                      [0x4,0x2,0xF,0x0,0xF,0x0,0x2],
                      [0x4,0x2,0x0,0x0,0xF,0x0,0x2],
                      [0x4,0x2,0x8,0x0,0x0,0x0,0x2],
                      [0xF,0x1,0x1,0x1,0x1,0x1,0xF]]
            @furniture = []
            @height.times do |y|
                @furniture[y] = []
                @width.times { furniture[y] << [nil, nil, nil, nil] }
            end
            @furniture[5][3][2] = :doorway_outside
            @furniture[3][3][1] = :column
            @furniture[3][3][3] = :column
            @furniture[3][2][0] = :column
            @furniture[4][1][3] = :ivy
            @points_of_interest = []
            @player_x = 3
            @player_y = 5
            @player_dir = 0
        end
    end
    
    # Return if a move is valid or not
    def valid_move? a,fx,fy,tx,ty
        if fx == tx && ty == fy - 1
            return (ty >= 0 && @tiles[ty][tx]&4!=4 && @tiles[fy][fx]&1!=1)
        end
        if fx == tx && ty == fy + 1
            return (ty < @height && @tiles[ty][tx]&1!=1 && @tiles[fy][fx]&4!=4)
        end
        if fy == ty && tx == fx - 1
            return (tx >= 0 && @tiles[ty][tx]&8!=8 && @tiles[ty][fx]&2!=2)
        end
        if fy == ty && tx == fx + 1
            return (tx < @width && @tiles[ty][tx]&2!=2 && @tiles[ty][fx]&8!=8)
        end
        # default to false
        return false
    end

    # Return if the player is facing the dungeon exit
    def exit_ahead?
        @furniture[@player_y][@player_x][@player_dir] == :doorway_outside
    end

    def stairs_down_ahead?
        @furniture[@player_y][@player_x][@player_dir] == :doorway_stairs_down
    end

    def stairs_up_ahead?
        @furniture[@player_y][@player_x][@player_dir] == :doorway_stairs_up
    end

    # Flip the direction the player is facing.
    # Used on floor change to ensure player faces away from the stairwell.
    def flip_direction
        @player_dir = (@player_dir + 2) % 4
    end

    def poi_underfoot
        return @points_of_interest.find { |p| p.x == @player_x && p.y && p.y == @player_y }
    end

    def north_wall? x, y
        @tiles[y][x] & 1 == 1 || @tiles[y - 1][x] & 4 == 4
    end

    def west_wall? x, y
        @tiles[y][x] & 2 == 2 || @tiles[y][x - 1] & 8 == 8
    end

    def south_wall? x, y
        @tiles[y][x] & 4 == 4 || @tiles[y + 1][x] & 1 == 1
    end

    def east_wall? x, y
        @tiles[y][x] & 8 == 8 || @tiles[y][x + 1] & 2 == 2
    end

    def update_render args
        start_time = Time.now()
        # Render background
        args.render_target(:dungeon_view).solids << { x: 0, y: 142, w: 1024, h: 576, r: 1, g: 1, b: 1, a: 255 }
        sprites_to_render = []
        sprites_to_render << { path: 'sprites/gradiant2.png',
                     x: 0, y: 480, w: 1024, h: 240,
                     flip_vertically: true}
        sprites_to_render << { path: 'sprites/gradiant2.png',
                     x: 0, y: 142, w: 1024, h: 240,
                     flip_vertically: false}
        sprites_to_render << get_raycast_sprites(args)
        # Add vignette
        sprites_to_render << { x: 0, y: 142, w: 1042, h: 576, a: 200, path: "sprites/vignette.png"}
        args.render_target(:dungeon_view).sprites.clear
        args.render_target(:dungeon_view).sprites << sprites_to_render
        args.state.method_measures.record_time(:update_dungeon_render, Time.now() - start_time)
    end

    # Constrain an angle in degrees between 0 and 360
    def bound_degrees angle
        return angle - 360 if angle >= 360
        return angle + 360 if angle < 0
        angle 
    end

    # @return Array of sprites representing vertical strips to render the viewport of this dungeon
    def get_raycast_sprites args
        start_time = Time.now()

        res = []

        # Determine player view angle
        pa = case @player_dir
        when NORTH
            90
        when WEST
            180
        when SOUTH
            270
        when EAST
            0
        end

        # If turning then turn by the appropriate amount
        if args.state.turning
            if args.state.turning_clockwise
                pa += 90 - args.state.turning_counter
            else
                pa += 270 + args.state.turning_counter
            end
        end

        # Player position, given that we'll treat a tile in the map as being 64 units across
        px = @player_x * 64 + 32.0
        py = @player_y * 64 + 32.0
        # If moving then move the player back by the amount they've moved
        if args.state.moving
            # moving_duration is 16. Multiply by 4 to scale to a tile (64 units)
            py += Math.sin(pa * DEGREES_TO_RADIANS) * 4 * (args.state.moving_duration - args.state.moving_counter)
            px -= Math.cos(pa * DEGREES_TO_RADIANS) * 4 * (args.state.moving_duration - args.state.moving_counter)
        end

        # Rendering 120 degrees, start 60 degrees beyond player view angle
        render_angle = bound_degrees(pa + 60)

        # depths of each array hit, used after drawing walls to draw sprites
        depths = []

        # Iterate across 240 rays (1024 pixel display / 120 degree view, 0.5 degree increments)
        for i in 0...240 do

            a = render_angle * DEGREES_TO_RADIANS

            # Furniture for this ray hit
            f = nil

            # Find Vertical hit
            depth_checked = 0               # How many tiles we've tried checking
            depth_limit = 18                # How far to keep looking (this needs to be large enough for the max number of tiles a ray might cast over)
            distance_vertical = 1000000     # The distance found, initially massive (the real hit will be horizontal)
            looking_left = false
            looking_right = false
            looking_up = false
            looking_down = false
            #Determine a hit spot. Keep huge distance_vertical if player is looking directly left or right
            if (Math.cos(a) < -0.001)           # Ray is looking left-ward ( 91 degrees = 1.58825 rad = -0.0174 as cos)
                rx = px - px % 64 - 0.0001       # Hit left of square player is on (plus a little more to push into tile to the left)
                ry = (px - rx)*Math.tan(a)+py   # Hit applicable spot in y
                xo = -64                        # Next step is -64 in x
                yo = -xo * Math.tan(a)          # Applicable next step in y
                looking_left = true
            elsif (Math.cos(a) > 0.001)         # Ray is looking right-ward ( 89 degrees = 1.55344 rad = 0.0174 as cos )
                rx = px - px % 64 + 64          # Hit right of square player is on
                ry = (px - rx)*Math.tan(a)+py   # Hit applicable spot in y
                xo = 64                         # Next step is +64 in x
                yo = -xo * Math.tan(a)          # Applicable next step in y
                looking_right = true
            else
                # If looking directly up or down, we will never reasonably hit a vertical wall.
                # Keep the default massive vertical distance and move on to checking for a horizontal wall,
                # assuming we will get a hit that way.
                rx = px
                ry = py
                depth_checked = depth_limit
            end

            hit = false
            while (depth_checked < depth_limit) && !hit do
                map_x = (rx / 64).to_i
                map_y = (ry / 64).to_i
                # If we're looking leftward then we might hit the east wall of the tile being considered
                # OR the west wall of the tile to the right of it
                # Note that at this point we might be scanning outside the map, so here we check if the tile is within map dimensions.
                # TODO: Might it be quicker to exit the loop as soon as one considered position is out of bounds? 
                if (map_x >= 0 && map_x <= (@width - 1) && map_y >= 0 && map_y <= (@height - 1)) &&
                   ((looking_left && east_wall?(map_x, map_y)) || (looking_right && west_wall?(map_x, map_y)))
                    # Exit the loop, we have a hit
                    hit = true
                    # Store the distance to the hit.
                    distance_vertical = Math.cos(a) * (rx - px) - Math.sin(a) * (ry - py)
                elsif render_angle % 45 <= 0.5 && (map_x > 0 && map_x < (@width - 1) && map_y > 0 && map_y < (@height - 1))
                    # Corner cases (literally) where the raycasting misses because it passes through a corner into an empty tile diagonally adjacent.
                    # If the raycast is close to a corner then consider the cast a hit if a side wall touches that corner.
                    # Need to consider the 4 cases of looking left and looking right, and seeing the top corner or bottom corner of the tile.
                    # Also note that the map bounds here exclude the edge tiles of the map since we will consider surrounding tiles.
                    # Also note: Needs further testing but seemingly this does not need repeated for the horizontal casting.
                    # TODO: Make this conditional on player facing a wall, to avoid edge cases where the corner should not be added in.
                    if (looking_left && (ry % 64).to_i == 63 && (south_wall?(map_x + 1, map_y)) ||
                        looking_left && (ry % 64).to_i == 0 && (north_wall?(map_x + 1, map_y)) ||
                        looking_right && (ry % 64).to_i == 63 && (south_wall?(map_x - 1, map_y)) ||
                        looking_right && (ry % 64).to_i == 0 && (north_wall?(map_x - 1, map_y)))
                        # Exit the loop, we have a hit
                        hit = true
                        # Store the distance to the hit.
                        distance_vertical = Math.cos(a) * (rx - px) - Math.sin(a) * (ry - py)
                    end
                end

                unless hit
                    # Move x and y by the calculated increments and check again.
                    rx += xo
                    ry += yo
                    depth_checked += 1
                end
            end
            vx=rx
            vy=ry

            # Find Horizontal hit
            depth_checked = 0               # How many tiles we've tried checking
            distance_horizontal = 1000000
            inverse_tan = (1.0/Math.tan(a))
            #Determine a hit spot. Keep huge distance_horizontal if player is looking directly up or down
            if(Math.sin(a) > 0.001)     # Looking up
                ry = py - (py % 64) - 0.0001
                rx = (py - ry) * inverse_tan + px
                yo = -64
                xo = -yo * inverse_tan
                looking_up = true
            elsif(Math.sin(a) < -0.001) # Looking down
                ry = py - (py % 64) + 64
                rx = (py - ry) * inverse_tan + px
                yo = 64
                xo = -yo * inverse_tan
                looking_down = true
            else                        # Looking directly up or down and never hitting the top/bottom of another tile
                rx = px
                ry = py
                depth_checked = depth_limit
            end

            while depth_checked < depth_limit
                map_x = (rx / 64).to_i
                map_y = (ry / 64).to_i
                # If looking upward then check for a south wall on the hit point or a north wall on the tile below it
                # Note that at this point we might be scanning outside the map, so here we check if the tile is within map dimensions.
                if (map_x >= 0 && map_x <= (@width - 1) && map_y >= 0 && map_y <= (@height - 1)) && 
                    ((looking_up && south_wall?(map_x, map_y)) || (looking_down && north_wall?(map_x, map_y)))
                    # Hit!
                    depth_checked = depth_limit
                    distance_horizontal = Math.cos(a) * (rx - px) - Math.sin(a) * (ry - py)
                else
                    # Step to next possible tile hit
                    rx += xo
                    ry += yo
                    depth_checked += 1
                end
            end

            # This is a valid assertion but for speed leave it commented out unless debugging.
            #raise "No sensible hit either vertical (#{distance_vertical} to (#{vx},#{vy})) or horizontal (#{distance_horizontal} to (#{rx},#{ry}))" unless (distance_horizontal < 1000000 || distance_vertical < 1000000)

            # Actual hit is shortest of Horizontal or Vertical
            if distance_vertical < distance_horizontal
                rx = vx
                ry = vy
                distance = distance_vertical
            else
                distance = distance_horizontal
            end
            

            # Store distance of hit for this ray, will be needed later when drawing sprites
            depths << distance

            # Draw the ray
            ## Fix "fisheye" by making distance to hit point dist = dist * cos(player_angle - raycast_angle)
            angle_delta = bound_degrees(pa - render_angle) * DEGREES_TO_RADIANS
            distance = distance * Math.cos(angle_delta) * 0.3

            ## TODO: 12 * 576 is a magic number - document/calculate properly.
            ## TODO: The strip height can get very large for nearby walls, is this an issue?
            ## It might be wiser to limit the strip to the viewport height then calculate the subsection of the wall texture height to draw on it...
            ## TODO: 16000 is also a magic number
            strip_height = (12 * 576) / distance - (16000 / distance**2)
            shading = 255 - distance * 3.0
            # Determine section of source texture to use
            tx = distance_vertical > distance_horizontal ? (rx.to_i % 64).to_i : (ry.to_i % 64).to_i
            # If player is looking backwards towards a tile then then flip the side of the texture to sample
            tx = 63 - tx if (render_angle > 180 && distance_vertical > distance_horizontal)
            tx = 63 - tx if (render_angle > 90 && render_angle < 270 && distance_vertical < distance_horizontal)
            res << { x: i * 4.28, y: 142 + (576 - strip_height) / 2, w: 4.28, h: strip_height, path: @wall_sprite,
                    source_x: tx, source_w: 1,
                    r: shading, g: shading, b: shading }

            ## Draw furniture by drawing a strip of the furniture on top of the strip of wall
            ## Furniture directions are stored as int as:
            ## 0 - North
            ## 1 - East
            ## 2 - South
            ## 3 - West
            ## Because the raycasting casts onto/into the first tile that is solid, we need to consider
            ## the tile beside the raycast hit for the furniture in view. So determine deltas for x and y...
            fdx = 0
            fdy = 0
            ## ...for example, if the hit for this ray was when scanning horizontally while angled upwards,
            ## then the furniture we want is on the north (0) wall of the empty tile below it (y + 1)
            if distance_horizontal < distance_vertical && looking_up
                dir = 0
                fdy = 1
            end
            if distance_vertical < distance_horizontal && looking_left 
                dir = 3
                fdx = 1
            end
            if distance_horizontal < distance_vertical && looking_down
                dir = 2
                fdy = -1
            end
            if distance_vertical < distance_horizontal && looking_right
                dir = 1
                fdx = -1
            end 

            # These are valid assertions but for performance leave them commented out unless debugging
            #raise "dir is nil!" unless dir
            #fx = (rx / 64).to_i + fdx
            #fy = (ry / 64).to_i + fdy
            #raise "Looking for furniture out of bounds [#{fy}][#{fx}] with distance_vertical #{distance_vertical} distance_horizontal #{distance_horizontal} hit rx #{rx} hit ry #{ry}" unless fy >= 0 && fy < @furniture.length && fx >= 0 && fx < @furniture[fy].length

            if f ||= @furniture[(ry / 64).to_i + fdy][(rx / 64).to_i + fdx][dir]
                res << { x: i * 4.28, y: 142 + (576 - strip_height) / 2, w: 4.28, h: strip_height, path: "sprites/walls/#{f}64.png",
                    source_x: tx, source_w: 1,
                    r: shading, g: shading, b: shading }
            end

            # Next angle
            render_angle = bound_degrees(render_angle - 0.5)
        end

        # Draw sprites
        ## Proper method
        ### Determine POI(s) that might be in view
        ### Position the sprite in x,y,z
        ### Rotate around the player
        ### Convert from map x,y,z to screen space
        ### Determine scale
        ### Draw in vertical strips, skip the strip if the sprite's distance is further than the wall strip for this X
        ## Easy method to be used initially
        ### Determine POI(s) that might be in view
        ### Render the sprite as if it were in the same place as the wall of the tile it's on
        ### Use rough/magic numbers to slightly scale and offset the sprite to account for it being in the middle of the tile space.
        ## Use a temp array to hold the sprites, sort by Z before shovelling into the resulting sprites
        #sprites_to_draw = []

        # TODO: This sort is expensive and could consider some optimisation like only doing it when a tile move completes
        @points_of_interest.each do |poi|
            poi[:dist] = args.geometry.distance([px,py],[poi[:x] * 64 + 32 , poi[:y] * 64 + 32]).abs
        end
        @points_of_interest = @points_of_interest.sort_by { |t| t[:dist] }.reverse

        @points_of_interest.each do |poi|
            # :empty POIs have nothing to see
            next if poi[:type] == :empty
            
            # Determine sprite
            poi_sprite = case poi.type
            when :chest
                if args.state.opened_chests.find { |c| c[:x] == poi[:x] && c[:y] == poi[:y] && c[:dungeon] == args.state.dungeon.id && c[:floor] == args.state.dungeon.current_floor_number }
                    'sprites/pois/chest_opened.png'
                else
                    'sprites/pois/chest_closed.png'
                end
            when :quest
                'sprites/pois/quest.png'
            end

            # If the sprite is "underfoot", draw a static sprite
            if poi[:dist] < 10
                res << {
                    x: 0,
                    y: -55,
                    w: 1024,
                    h: 1024,
                    path: poi_sprite
                }
                next
            end

            poi_x = poi[:x] * 64 + 32.0
            poi_y = poi[:y] * 64 + 32.0

            # Hack for sprites not appearing properly diagonally; only render them dead ahead
            next if (args.geometry.angle_to([px, py], [poi_x, poi_y]) - pa).abs % 90 > 40

            # Get the POI's position relative to the player position and angle
            tx = poi_x - px
            ty = poi_y - py
            cs = Math.cos(pa * Math::PI / 180)
            sn = Math.sin(pa * Math::PI / 180)
            dx = ty * cs + tx * sn
            dy = tx * cs - ty * sn

            # The next 5 lines determine the screen x and y of (the center of) the entity, and a scale
            next if dy == 0 # Avoid invalid Infinity/NaN calculations if the projected Y is 0
            ody = dy
            dx = dx*512/(dy) + 512
            dy = 256/dy + 256
            scale = 128.0*96/(ody/2)

            # Distance fading
            fading = 255 / [(poi[:dist]) * 3, 1].max

            # Now we know the x and y on-screen for the entity, and its scale, we can draw it.
            # Simply drawing the sprite on the screen doesn't work in a raycast view because the entity might be partly obscured by a wall.
            # Instead we draw the entity in vertical strips, skipping strips if a wall is closer to the player on that strip of the screen.

            # Since dx stores the center x of the enemy on-screen, we start half the scale of the entity to the left of dx
            x = dx - scale/2
            next if (x > 960 or (dx + scale/2 <= 180)) # Skip rendering if the X position is entirely off-screen
            strip = 0                    # Keep track of the number of strips we've drawn
            strip_width = scale / 64     # Draw the sprite in 64 strips
            sample_width = 1             # For each strip we will sample 1/64 of sprite image, here we assume 64x64 sprites

            #puts "Drawing chest"
            until x >= dx + scale/2 do
                if x > 0 && x < 960
                    # Here we get the distance to the wall for this strip on the screen
                    # The 4.266 magic number is because we have 240 samples over 1024 pixels
                    wall_depth = depths[(x/4.266).to_i]
                    if ((poi[:dist] < wall_depth))
                        #raise "#{poi[:dist]}, #{wall_depth}"
                        #puts "player: #{px},#{py} poi: #{poi_x},#{poi_y}, sample width: #{sample_width}"
                        res << {
                            x: x,
                            y: dy + 160 - scale * 0.45,
                            w: strip_width,
                            h: scale,
                            path: poi_sprite,
                            source_x: strip * sample_width,
                            source_w: sample_width,
                            r: 255 * fading,
                            g: 255 * fading,
                            b: 255 * fading
                        }
                    end
                end
                x += strip_width
                strip += 1
            end
        end

        args.state.method_measures.record_time(:get_dungeon_raycast_sprites, Time.now() - start_time)

        res
    end

    def serialize
        {}
    end

    def inspect
        self.to_s
    end

    def to_s
        "Dungeon Map"
    end
end