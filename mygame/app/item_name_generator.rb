class ItemNameGenerator
    # Make gtk available for logging
    attr_gtk

    @@quality_modifiers = [
        [1, "Lowly"], [2, "Paltry"], [3, "Mediocre"], [4, "Humble"], [6, "Middling"], [8, "Fair"],
        [10, "Second-Rate"], [12, "Average"], [14, "Common"], [16, "So-So"], [19, "Simple"], [22, "Adequate"],
        [26, "Moderate"], [27, "Decent"], [28, "Standard"], [29, "Worthy"], [30, "Dependable"], [31, "Solid"],
        [32, "Dandy"], [33, "Fine"], [34, "Great"], [35, "Superb"], [36, "Remarkable"], [37, "Rare"],
        [38, "Superior"], [39, "Exceptional"], [40, "Peerless"], [41, "Excellent"], [42, "Magnificent"],
        [43, "Brilliant"], [44, "Supreme"], [45, "Legendary"], [46, "Ultimate"], [47, "Almighty"]
    ]

    @@junk_names = [[1, "Empty Bottle"], [3, "Childs Toy"], [4, "Antique Book"], [5, "Ball bearing"], [5, "Merchant Invoices"], [7, "Glass Shard"]]
    @@blunt_names = [
        [1, "Rusty Mace"], [2, "Cudgel"], [3, "Mace"], [4, "Club"], [6, "Morning Star"], [12, "Flail"], 
        [24, "Spiked Mace"], [64, "Nunchaku"], [96, "Chain Sickle"], [128, "Monk's Mace"], 
        [150, "Kusarigama"], [180, "Sansetsukon"], [200, "Lord's Mace"]
    ]
    @@ranged_names = [
        [1, "Practice Bow"], [2, "Bow"], [3, "Longbow"], [4, "Compound Bow"], [6, "Hunting Bow"], 
        [8, "Yew Longbow"], [12, "Composite Bow"], [30, "Holmegaard Bow"], [45, "Recurve"], 
        [70, "Yew Longbow"], [90, "Elven Bow"], [110, "Yumi"], [140, "Sniping Bow"], [180, "Hell Bow"], 
        [200, "Balore's Longbow"]
    ]
    # TODO: Small arms isn't just daggers and long arms aren't just spears! Weapon's type should not be == the skill it uses
    @@small_arms_names = [
        [1, "Knife"], [2, "Cestus"], [3, "Dagger"], [4, "Combat Knife"], [5, "Machete"], [6, "Assassins Blade"],
        [10, "Baselard"], [25, "Tanto"], [36, "Dirk"], [48, "Misericorde"], [60, "Jackblade"], 
        [70, "Main-Gauche"], [80, "Stiletto"], [90, "Hapkido Dagger"], [100, "Khukri"], [110, "Pugio Dagger"], 
        [150, "Holbein Blade"]
    ]
    @@long_arms_names = [
        [1, "Bamboo Spear"], [2, "Fishing Spear"], [3, "Spear"], [4, "Troglodyte Spear"], [5, "Infantry Spear"], 
        [8, "Halberd"], [12, "Pole-Arm"], [25, "Pike"], [40, "Pila"], [55, "Lance"], [75, "Iklwa"], 
        [90, "Naginata"], [110, "Obsidian Spear"], [130, "Spear Of Lugh"], [160, "Holy Lance"], [180, "Gáe Bulg"], 
        [200, "Gungnir"]
    ]
    @@sword_names = [
        [1, "Training Sword"], [2, "Short Sword"], [3, "Bastard Sword"], [4, "Infantry Sword"], [6, "Cutlass"],
        [12, "Rapier"], [18, "Scimitar"], [24, "Hunter Sword"], [36, "Saber"], [42, "Gladius"], [68, "Tulwar"],
        [85, "Knight's Sword"], [100, "Katana"], [120, "Wind Sword"], [140, "Nodachi"], [160, "Demon Blade"],
        [180, "Void Foil"], [200, "Lord's Blade"]
    ]
    @@light_shield_names = [
        [1, "Training Shield"], [2, "Small Shield"], [4, "Buckler"], [16, "Targe"], 
        [32, "Aegis"], [64, "Yetholm Shield"]
    ]
    @@medium_shield_names = [
        [1, "Round Shield"], [2, "Buckler"], [4, "Targe"], [8, "Heater Shield"], [16, "Kite Shield"], 
        [32, "Knight's Shield"],  [128, "Lord's Shield"]
    ]
    @@heavy_shield_names = [
        [1, "Infantry Shield"], [4, "Tower Shield"], [16, "Pavise"], [64, "Dragon Shield"]
    ]
    @@light_armour_names = [
        [1, "Padded Vest"], [4, "Leather Cuirass"], [8, "Boiled Leather Cuirass"], [32, "Elven Mail"], [128, "Carburised Cuirass"]
    ]
    @@medium_armour_names = [
        [1, "Bronze Cuirass"], [5, "Chainmail Armour"], [32, "Elven Chainmail"], [64, "Mycenaean Plate"], 
        [128, "Royal Chainmail"], [256, "Daemon Armour"]
    ]
    @@heavy_armour_names = [
        [1, "Iron Cuirass"], [4, "Steel Cuirass"], [8, "Dwarven Armour"], [16, "Splintmail Armour"], 
        [32, "Plate Armour"], [64, "Dwarven Plate"], [128, "Heavenly Plate"], [150, "Dragonscale"], 
        [200, "Lords Armour"]
    ]

    @@amulet_names = [
        [1, "Necklace"], [2, "Amulet"], [3, "Charm"], [6, "Medal"], [10, "Talisman"], [20, "Pendant"],
        [40, "Bell"], [60, "Sigil"], [80, "Wizard Amulet"], [100, "Meket"]
    ]

    @@footwear_names = [
        [1, "Sandals"], [2, "Shoes"], [4, "Leather Shoes"], [8, "Boots"], [16, "Chausses"],
        [24, "Infantry Boots"], [50, "High Boots"], [80, "Elven Boots"], [100, "Sabatons"], [120, "Dragonscale Boots"]
    ]

    @@gauntlet_names = [
        [1, "Villager Gloves"], [2, "Dress Gloves"], [4, "Cowhide Gloves", ], [8, "Goatskin Gloves"],
        [14, "Leather Gauntlets"], [20, "Splint Gauntlets"], [30, "Wizard Gloves"], [40, "War Gauntlets"],
        [60, "Dragonskin Gloves"]
    ]

    @@helmet_names = [
        [1, "Villager Hat"], [2, "Shepherd Cap"], [4, "Spangenhelm"], [8, "Kettle Helm"], [16, "Sallet"], 
        [32, "Morion"], [64, "Great Helm"], [80, "Bascinet"]
    ]

    @@ring_names = [
        [1, "Ring"], [4, "Signet Ring"], [8, "Platinum Ring"], [16, "Wizards Band"], [32, "Claddagh Ring"],
        [64, "Ancient Ring"]
    ]

    # @param level   the level value of the item. The name will refect the value
    # @param type    the type of the item e.g. armour, weapon, junk
    # @param variant the variant of the item type e.g. :sword for a :weapon
    def self.random_name level, type, variant = nil
        case type
        when :junk
            # Simply return the junk name with greatest value <= level
            # It is assumed the item generator is choosing a variety of levels,
            # and this class can repeatedly give the same name for items at the same level
            highest_usable_level = (@@junk_names.map { |n| n[0] }.reject { |l| l > level }).max
            (@@junk_names.find { |n| n[0] == highest_usable_level })[1]
        when :armour
            # Return a name that reflects the level of the armour
            word_set = case variant
            when :light_armour
                @@light_armour_names
            when :medium_armour
                @@medium_armour_names
            when :heavy_armour
                @@heavy_armour_names
            else
                raise "Asked to generate random name for unhandled armour variant #{variant}"
            end
            base_value, name = pick_base_name(word_set, level)

            # Modify the name with the remaining value
            modify_name(name, level - base_value)
        when :shield
            # Return a name that reflects the level of the shield
            word_set = case variant
            when :light_shield
                @@light_shield_names
            when :medium_shield
                @@medium_shield_names
            when :heavy_shield
                @@heavy_shield_names
            else
                raise "Asked to generate random name for unhandled shield variant #{variant}"
            end
            base_value, name = pick_base_name(word_set, level)

            # Modify the name with the remaining value
            modify_name(name, level - base_value)
        when :weapon
            # Return a name that reflects the level of the weapon

            # Choose a base weapon name that is within the level and spend the remainder on quality modifiers
            word_set = case variant
            when :blunt
                @@blunt_names
            when :long_arms
                @@long_arms_names
            when :ranged
                @@ranged_names
            when :small_arms
                @@small_arms_names
            when :sword
                @@sword_names
            else
                raise "Asked to generate random name for unhandled weapon variant #{variant}"
            end
            base_value, name = pick_base_name(word_set, level)

            # Modify the name with the remaining value
            modify_name(name, level - base_value)
        when :amulet
            base_value, name = pick_base_name(@@amulet_names, level)
            modify_name(name, level - base_value)
        when :footwear
            base_value, name = pick_base_name(@@footwear_names, level)
            modify_name(name, level - base_value)
        when :gauntlets
            base_value, name = pick_base_name(@@gauntlet_names, level)
            modify_name(name, level - base_value)
        when :helmet
            base_value, name = pick_base_name(@@helmet_names, level)
            modify_name(name, level - base_value)
        when :ring
            base_value, name = pick_base_name(@@ring_names, level)
            modify_name(name, level - base_value)
        else
            raise "Asked to generate random name for unhandled item type #{type}"
        end
    end

    # Pick a base name from a word set of all base names for the item
    # @param  word_set the set of weighted words that this item can get its base name from
    # @param  level         the items level
    # @return [value, name] tuple of base name to use and its weight value
    def self.pick_base_name word_set, level
        possible_base_name_tuples = word_set.reject { |n| n[0] > level }
        raise "No possible base names in #{word_set} for level #{level}" if possible_base_name_tuples.empty?
        # For higher quality items, pick within the 4 highest value names
        possible_base_name_tuples.length > 4 ? possible_base_name_tuples[-4,4].sample : possible_base_name_tuples.sample
    end

    def self.modify_name name, remainder
        return name if remainder <= 0

        possible_adjective_tuples = @@quality_modifiers.reject { |n| n[0] > remainder }
        # If many adjectives are possible, pick from the top 8
        adjective_tuple = possible_adjective_tuples.length > 8 ? possible_adjective_tuples[-8,8].sample: possible_adjective_tuples.sample 
        name = adjective_tuple[1] + " " + name
        remainder = remainder - adjective_tuple[0]
        # If there is still a remainder, give the item a + modifier
        remainder > 2 ? name = "+#{(remainder / 2).floor} #{name}" : name
    end
end