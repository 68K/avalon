#Defines the player's party
class Party

    attr_reader :back_rank_count, :inventory, :learned_skill_books, :members, :ready, :silver

    def initialize
        @members = []
        @inventory = []
        # Hash of skills to known skill books e.g. { unarmed: [:skilled, master], sword: [:skilled, :expert] }
        @learned_skill_books = {}
        @silver = 0
        # The number of members who are in the back rank
        @back_rank_count = 2
        @ready = true
    end

    def empty?
        @members.empty?
    end

    def length
        @members.length
    end

    def capable_members
        @members.select { |m| m.alive }
    end

    def index_of_first_capable_member
        raise "Asked for index of first capable member when no capable members" unless capable_members.any?
        @members.index { |m| m.alive }
    end

    def dead_members
        @members.select { |m| !m.alive }
    end

    def add_member(new_member)
        raise "Can't add more than 4 members" if @members.length >= 4
        @members << new_member
    end

    def set_member(idx, new_member)
        raise "Cannot set party member idx #{idx} when party has only #{@members.length} members" if idx >= @members.length
        raise "Cannot set party member idx #{idx} to '#{new_member}' - it is not a Character" unless new_member.is_a? Character 
        @members[idx] = new_member
    end

    def clear_members
        @members = []
    end

    # Make the party 4 "blank" (nil) entries
    # Differs from `clear_members` in that the 4 entries are reserved and can be indexed
    def set_blank_party
        @members = [nil, nil, nil, nil]
    end

    def set_learned_skill_books sb
        @learned_skill_books = sb
    end

    # @return hash with keys being skills the party can still gain grades in, and values being the next grade not known
    def next_skill_grades
        res = {}
        @members.each do |m|
            m.skills.keys.each { |s| res[s] = :skilled }
        end
        @learned_skill_books.keys.each do |s|
            case @learned_skill_books[s]
            when :skilled
                res[s] = :expert
            when :expert
                res[s] = :master
            when :master
                # Already maxed out, no "next grade" for this skill
                res.delete(s)
            end
        end

        res
    end

    # @return array of hashes, each hash being the details of a skill upgrade that can currently be purchased
    def purchasable_skill_upgrades
        res = []
        @members.each_with_index do |m,i|
            next unless m.alive
            m.skills.keys.each do |s|
                next_grade = next_learnable_skill_grade(s,m.skills[s][:grade])
                next if next_grade == :novice
                next if next_grade == m.skills[s][:grade]
                minimum_skill_level = case next_grade
                when :skilled
                    4
                when :expert
                    8
                when :master
                    12
                else
                    raise "Unhandled skill_grade #{next_grade}"
                end
                if lower_grade?(m.skills[s][:grade], next_grade)
                    meets_skill_level = m.skills[s][:level] >= minimum_skill_level
                    res << { character_idx: i, skill: s, next_grade: next_grade, cost: cost_to_upgrade_skill(s,next_grade), skill_level_met: meets_skill_level, minimum_skill_level: minimum_skill_level }
                end
            end
        end
        
        res
    end

    def purchase_skill_upgrade upgrade
        raise "upgrade not a hash" unless upgrade.is_a? Hash
        character = @members[upgrade[:character_idx]]
        raise "#{character.name} does not know the skill #{upgrade[:skill]}" unless character.skills[upgrade[:skill]]
        character.upgrade_skill(upgrade[:skill], upgrade[:next_grade])
        @silver -= upgrade[:cost]
    end

    def lower_grade? grade, comparison
        raise "Given grade #{grade} needs to be a symbol, not #{grade.class.name}" unless grade.is_a? Symbol
        raise "Grade to compare #{grade} is not a grade" unless [:novice, :skilled, :expert, :master].include?(grade)
        raise "#{comparison} is not a grade to compare with" unless [:novice, :skilled, :expert, :master].include?(comparison)
        case grade
        when :novice
            comparison != :novice
        when :skilled
            [:expert, :master].include?(comparison)
        when :expert
            comparison == :master
        when :master
            false
        end
    end

    def next_learnable_skill_grade skill, current_grade
        raise "current_grade #{current_grade} is not a Symbole" unless current_grade.is_a? Symbol
        return :novice unless @learned_skill_books[skill]
        return :master if @learned_skill_books[skill].include?(:master) && current_grade == :expert
        return :expert if @learned_skill_books[skill].include?(:expert) && current_grade == :skilled
        return :skilled if @learned_skill_books[skill].include?(:skilled) && current_grade == :novice
        return current_grade
    end

    def cost_to_upgrade_skill skill, new_grade
        case new_grade
        when :novice
            raise "Should not be trying to upgrade to :novice!"
        when :skilled
            1000
        when :expert
            3000
        when :master
            10000
        else
            raise "Invalid new_grade #{new_grade}"
        end
    end

    def add_skill_book skill, grade
        raise "Already known: #{skill} #{grade}" if @learned_skill_books[skill] && @learned_skill_books[skill].include?(grade)
        @learned_skill_books[skill] ? @learned_skill_books[skill] << grade : @learned_skill_books[skill] = [grade]
    end

    # @return true if the party is ready to play in the game
    def ready?
        return false if @members.compact.length < 4
        return false if @members.select { |m| m.name.nil? }.any?
        return false if @members.select { |m| m.cclass_id.nil? }.any?
        # TODO: Do not allow an incomplete appearance
        return false if @members.select { |m| m.appearance.empty? }.any?
        true
    end

    # Move the player at index further up in the party
    # @return true if the move was allowed
    def move_up index
        raise "index #{index} must be positive" if index.negative?
        raise "index #{index} greater than party size" unless index < (members.count)
        return if index == 0
        members[index], members[index - 1] = members[index - 1], members[index]
    end

    # Move the player at index further down the party
    # @return true if the move was allowed
    def move_down index
        raise "index #{index} must be positive" if index.negative?
        raise "index #{index} greater than party size" unless index < (members.count)
        return if index == (members.count - 1)
        members[index], members[index + 1] = members[index + 1], members[index]
    end

    def add_to_inventory i
        raise "Attempting to add nil to inventory!" if i.nil?
        raise "Can only add Item class to inventory" unless i.is_a? Item
        if i.type == :silver
            add_silver i.value
        else
            @inventory << i
        end
    end

    # @return true  if the item was applied
    #         false if the item could not be applied (e.g. trying to heal a dead character)
    def use_inventory_item item, character_index
        raise "item #{item} not in inventory" unless @inventory.include? item
        raise "Item #{item} is not usable" unless item.usable_in_inventory
        if item.type == :potion && item.variant == :healing_potion
            return false unless members[character_index].alive && members[character_index].hp < members[character_index].max_hp
            members[character_index].heal(item.power)
            @inventory.delete(item)
            return true
        else
            raise "Using item #{item} is not implemented"
        end
    end

    def remove_from_inventory item
        raise "#{item} is not an Item" unless item.is_a? Item
        raise "#{item} is not in the player inventory" unless @inventory.include? item
        @inventory.delete(item)
    end

    # TODO: Needs to be a function of party skills and stats
    def max_carry_weight
        200
    end

    def carried_weight
        return 0 if @inventory.empty?
        @inventory.map { |i| i.weight }.sum
    end

    # @return Array of Items that are sellable
    def sellable_items
        # TODO: Initially everything is sellable!
        @inventory.dup
    end

    # @return Array of Items that character with character_index can equip to a given slot
    def equippable_items character_index, slot
        res = []
        c = @members[character_index]
        @inventory.each do |i|
            next unless [:weapon, :armour, :shield, :ring, :amulet, :footwear, :helmet, :gauntlets].include?(i.type)
            if i.type == :ring
                next unless [:ring1, :ring2, :ring3, :ring4].include? slot
            elsif [:weapon, :shield].include?(i.type)
                next unless [:mainhand, :offhand].include?(slot)
            else
                next unless i.type == slot
            end
            res << i if c.can_equip?(i)
        end
        res
    end

    def add_silver s
        @silver += s
    end

    def remove_silver s
        raise "Attempt to remove more silver (#{s}) than the party has (#{@silver}). Check amount before calling remove_silver." if @silver - s < 0
        @silver -= s
    end

    def set_silver s
        @silver = s
    end

    def rest
        @members.each do |m|
            m.rest if m.alive
        end
    end

    # Is the front rank destroyed? i.e. can creatures ignore it and treat the rear rank as the front?
    def front_rank_defeated?
        front_rank.select { |m| m.alive }.empty?
    end

    # return members in the front rank (alive or dead)
    def front_rank
        @members[0...@back_rank_count]
    end

    # return members in the back rank (alive or dead)
    def back_rank
        @members[(@members.length - @back_rank_count)..-1]
    end

    def front_rank_alive?
        @members[0...@back_rank_count].select { |m| m.alive }.any?
    end

    def possible_targets range = 1
        # If range > 1 then the creature can hit anything alive
        return @members.select { |m| m.alive } if range > 1
        # If range = 1 and a front rank char is alive then only a front rank character can be hit
        if front_rank_alive?
            @members[0...back_rank_count].select { |m| m.alive }
        else
            @members[back_rank_count..-1].select { |m| m.alive }
        end
    end

    # @param    Character           character
    # @return   [Numeric,Numeric]   Screen x and y of centre of the HUD panel for that character  
    def centre_of_hud_panel character
        raise "character #{character} is not a Character" unless character.is_a? Character
        raise "#{character} is not a member of the party" unless idx = @members.index(character)
        [1160, 660 - 84 * idx]
    end

    # Determine a target position on screen that roughly equates to where a character would be standing "behind the camera"
    # @param    Character           character
    # @return   [Numeric,Numeric]   Screen x and y of a point below the viewport where you would imagine the character to be standing 
    def viewport_target_position character
        raise "character #{character} is not a Character" unless character.is_a? Character
        x = if idx = front_rank.index(character)
            300 + 300 * idx
        elsif idx = back_rank.index(character)
            200 + 300 * idx
        else
            raise "Character #{character} is in neither front_rank nor back_rank"
        end
        [x, 100]
    end

    def update_character_portraits
        members.each_with_index { |m,i| $gtk.args.state.portraits.render_portrait(i,m.appearance) }
    end

    def begin_generating_default_party
        @generation_tick = 0
        @ready = false
    end

    def update a
        return if @ready
        @generation_tick += 1
        case @generation_tick
        when 1,2
            return # Allow 2 ticks where nothing happens to let the loading screen appear
        when 3
            add_member(a.state.character_generator.random_character(:knight))
        when 4
            add_member(a.state.character_generator.random_character(:cleric))
        when 5
            add_member(a.state.character_generator.random_character(:hunter))
        when 6
            add_member(a.state.character_generator.random_character(:mage))
        when 7
            a.state.portraits.render_portrait(0, a.state.player_party.members[0].appearance)
        when 8
            a.state.portraits.render_portrait(1, a.state.player_party.members[1].appearance)
        when 9
            a.state.portraits.render_portrait(2, a.state.player_party.members[2].appearance)
        when 10
            a.state.portraits.render_portrait(3, a.state.player_party.members[3].appearance)
        when 11
            a.state.player_party.add_silver 1000
            8.times { a.state.player_party.add_to_inventory(a.state.item_generator.generate_potion(1 + rand(200), :healing_potion)) }
        when 12
            a.state.logs << "Entered dungeon with a default party. Good luck!"
        else
            @ready = true
        end
    end

    def serialize
        { silver: @silver, members: @members.map { |m| m.serialize }, inventory: @inventory.map { |i| i.serialize }, learned_skill_books: learned_skill_books }
    end

    def inspect
        self.to_s
    end

    def to_s
        serialize.to_s
    end
end

# Re-open PanelController and add equip menu
class PanelController
    def open_equip_menu args
        equip_panel = ButtonGroup.new(:equip_panel)
        args.state.choosing_character_to_equip = true
        args.state.chosen_character_to_equip = 0
        equip_panel.selected_slot = :offhand

        equip_panel.layout = :panel
        equip_panel.caption = nil # Equip screen is self-apparent
        equip_panel.x = 150
        equip_panel.y = 120
        equip_panel.focused = true

        def equip_panel.up_input args
            if args.state.choosing_character_to_equip
                args.state.chosen_character_to_equip = [args.state.chosen_character_to_equip - 1, 0].max
            else
                # Move through selected slots
                self.selected_slot = case self.selected_slot
                when :gauntlets
                    :helmet
                when :offhand
                    :gauntlets
                when :armour
                    :offhand
                when :footwear
                    :armour
                when :amulet
                    :helmet
                when :mainhand
                    :amulet
                when :ring1, :ring2
                    :mainhand
                when :ring3
                    :ring1
                when :ring4
                    :ring2
                else
                    self.selected_slot
                end
            end
        end
    
        def equip_panel.down_input args
            if args.state.choosing_character_to_equip
                args.state.chosen_character_to_equip = [args.state.chosen_character_to_equip + 1, args.state.player_party.members.count - 1].min
            else
                self.selected_slot = case self.selected_slot
                when :amulet
                    :mainhand
                when :mainhand
                    :ring1
                when :ring1
                    :ring3
                when :ring2
                    :ring4
                when :gauntlets
                    :offhand
                when :offhand
                    :armour
                when :armour
                    :footwear
                else
                    self.selected_slot
                end
            end
        end

        def equip_panel.left_input args
            return if args.state.choosing_character_to_equip

            self.selected_slot = case self.selected_slot
            when :gauntlets
                :helmet
            when :helmet
                :amulet
            when :offhand
                :mainhand
            when :armour
                :ring2
            when :footwear
                :ring4
            when :ring2
                :ring1
            when :ring4
                :ring3
            else
                self.selected_slot
            end
        end

        def equip_panel.right_input args
            return if args.state.choosing_character_to_equip

            self.selected_slot = case self.selected_slot
            when :amulet
                :helmet
            when :helmet
                :gauntlets
            when :mainhand
                :offhand
            when :ring1
                :ring2
            when :ring2
                :armour
            when :ring3
                :ring4
            when :ring4
                :footwear
            else
                self.selected_slot
            end
        end

        def equip_panel.confirm_input args
            # If the character to equip is being chosen, stop choosing
            if args.state.choosing_character_to_equip
                args.state.choosing_character_to_equip = false 
                return
            end
            if(args.state.player_party.equippable_items(args.state.chosen_character_to_equip, selected_slot).any?)
                args.state.panels.open_equippable_item_list(args)
            else
                args.state.panels.open_ok_dialog(args, :ack_generic, "You're not carrying anything suitable to equip")
            end
        end

        @button_groups << equip_panel
    end

    def open_equippable_item_list args
        e = ButtonGroup.new(:equippable_item_list)
        e.layout = :vertical
        e.x = 400
        e.y = 600
        e.buttonw = 500
        e.buttonh = 30
        e.focused = true
        e.set_options(args.state.player_party.equippable_items(args.state.chosen_character_to_equip, @button_groups.last.selected_slot))
        e.selected_slot = @button_groups.last.selected_slot

        # TODO: confirm_input needs to go in PanelController because the panel closes on confirmation. Is that possible here?

        @button_groups << e
    end

    def open_party_creation_panel args
        e = ButtonGroup.new(:party_creation_panel)
        e.layout = :panel
        e.x = 180
        e.y = 80
        e.focused = true
        args.state.confirming_party = false # true if player scrolls below last party member to highlight confirm button
        args.state.chosen_character_to_create = 0

        def e.up_input args
            if args.state.confirming_party
                args.state.confirming_party = false
                return
            end
            args.state.chosen_character_to_create -= 1 if args.state.chosen_character_to_create > 0
        end

        def e.down_input args
            return if args.state.confirming_party
            if args.state.chosen_character_to_create < (args.state.player_party.members.length - 1)
                args.state.chosen_character_to_create += 1
            elsif args.state.player_party.ready?
                args.state.confirming_party = true
            end
        end

        def e.confirm_input args
            if args.state.confirming_party
                args.state.panels.unfocus_top_dialog
                args.state.panels.open_choice_dialog(args, :confirm_start_with_party, "Start with this party?")
                return
            end
            args.state.panels.unfocus_top_dialog
            args.state.panels.open_character_edit_panel args
        end

        # Cancel input will be handled directly in panel_controller

        @button_groups << e
    end

    def open_character_edit_panel args
        e = ButtonGroup.new(:character_edit_panel)
        e.layout = :panel
        e.focused = true
        e.x = 180
        e.y = 80
        # TODO: Possibly revert trying coerce vertical options into a panel... 
        e.set_options(["Edit Name", "Choose Class", "Edit Appearance", "Ready!"])

        # If slot for character is not nil, put name and class and appearance into editor
        if args.state.player_party.members[args.state.chosen_character_to_create]
            args.state.creating_character_name = args.state.player_party.members[args.state.chosen_character_to_create].name
            # Class is a capitalized string in character creation
            args.state.creating_character_class = args.state.player_party.members[args.state.chosen_character_to_create].cclass_id.to_s.capitalize
            args.state.creating_character_appearance = args.state.player_party.members[args.state.chosen_character_to_create].appearance
        else
            args.state.creating_character_name = nil
            args.state.creating_character_class = nil
            args.state.creating_character_appearance = args.state.portraits.random_appearance
        end
        # Render character slot portrait
        args.state.portraits.render_portrait args.state.chosen_character_to_create, args.state.creating_character_appearance

        def e.up_input args
            self.selection -= 1 if self.selection > 0
        end

        def e.down_input args
            # Do not go to ready button if not ready
            return if self.options.keys[self.selection] == "Edit Appearance" && !self.ready?(args)
            self.selection += 1 if self.selection < (self.options.keys.length - 1)
        end

        def e.ready? args
            args.state.creating_character_name && args.state.creating_character_class && args.state.creating_character_appearance
        end

        # Confirm and Cancel are implemented in PanelController as they will dispose of this dialog
        # TODO: Implement cancel

        @button_groups << e
    end

    # confirm_input handled in PanelController because it involves disposing of this dialoge
    def open_class_selection_list args
        e = ButtonGroup.new(:class_selection_list)
        e.layout = :vertical
        e.set_options(args.state.character_generator.character_classes.classes.map { |k,v| v.name } )
        e.x = 800
        e.y = 480
        e.buttonw = 150
        e.buttonh = 30
        e.focused = true
        e.set_style(margin: 30)
        @button_groups << e
    end
end