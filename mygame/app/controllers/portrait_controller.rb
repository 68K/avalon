class PortraitController
    def initialize args
        @args = args

        @shoulder_options = [1,2]
        @face_options = [1,2]
        @ear_options = [1,2]
        @nose_options = [1,2]
        @mouth_options = [1,2,3]
        @eye_options = [1,2,3]
        @brow_options = [1,2,3]
        @hair_options = [1,2,3]
        @skin_tints = [[255,178,127], [158,108,79], [249,202,174]]
        @eye_tints = [[180,180,60], [60,200,60], [60,60,200]]
        @hair_tints = [[220,220,80], [180,140,90], [150,150,50], [90,90,90]]
        @clothing_tints = [[120,55,55], [190,70,70], [200,180,80], [80,80,80], [50,50,160], [55,120,55]]
        @lip_tints = [[180,90,90], [160,70,30]]
    end

    # Random appearance
    # @return Hash of appearance properties, to be stored with the character having this appearance
    def random_appearance
        res = {}
        res[:shoulders] = @shoulder_options.sample
        res[:face] = @face_options.sample
        res[:ears] = @ear_options.sample
        res[:nose] = @nose_options.sample
        res[:mouth] = @mouth_options.sample
        res[:eyes] = @eye_options.sample
        res[:iris] = res[:eyes] # Iris is matched with eye
        res[:brows] = @brow_options.sample
        res[:hair] = @hair_options.sample
        res[:lower_hair] = res[:hair] # Lower hair paired with upper
        res[:skin_tint] = @skin_tints.sample
        res[:eye_tint] = @eye_tints.sample
        res[:hair_tint] = @hair_tints.sample
        res[:clothing_tint] = @clothing_tints.sample
        res[:lip_tint] = @lip_tints.sample
        res
    end

    # Render a character portrait
    # TODO: Need to swap portraits when swapping character positions
    # @param Numeric    character_index         The index in the party of the character
    # @param Hash       character_appearance    Appearance hash for this character
    def render_portrait character_index, character_appearance
        raise "Does not appear to be a character appearance hash: #{character_appearance}" unless (character_appearance.is_a?(Hash) && character_appearance.keys.include?(:face))
        rt = case character_index
        when 0
            :portrait1
        when 1
            :portrait2
        when 2
            :portrait3
        when 3
            :portrait4
        else
            raise "No Render Target for character_index #{character_index}"
        end
        @args.render_target(rt).solids << { x: 0, y: 0, w: 64, h: 64, r: 130, g: 130, b: 130 }
        @args.render_target(rt).sprites.clear
        @args.render_target(rt).w = 64
        @args.render_target(rt).h = 64
        face_sprites = [{ x: 0, y: 0, w: 64, h: 64, path: "sprites/portraits/background.png" }]
        face_sprites << { x: 0, y: 0, w: 64, h: 64, path: "sprites/portraits/shoulders#{character_appearance[:shoulders]}.png", r: character_appearance[:clothing_tint][0], g: character_appearance[:clothing_tint][1], b: character_appearance[:clothing_tint][2] }
        face_sprites << { x: 0, y: 0, w: 64, h: 64, path: "sprites/portraits/hair_lower#{character_appearance[:lower_hair]}.png", r: character_appearance[:hair_tint][0], g: character_appearance[:hair_tint][1], b: character_appearance[:hair_tint][2] }
        face_sprites << { x: 0, y: 0, w: 64, h: 64, path: "sprites/portraits/face#{character_appearance[:face]}.png", r: character_appearance[:skin_tint][0], g: character_appearance[:skin_tint][1], b: character_appearance[:skin_tint][2] }
        face_sprites << { x: 0, y: 0, w: 64, h: 64, path: "sprites/portraits/mouth#{character_appearance[:mouth]}.png", r: character_appearance[:lip_tint][0], g: character_appearance[:lip_tint][1], b: character_appearance[:lip_tint][2] }
        face_sprites << { x: 0, y: 0, w: 64, h: 64, path: "sprites/portraits/nose#{character_appearance[:nose]}.png", r: character_appearance[:skin_tint][0], g: character_appearance[:skin_tint][1], b: character_appearance[:skin_tint][2] }
        face_sprites << { x: 0, y: 0, w: 64, h: 64, path: "sprites/portraits/ears#{character_appearance[:ears]}.png", r: character_appearance[:skin_tint][0], g: character_appearance[:skin_tint][1], b: character_appearance[:skin_tint][2] }
        face_sprites << { x: 0, y: 0, w: 64, h: 64, path: "sprites/portraits/eyes#{character_appearance[:eyes]}.png" }
        face_sprites << { x: 0, y: 0, w: 64, h: 64, path: "sprites/portraits/iris#{character_appearance[:iris]}.png", r: character_appearance[:eye_tint][0], g: character_appearance[:eye_tint][1], b: character_appearance[:eye_tint][2] }
        face_sprites << { x: 0, y: 0, w: 64, h: 64, path: "sprites/portraits/brows#{character_appearance[:brows]}.png", r: character_appearance[:hair_tint][0], g: character_appearance[:hair_tint][1], b: character_appearance[:hair_tint][2] }
        face_sprites << { x: 0, y: 0, w: 64, h: 64, path: "sprites/portraits/hair#{character_appearance[:hair]}.png", r: character_appearance[:hair_tint][0], g: character_appearance[:hair_tint][1], b: character_appearance[:hair_tint][2] }
        @args.render_target(rt).sprites << face_sprites
    end
end