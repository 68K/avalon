require 'app/ui/button_group.rb'

# Class controlling all panel instances in the game
class PanelController

    # Made accessible for debugging
    attr_reader :button_groups

    def initialize
        @button_groups = []

        # Status message is a simple string dialog that appears in the centre of the screen
        @status_message_open = false
        @status_message = ""
    end

    def any_dialogs_open?
        @button_groups.any?
    end

    def focused_dialog_type
        return unless any_dialogs_open?
        return @button_groups.last.type
    end

    def focus_top_dialog
        return unless any_dialogs_open?
        @button_groups.last.focused = true
    end

    def unfocus_top_dialog
        return unless any_dialogs_open?
        @button_groups.last.focused = false
    end

    def status_message_open?
        @status_message_open
    end

    def set_status_message s
        @status_message_open = true
        @status_message = s
    end

    def close_status_message
        @status_message_open = false
        @status_message = ""
    end

    def status_message_primitives args
        res = []
        cw, ch = args.gtk.calcstringbox(@status_message, 4)

        # Render main panel
        res << { x: 512 - cw/2 - 8, y: 400 - ch/2 - 8, w: cw + 16, h: ch + 16, path: 'sprites/white_square.png', a: 255, r: 255, g: 255, b: 255, primitive_marker: :sprite }
        res << { x: 512 - cw/2 - 5, y: 400 - ch/2 - 5, w: cw + 10, h: ch + 10, path: 'sprites/white_square.png', a: 255, r: 100, g: 100, b: 100, primitive_marker: :sprite }

        # Render text
        res << { x: 512 - cw/2, y: 400 + ch/2, size_enum: 4, text: @status_message, primitive_marker: :label }

        res
    end

    def down_input a
        return unless any_dialogs_open?

        # If the panel implements up_input, call it
        if @button_groups.last.respond_to?(:down_input)
            @button_groups.last.down_input a
            return
        end

        if @button_groups.last.layout == :string_editor
            @button_groups.last.rollback_string_character
        end

        @button_groups.last.select_next if (@button_groups.last.layout == :vertical && !a.state.combat.choosing_ally && !a.state.combat.choosing_target)
        
        if @button_groups.last.type == :character_combat_choice && a.state.combat.choosing_target
            a.state.combat.target_nearer_rank a
        end
        if a.state.combat.choosing_ally
            a.state.combat.chosen_ally += 1 if a.state.combat.chosen_ally < (a.state.player_party.members.count - 1)
        end
        if [:inventory_panel, :shop_buy_panel, :shop_sell_panel].include? @button_groups.last.type
            if @button_groups.last.type == :inventory_panel && a.state.choosing_item_target
                a.state.item_target_character += 1 if a.state.item_target_character < (a.state.player_party.members.count - 1)
            else
                @button_groups.last.select_next
            end
        end
        if @button_groups.last.type == :party_panel
            a.state.selected_character_in_party += 1 if a.state.selected_character_in_party < (a.state.player_party.members.count - 1)
        end
        if @button_groups.last.type == :spell_list
            if a.state.choosing_spell_target
                a.state.spell_target_character += 1 if a.state.spell_target_character < (a.state.player_party.members.count - 1)
            else
                a.state.selected_spell += 1 if a.state.selected_spell < (a.state.player_party.members[a.state.selected_character_for_status].known_spells.length - 1)
            end
        end
        if @button_groups.last.type == :character_status_panel
            @button_groups.last.selection += 1 if @button_groups.last.selection < (a.state.player_party.members[a.state.selected_character_for_status].skills.length - 1)
        end
        @button_groups.last.select_next if @button_groups.last.type == :guild_training_list
    end

    def up_input a
        return unless any_dialogs_open?
        # If the panel implements up_input, call it
        if @button_groups.last.respond_to?(:up_input)
            @button_groups.last.up_input a
            return
        end

        if @button_groups.last.layout == :string_editor
            @button_groups.last.roll_string_character
        end

        @button_groups.last.select_previous if (@button_groups.last.layout == :vertical && !a.state.combat.choosing_ally && !a.state.combat.choosing_target)
        if @button_groups.last.type == :character_combat_choice && a.state.combat.choosing_target
            a.state.combat.target_farther_rank a
        end
        if a.state.combat.choosing_ally
            a.state.combat.chosen_ally -= 1 if a.state.combat.chosen_ally > 0
        end
        if [:inventory_panel, :shop_buy_panel, :shop_sell_panel].include? @button_groups.last.type
            if @button_groups.last.type == :inventory_panel && a.state.choosing_item_target
                a.state.item_target_character -= 1 if a.state.item_target_character > 0
            else
                @button_groups.last.select_previous
            end
        end
        if @button_groups.last.type == :party_panel
            a.state.selected_character_in_party -= 1 if a.state.selected_character_in_party > 0
        end
        if @button_groups.last.type == :spell_list
            if a.state.choosing_spell_target
                a.state.spell_target_character -= 1 if a.state.spell_target_character > 0
            else
                a.state.selected_spell -= 1 if a.state.selected_spell > 0
            end
        end
        if [:character_status_panel, :guild_training_list].include?(@button_groups.last.type)
            @button_groups.last.selection -= 1 if @button_groups.last.selection > 0
        end
    end

    def left_input a
        return unless any_dialogs_open?

        # If the panel implements left_input, call it
        if @button_groups.last.respond_to?(:left_input)
            @button_groups.last.left_input a
            return
        end

        if @button_groups.last.layout == :string_editor
            @button_groups.last.previous_input_character
            return
        end

        if a.state.combat.choosing_target
            a.state.combat.target_enemy_left
        elsif @button_groups.last.layout == :horizontal
            @button_groups.last.select_previous
        elsif @button_groups.last.type == :character_status_panel
            a.state.selected_character_for_status -= 1
            a.state.selected_character_for_status = a.state.player_party.members.count - 1 if a.state.selected_character_for_status < 0
            @button_groups.last.caption = "#{a.state.player_party.members[a.state.selected_character_for_status].name} the #{a.state.player_party.members[a.state.selected_character_for_status].cclass_name}"
            @button_groups.last.selection = 0
        end
        if @button_groups.last.type == :party_panel
            a.state.selected_character_in_party -= 1 if a.state.player_party.move_up(a.state.selected_character_in_party)
            a.state.player_party.update_character_portraits
        end
        if @button_groups.last.type == :spell_list && !a.state.choosing_spell_target
            if a.state.selected_character_for_status > 0
                a.state.selected_character_for_status -= 1
                a.state.selected_spell = 0
            end
        end
    end

    def right_input a
        return unless any_dialogs_open?

        # If the panel implements right_input, call it
        if @button_groups.last.respond_to?(:right_input)
            @button_groups.last.right_input a
            return
        end

        if @button_groups.last.layout == :string_editor
            @button_groups.last.next_input_character
            return
        end

        if a.state.combat.choosing_target
            a.state.combat.target_enemy_right
        elsif @button_groups.last.layout == :horizontal
            @button_groups.last.select_next
        elsif @button_groups.last.type == :character_status_panel
            a.state.selected_character_for_status += 1
            a.state.selected_character_for_status = 0 if a.state.selected_character_for_status >= a.state.player_party.members.count
            @button_groups.last.caption = "#{a.state.player_party.members[a.state.selected_character_for_status].name} the #{a.state.player_party.members[a.state.selected_character_for_status].cclass_name}"
            @button_groups.last.selection = 0
        end
        if @button_groups.last.type == :party_panel
            a.state.selected_character_in_party += 1 if a.state.player_party.move_down(a.state.selected_character_in_party)
            a.state.player_party.update_character_portraits
        end
        if @button_groups.last.type == :spell_list && !a.state.choosing_spell_target
            if a.state.selected_character_for_status < (a.state.player_party.members.count - 1) 
                a.state.selected_character_for_status += 1
                a.state.selected_spell = 0
            end
        end
    end

    def confirm_input a
        return unless any_dialogs_open?

        # If the panel implements confirm_input, call it
        if @button_groups.last.respond_to?(:confirm_input)
            @button_groups.last.confirm_input a
            return
        end
        
        if @button_groups.last.layout == :string_editor
            done = @button_groups.last.confirm_input_character
            if done
                case @button_groups.last.type
                when :string_character_name
                    a.state.creating_character_name = @button_groups.last.input_string[0...-1] # Chop last string, TODO: max length string might have valid last character!
                end
                @button_groups.pop
                focus_top_dialog
                return
            end
        end

        case @button_groups.last.type
        when :character_combat_choice
            # If the player is in targeting mode, lock in the target and then close the combat options
            if a.state.combat.choosing_target || a.state.combat.choosing_ally
                a.state.combat.confirm_target
                close_character_combat_choice a
            else
                case @button_groups.last.options.keys[@button_groups.last.selection]
                when 'Attack'
                    a.state.combat.begin_choosing_target a
                when 'Spells'
                    open_combat_spells a
                else
                    close_character_combat_choice a
                end
            end
        when :combat_spell_list
            # If the player is in targeting mode, lock in the target and then close the combat options
            if a.state.combat.choosing_target || a.state.combat.choosing_ally
                a.state.combat.confirm_target
                close_character_combat_choice a
            else
                spell_definition = a.state.spells.spell_definitions.select { |s| s.name == @button_groups.last.options.keys[@button_groups.last.selection] }.first
                if spell_definition[:target] == :enemy
                    a.state.combat.chosen_spell_id = spell_definition[:id]
                    a.state.combat.begin_choosing_target a
                    a.state.logs << "Cast on which foe?"
                elsif spell_definition[:target] == :ally
                    a.state.combat.chosen_spell_id = spell_definition[:id]
                    a.state.combat.begin_choosing_ally a
                    a.state.logs << "Cast on which ally?"
                else
                    raise "TODO: Casting of spells targeting :#{spell_definition[:target]} not implemented"
                end
            end
        when :ingame_options
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Status"
                @button_groups.pop
                open_character_status_panel a
            end
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Inventory"
                @button_groups.pop
                open_inventory_panel a
            end
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Party"
                @button_groups.pop
                open_party_panel a
            end
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Quests"
                @button_groups.pop
                open_quest_list a
            end
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Equip"
                @button_groups.pop
                open_equip_menu a
            end
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Spells"
                @button_groups.pop
                open_spells_list a
            end
        when :inventory_panel
            if a.state.choosing_item_target
                if(a.state.player_party.use_inventory_item(@button_groups.last.list[@button_groups.last.selection], a.state.item_target_character))
                    @button_groups.last.focused = true
                    a.state.choosing_item_target = false
                    @button_groups.last.set_list(a.state.player_party.inventory)
                end
            else
                if(@button_groups.last.list[@button_groups.last.selection].usable_in_inventory)
                    a.state.choosing_item_target = true
                    a.state.item_target_character = 0
                    @button_groups.last.focused = false
                end
            end
        when :character_status_panel
            character = a.state.player_party.members[a.state.selected_character_for_status]
            skill = character.skills.keys[@button_groups.last.selection]
            if character.can_afford_skill_increase?(skill)
                character.increase_skill(skill)
            end
        when :equippable_item_list
            # TODO: Can this go in the class definition? It needs to close the dialog so would need to allow a dialog to close itself
            a.state.test_thing = @button_groups.last.options.keys[@button_groups.last.selection]
            a.state.player_party.members[a.state.chosen_character_to_equip].equip(@button_groups.last.options.keys[@button_groups.last.selection], @button_groups.last.selected_slot)
            @button_groups.pop
            @button_groups.last.focused = true
        when :spell_list
            if a.state.choosing_spell_target && a.state.spells.can_cast?(a.state.player_party.members[a.state.selected_character_for_status], a.state.player_party.members[a.state.selected_character_for_status].known_spells[a.state.selected_spell])
                a.state.choosing_spell_target = false
                @button_groups.last.focused = true
                a.state.spells.cast_on_ally!(a.state.player_party.members[a.state.selected_character_for_status].known_spells[a.state.selected_spell], a.state.player_party.members[a.state.selected_character_for_status], a.state.player_party.members[a.state.spell_target_character])
                a.state.calendar.add_seconds(10)
            else
                if a.state.spells.can_cast_from_spell_list?(a.state.player_party.members[a.state.selected_character_for_status].known_spells[a.state.selected_spell])
                    # TODO: Some spells will target the whole party, a target does not need chosen
                    a.state.choosing_spell_target = true
                    a.state.spell_target_character = 0
                    @button_groups.last.focused = false
                end 
            end
        when :return_to_town
            if @button_groups.last.options.keys[@button_groups.last.selection] == "No"
                @button_groups.pop
            elsif @button_groups.last.options.keys[@button_groups.last.selection] == "Yes"
                @button_groups.pop
                a.state.scenes.switch_to a, :town_scene
            end
        when :stairs_down
            if @button_groups.last.options.keys[@button_groups.last.selection] == "No"
                @button_groups.pop
            elsif @button_groups.last.options.keys[@button_groups.last.selection] == "Yes"
                @button_groups.pop
                a.state.dungeon.descend a
            end
        when :stairs_up
            if @button_groups.last.options.keys[@button_groups.last.selection] == "No"
                @button_groups.pop
            elsif @button_groups.last.options.keys[@button_groups.last.selection] == "Yes"
                @button_groups.pop
                a.state.dungeon.ascend a
            end
        when :open_chest_confirm
            if @button_groups.last.options.keys[@button_groups.last.selection] == "No"
                @button_groups.pop
            elsif @button_groups.last.options.keys[@button_groups.last.selection] == "Yes"
                @button_groups.pop
                3.times do 
                    loot = a.state.item_generator.generate_item(a.state.dungeon.level + 1, false)
                    open_ok_dialog(a, :generic_ok, "You got #{loot.name}!")
                    a.state.player_party.add_to_inventory loot
                end
                poi = a.state.dungeon.current_floor.poi_underfoot
                raise "Opened chest without a POI underfoot!" unless poi
                raise "Opened chest but POI underfoot is a #{poi.type}" unless poi.type == :chest
                a.state.opened_chests << { x: poi.x, y: poi.y, dungeon: a.state.dungeon.id, floor: a.state.dungeon.current_floor_number }
                # The chest is drawn on the dungeon viewport RT so that now needs redrawn
                a.state.dungeon.current_floor.update_render a
            end
        when :town_options
            # TODO: (maybe as part of #38) these serial if's are unsafe because one option
            # might dismiss all button groups making @button_groups.last nil
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Guild"
                @button_groups.pop
                open_guild_options a
            end
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Inn"
                @button_groups.pop
                open_inn_options a
            end
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Temple"
                @button_groups.pop
                open_temple_options a
            end
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Shop"
                @button_groups.pop
                open_shop_options a
            end
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Save"
                a.state.save_controller.save a.state.save_slot
                @button_groups.last.focused = false
                open_ack_game_saved_dialog a
            end
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Quests"
                @button_groups.pop
                open_quest_list a
            end
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Go To Dungeon"
                open_available_dungeon_list a
            end
        when :inn_options
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Rest"
                @button_groups.last.focused = false
                open_confirm_rest_at_inn_panel a
            end
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Talk"
                @button_groups.last.focused = false
                open_innkeeper_chat a
            end
        when :confirm_rest_at_inn
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Yes"
                if a.state.player_party.silver >= a.state.cost_to_rest
                    @button_groups.pop
                    a.state.player_party.remove_silver a.state.cost_to_rest
                    a.state.calendar.move_forward_to_dawn
                    a.state.player_party.rest
                    @button_groups.last.focused = true
                else
                    @button_groups.pop
                    open_not_enough_silver_to_rest_dialog a
                end 
            end
            if @button_groups.last.options.keys[@button_groups.last.selection] == "No"
                @button_groups.pop
                @button_groups.last.focused = true
            end
        when :dungeon_list
            a.state.scenes.change_scene a, :town_scene, :dungeon_scene, dungeon_name: @button_groups.last.options.keys[@button_groups.last.selection]
            @button_groups.pop # Pop the dungeon list
            @button_groups.pop # Pop the town menu
        when :shop_options
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Buy"
                open_shop_buy_panel a
            elsif @button_groups.last.options.keys[@button_groups.last.selection] == "Sell"
                open_shop_sell_panel a
            end
        when :shop_buy_panel
            shop_attempt_to_buy a
        when :confirm_buy_panel
            if(@button_groups.last.options.keys[@button_groups.last.selection] == "Yes")
                a.state.shop.buy_item(a, @button_groups.last.item)
                @button_groups.pop
                @button_groups.last.focused = true
                # Update the shop inventory scroll list
                @button_groups.last.reset_length
            end
            if(@button_groups.last.options.keys[@button_groups.last.selection] == "No")
                @button_groups.pop
                @button_groups.last.focused = true
            end
        when :shop_sell_panel
            open_sell_confirm_panel(a, @button_groups.last.list[@button_groups.last.selection])
        when :confirm_sell_panel
            if(@button_groups.last.options.keys[@button_groups.last.selection] == "Yes")
                a.state.shop.sell_item(a, @button_groups.last.item)
                @button_groups.pop
                @button_groups.last.focused = true
                # Update the list of items player can sell
                @button_groups.last.set_list(a.state.player_party.inventory)
                # Update the player inventory scroll list position
                @button_groups.last.reset_length
            end
            if(@button_groups.last.options.keys[@button_groups.last.selection] == "No")
                @button_groups.pop
                @button_groups.last.focused = true
            end
        when :ack_not_enough_silver_to_buy_item
            @button_groups.pop
            @button_groups.last.focused = true
        when :temple_options
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Raise"
                @button_groups.last.focused = false
                if a.state.player_party.dead_members.empty?
                    a.gtk.log_info "About to open no-one to raise dialog."
                    open_noone_to_raise_dialog a
                else
                    a.gtk.log_info "About to open raise choice dialog."
                    open_choose_to_raise_dialog a
                end
            end
        when :choose_who_to_raise
            # TODO: more solid logic for choosing selected character when there are duplicate names
            matching_chars = a.state.player_party.dead_members.select { |m| m.name ==  @button_groups.last.options.keys[@button_groups.last.selection] } 
            a.state.chosen_to_raise = matching_chars.first
            if a.state.cost_to_raise > a.state.player_party.silver
                @button_groups.last.focused = false
                open_cannot_afford_raise_diag a
            else
                @button_groups.last.focused = false
                open_confirm_raise_dialog a
            end
        when :confirm_raise
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Yes"
                a.state.chosen_to_raise.resurrect!
                a.state.player_party.remove_silver a.state.cost_to_raise
                a.state.calendar.add_hours(12)
                @button_groups.pop # Pop this dialog
                @button_groups.pop # Also pop the resurrection choice panel
                @button_groups.last.focused = true
            else
                @button_groups.pop
                @button_groups.last.focused = true
            end
        when :guild_options
            case @button_groups.last.options.keys[@button_groups.last.selection]
            when "Quest"
                a.state.guild.generate_quests(a) if a.state.guild.quests.empty?
                @button_groups.last.focused = false
                open_guild_quests a
            when "Train"
                @button_groups.last.focused = false
                open_guild_training a
            end
        when :guild_quest_list
            if a.state.quests.already_doing_skill_promotion_quest?
                open_ok_dialog a, :ack_already_doing_tome_quest, "You are already seeking a tome!"
            else
                a.state.quests.accept_guild_quest(a, @button_groups.last.selection)
                @button_groups.last.selection = 0
                @button_groups.last.set_options(a.state.guild.quests.map{ |q| q.title } )
            end
        when :guild_training_list
            if @button_groups.last.options.empty?
                @button_groups.pop
                @button_groups.last.focused = true
            else
                upgrade = @button_groups.last.options.keys[@button_groups.last.selection]
                if upgrade[:skill_level_met]
                    # TODO: warning if cannot afford
                    open_choice_dialog a, :confirm_train, "Train for #{upgrade[:cost]} silver?"
                else
                    open_ok_dialog a, :ack_skill_not_enough_to_train, "Need at least #{upgrade[:minimum_skill_level]} skill!"
                end
            end
        when :confirm_train
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Yes"
                @button_groups.pop # Pop this dialog
                @button_groups.last.focused = true
                choice = @button_groups.last.options.keys[@button_groups.last.selection]
                if choice[:cost] <= a.state.player_party.silver
                    a.state.player_party.purchase_skill_upgrade(choice)
                    # Update training list
                    @button_groups.last.selection = 0
                    @button_groups.last.set_options(a.state.player_party.purchasable_skill_upgrades)
                else
                    raise "Cannot afford training cost #{choice[:cost]} and this should already have been checked"
                end
            else
                @button_groups.pop
                @button_groups.last.focused = true
            end
        when :event_dialog
            if @button_groups.last.current_text_index < (@button_groups.last.text_pages.length - 1)
                @button_groups.last.set_current_text(@button_groups.last.current_text_index + 1)
            else
                a.state.events.mark_event_passed(@button_groups.last.event)
                @button_groups.pop
                @button_groups.last.focused = true
            end
        when :innkeeper_chat, :quest_update_dialog
            if @button_groups.last.current_text_index < (@button_groups.last.text_pages.length - 1)
                @button_groups.last.set_current_text(@button_groups.last.current_text_index + 1)
            else
                a.state.quests.goal_complete(a, @button_groups.last.quest_goal) if @button_groups.last.quest_goal
                should_return_to_town = @button_groups.last.should_return_to_town?
                @button_groups.pop
                @button_groups.last.focused = true if @button_groups.last
                if a.state.scenes.in_dungeon?
                    #  Return to town if indicated by the quest
                    if should_return_to_town
                        # puts "Ending quest and returning to town"
                        a.state.scenes.change_scene a, :dungeon_scene, :town_scene
                    else
                        a.state.dungeon.current_floor.update_render a 
                    end
                end
            end
        when :confirm_quick_start
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Yes"
                @button_groups.pop
                a.state.scenes.change_scene(a, :party_creation_scene, :town_scene)
            else
                @button_groups.pop
                a.state.player_party.set_blank_party
                a.state.panels.open_party_creation_panel a
            end
        when :character_edit_panel
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Edit Name"
                unfocus_top_dialog
                open_string_editor(a, :string_character_name, "Enter Character Name", nil, 10)
            end
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Choose Class"
                unfocus_top_dialog
                open_class_selection_list a
            end
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Edit Appearance"
                unfocus_top_dialog
                open_ok_dialog a, :generic_ok, "Appearance Editor is WIP..."
            end
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Ready!"
                a.state.player_party.set_member(a.state.chosen_character_to_create, a.state.character_generator.from_character_editor(a))
                @button_groups.pop
                focus_top_dialog
            end
        when :class_selection_list
            a.state.creating_character_class = @button_groups.last.options.keys[@button_groups.last.selection]
            @button_groups.pop
            @button_groups.last.focused = true
        when :confirm_start_with_party
            if @button_groups.last.options.keys[@button_groups.last.selection] == "Yes"
                @button_groups.pop # Pop this confirmation
                @button_groups.pop # Pop party creation
                a.state.scenes.change_scene(a, :party_creation_scene, :town_scene)
                # Initial save
                a.state.save_controller.save a.state.save_slot
            else
                @button_groups.pop
                focus_top_dialog
            end
        when :ack_not_enough_silver_to_rest, :ack_not_enough_silver_to_buy_item, :ack_game_saved, :ack_cant_afford_raise, :ack_noone_to_raise, :ack_already_doing_tome_quest, :ack_skill_not_enough_to_train, :ack_generic
            @button_groups.pop
            @button_groups.last.focused = true
        when :generic_ok
            @button_groups.pop
            @button_groups.last.focused = true if @button_groups.last
        end
    end

    def cancel_input a
        if a.state.combat.choosing_target || a.state.combat.choosing_ally
            a.state.combat.cancel_character_combat_choice a
        else
            return unless any_dialogs_open?

            # NOTE: No specialised `cancel_input` methods on panels because cancelling often involves dismissing the current panel

            case @button_groups.last.type
            when :character_combat_choice
                a.state.combat.cancel_character_combat_choice a
                # Reset the combat options since the player is picking for the previous character again
                @button_groups.last.set_options a.state.combat.combat_commands(a.state.player_party.members[a.state.combat.character_making_choice])
            when :character_status_panel, :party_panel, :quests_panel
                @button_groups.pop
                open_ingame_menu a if a.state.scenes.in_dungeon?
                open_town_options a if a.state.scenes.in_town?
            when :spell_list
                if a.state.choosing_spell_target
                    a.state.choosing_spell_target = false
                    @button_groups.last.focused = true
                else
                    @button_groups.pop
                    open_ingame_menu a
                end
            when :inventory_panel
                if a.state.choosing_item_target
                    a.state.choosing_item_target = false
                    @button_groups.last.focused = true
                else
                    @button_groups.pop
                    open_ingame_menu a
                end
            when :equip_panel
                if a.state.choosing_character_to_equip
                    a.state.choosing_character_to_equip = false
                    @button_groups.pop
                    open_ingame_menu a
                else
                    a.state.choosing_character_to_equip = true
                end
            when :equippable_item_list
                @button_groups.pop
                @button_groups.last.focused = true
            when :return_to_town, :ingame_options, :stairs_down, :stairs_up, :open_chest_confirm
                @button_groups.pop
            when :combat_spell_list
                @button_groups.pop
            when :guild_options, :inn_options, :shop_options, :temple_options
                @button_groups.pop
                open_town_options a
            when :confirm_rest_at_inn, :confirm_buy_panel, :choose_who_to_raise, :dungeon_list, :shop_buy_panel, :shop_sell_panel, :class_selection_list, :guild_quest_list, :guild_training_list
                @button_groups.pop
                @button_groups.last.focused = true
            when :ack_not_enough_silver_to_rest, :ack_not_enough_silver_to_buy_item, :ack_game_saved, :ack_cant_afford_raise, :ack_noone_to_raise, :ack_already_doing_tome_quest, :ack_generic
                # TODO: Can/should these all just be :ack_generic ? 
                @button_groups.pop
                @button_groups.last.focused = true
            end
        end
    end

    # Handle the player indicating to toggle menu
    def menu_input a
        # If in dungeon and no dialogs open, OK to open ingame options
        if a.state.scenes.in_dungeon? && (! any_dialogs_open?)
            open_ingame_menu a
        else
            # Else if options are open, close them
            @button_groups.pop if @button_groups.last.type == :ingame_options
        end
    end

    # Generic method for OK dialog
    def open_ok_dialog a, type, caption
        od = ButtonGroup.new(type)
        od.layout = :horizontal
        od.caption = caption
        od.x = 400
        od.y = 400
        od.buttonw = 300
        od.buttonh = 30
        od.set_options ["OK"]
        od.set_style(margin: 20)
        od.focused = true
        @button_groups.last.focused = false if @button_groups.last
        @button_groups << od
    end

    # Generic Yes/No dialog
    def open_choice_dialog a, type, caption
        c = ButtonGroup.new(type)
        c.layout = :horizontal
        c.caption = caption
        c.x = 400
        c.y = 400
        c.buttonw = 100
        c.buttonh = 30
        c.set_options ["Yes", "No"]
        c.set_style(margin: 20)
        c.focused = true
        # Default No
        c.select_next
        @button_groups << c
    end

    # Open generic string editor
    def open_string_editor a, type, caption, default_string, max_length
        e = ButtonGroup.new(type)
        e.layout = :string_editor
        e.caption = caption
        e.input_string = default_string || "A"
        e.input_string = "A" if e.input_string == ""
        e.input_string_max_length = max_length
        e.selection = e.input_string_chars_entered = (e.input_string.length - 1)
        @button_groups << e
    end

    # Handle closing of a combat choice dialog. Move to next character, or finalise choices.
    def close_character_combat_choice a
        spell_id = nil
        # If the spell list is open as well as the command list, note the selected spell ID then close the list
        if @button_groups.last.type == :combat_spell_list
            spell_id = a.state.spells.spell_definitions.select { |sd| sd.name == @button_groups.last.options.keys[@button_groups.last.selection] }.first[:id]
            @button_groups.pop 
        end
        a.state.combat.confirm_choice(a, @button_groups.last.options.keys[@button_groups.last.selection], spell_id)
        @button_groups.pop
    end

    def open_character_combat_options a
        character_options = ButtonGroup.new(:character_combat_choice)
        character_options.x = 900
        character_options.y = 250
        character_options.buttonw = 100
        character_options.buttonh = 30
        character_options.set_options a.state.combat.combat_commands(a.state.player_party.members[a.state.combat.character_making_choice])
        character_options.focused = true
        @button_groups << character_options
    end

    def open_combat_spells a
        csl = ButtonGroup.new(:combat_spell_list)
        csl.layout = :vertical
        csl.x = 720
        csl.y = 250
        csl.buttonw = 160
        csl.buttonh = 30
        # TODO: Another menu where the ID to track and the label string are not the same...
        spell_names = a.state.spells.combat_castable_spells(a.state.player_party.members[a.state.combat.character_making_choice]).map do |s|
            a.state.spells.spell_definitions.select { |sd| sd.id == s }.first[:name]
        end
        csl.set_options spell_names
        csl.focused = true
        @button_groups << csl
    end

    def open_ingame_menu a
        ino = ButtonGroup.new(:ingame_options)
        ino.x = 850
        ino.y = 450
        ino.buttonw = 150
        ino.buttonh = 30
        ino.set_options ["Status", "Quests", "Equip", "Spells", "Party", "Inventory"]
        ino.focused = true
        @button_groups << ino
    end

    def open_character_status_panel a
        sp = ButtonGroup.new(:character_status_panel)
        sp.layout = :panel
        a.state.selected_character_for_status = 0
        sp.caption = "#{a.state.player_party.members[0].name} the #{a.state.player_party.members[0].cclass_name}"
        sp.x = 150
        sp.y = 200
        @button_groups << sp
    end

    def open_spells_list a
        sl = ButtonGroup.new(:spell_list)
        sl.layout = :panel
        a.state.selected_character_for_status = 0
        a.state.selected_spell = 0
        a.state.spell_target_character = 0
        sl.caption = a.state.player_party.members[0].name
        sl.set_options(a.state.player_party.members[0].known_spells.map { |s| a.state.spells.spell_definitions.select { |d| d[:id] == s }.first.name } )
        sl.x = 150
        sl.y = 200
        sl.set_style(margin: 30)
        sl.focused = true
        @button_groups << sl
    end

    def open_inventory_panel a
        ip = ButtonGroup.new(:inventory_panel)
        ip.layout = :panel
        ip.scroll_length = 14
        ip.x = 150
        ip.y = 200
        ip.margin = 2
        ip.set_list a.state.player_party.inventory
        ip.focused = true
        @button_groups << ip
    end

    def open_party_panel a
        pp = ButtonGroup.new(:party_panel)
        pp.layout = :panel
        pp.caption = "The Party"
        a.state.selected_character_in_party = 0
        pp.x = 150
        pp.y = 200
        pp.focused = true
        @button_groups << pp
    end

    def open_quest_list a
        ql = ButtonGroup.new(:quests_panel)
        ql.layout = :panel
        ql.caption = "Quests"
        ql.x = 150
        ql.y = 200
        @button_groups << ql
    end

    def open_return_to_town_panel a
        open_choice_dialog a, :return_to_town, "Return to town?"
    end

    def open_stairs_down_panel a
        open_choice_dialog a, :stairs_down, "Descend to next floor?"
    end

    def open_stairs_up_panel a
        open_choice_dialog a, :stairs_up, "Ascend to previous floor?"
    end

    def open_chest_confirm_panel a
        open_choice_dialog a, :open_chest_confirm, "Open the chest?"
    end

    def open_quest_update_panel a, goal
        qu = ButtonGroup.new(:quest_update_dialog)
        qu.layout = :panel
        qu.x = 400
        qu.y = 300
        qu.set_style(margin: 10)
        qu.quest_goal = goal
        qu.set_text(goal.goal_complete_lines)
        @button_groups << qu
    end

    def open_town_options a
        to = ButtonGroup.new(:town_options)
        to.x = 1080
        to.y = 580
        to.buttonw = 200
        to.buttonh = 60
        opts = ["Go To Dungeon", "Inn", "Temple", "Quests", "Save"]
        opts.insert(2, "Shop") if a.state.shop.available_in_town?
        opts.insert(3, "Guild") if a.state.guild.available_in_town?
        to.set_options opts
        to.focused = true
        @button_groups << to
        # Any time the town options are made available, check if a :return_to_town event is possible
        # This allows the events to get processed when moving out of town services as well as when travelling into town
        if event = a.state.events.available_event(a, :return_to_town)
            open_event_dialog(event)
        end
    end

    def open_inn_options a
        to = ButtonGroup.new(:inn_options)
        to.x = 1080
        to.y = 580
        to.buttonw = 200
        to.buttonh = 60
        to.set_options ["Rest", "Buy", "Talk"]
        to.focused = true
        @button_groups << to
    end

    def open_confirm_rest_at_inn_panel a
        open_choice_dialog a, :confirm_rest_at_inn, "Rest for #{a.state.cost_to_rest} silver?"
    end

    def open_not_enough_silver_to_rest_dialog a
        open_ok_dialog a, :ack_not_enough_silver_to_rest, "You can't afford that!"
    end

    def open_innkeeper_chat a
        ikc = ButtonGroup.new(:innkeeper_chat)
        goal = a.state.quests.current_goal_for_innkeep
        ikc.layout = :panel
        ikc.x = 400
        ikc.y = 300
        ikc.set_style(margin: 10)
        if goal
            ikc.quest_goal = goal
            ikc.set_text(goal.goal_complete_lines)
        else
            ikc.set_text([["Have a seat!"],["Need a room?"]].sample)
        end
        @button_groups << ikc
    end

    def open_temple_options a
        to = ButtonGroup.new(:temple_options)
        to.x = 1080
        to.y = 580
        to.buttonw = 200
        to.buttonh = 60
        to.set_options ["Cleanse", "Raise", "Donate"]
        to.focused = true
        @button_groups << to
    end

    def open_noone_to_raise_dialog a
        open_ok_dialog a, :ack_noone_to_raise, "No-one in your party needs raised."
    end

    def open_choose_to_raise_dialog a
        c = ButtonGroup.new(:choose_who_to_raise)
        c.x = 440
        c.y = 400
        c.buttonw = 100
        c.buttonh = 30
        c.caption = "Raise whom?"
        c.set_options a.state.player_party.dead_members.map { |c| c.name }
        c.focused = true
        @button_groups << c
    end

    def open_cannot_afford_raise_diag a
        open_ok_dialog a, :ack_cant_afford_raise, "You need #{a.state.cost_to_raise} silver to raise #{a.state.chosen_to_raise.name}!"
    end

    def open_confirm_raise_dialog a
        open_choice_dialog a, :confirm_raise, "Raise #{a.state.chosen_to_raise.name} for #{a.state.cost_to_raise} silver?"
    end

    def open_ack_game_saved_dialog a
        ack = ButtonGroup.new(:ack_game_saved)
        ack.layout = :horizontal
        ack.caption = "Game Saved!"
        ack.x = 400
        ack.y = 400
        ack.buttonw = 300
        ack.buttonh = 30
        ack.set_options ["OK"]
        ack.set_style(margin: 20)
        ack.focused = true
        @button_groups << ack
    end

    def open_event_dialog(event)
        @button_groups.last.focused = false if @button_groups.last
        ed = ButtonGroup.new(:event_dialog)
        ed.layout = :panel
        ed.caption = event.title
        ed.x = 400
        ed.y = 300
        ed.set_style(margin: 10)
        ed.event = event.id
        ed.set_text(event.text)
        @button_groups << ed
    end

    def open_available_dungeon_list a
        @button_groups.last.focused = false if @button_groups.last
        dl = ButtonGroup.new(:dungeon_list)
        dl.layout = :vertical
        dl.x = 800
        dl.y = 575
        dl.buttonw = 290
        dl.buttonh = 40
        dl.set_options a.state.dungeons.select { |d| d.available }.map { |d| d.name }
        dl.focused = true
        @button_groups << dl
    end

    def clear_all
        @button_groups = []
    end

    def primitives_to_render a, outs
        res = []
        @button_groups.each { |b| res << b.primitives_to_render(a, outs) }
        res
    end

    def serialize
        {}
    end

    def inspect
        self.to_s
    end

    def to_s
        "ButtonGroupController"
    end

end