require 'app/LevisLibs/json.rb'

class SaveController
    def initialize a
        @args = a
    end

    def transform_keys_to_symbols(value)
        if value.is_a?(Array)
            array = value.map{|x| x.is_a?(Hash) || x.is_a?(Array) ? transform_keys_to_symbols(x) : x}
            return array
        end
        if value.is_a?(Hash)
            hash = value.inject({}){|memo,(k,v)| memo[k.to_sym] = transform_keys_to_symbols(v); memo}
            return hash
        end
        return value
    end

    def save slot_num
        s = {}
        s[:version] = @args.cvars['game_metadata.version'].value
        s[:calendar] = @args.state.calendar.serialize
        s[:party] = @args.state.player_party.serialize
        s[:shop_in_town] = @args.state.shop.available_in_town?
        s[:shop_level] = @args.state.shop.level
        s[:cost_to_raise] = @args.state.cost_to_raise
        s[:cost_to_rest] = @args.state.cost_to_rest
        # Quest progress keys are Numeric, for JSON they must be strings
        s[:quest_progress] = @args.state.quests.progress.map { |k,v| [k.to_s, v] }.to_h
        s[:dungeon_id_sequence] = @args.state.quests.dungeon_id_sequence
        s[:quest_id_sequence] = @args.state.quests.quest_id_sequence
        s[:event_completion] = @args.state.events.finished_events
        s[:dungeon_map_data] = @args.state.player_maps.to_save_data
        s[:opened_chests] = @args.state.opened_chests
        @args.gtk.write_file("game#{slot_num}.sav", s.to_json(minify: true))
        #@args.gtk.parse_json s.to_s.gsub('=>', ':')

        summary = {}
        summary[:version] = @args.cvars['game_metadata.version'].value
        summary[:members] = @args.state.player_party.members.map{ |m| { name: m.name, level: m.level, cclass: m.cclass_name } }
        summary[:silver] = @args.state.player_party.silver
        @args.gtk.write_file("game_summary#{slot_num}.sav", summary.to_json(minify: true))
        @args.state.save_slot_summaries[slot_num] = summary
    end

    def load slot_num
        # TODO: Check if the file exists
        raise "game#{slot_num}.sav could not be read" unless data = @args.gtk.parse_json_file("game#{slot_num}.sav")
        data = transform_keys_to_symbols(data)
        if(data.key?(:calendar))
            @args.state.calendar.from_save_data(data[:calendar])
        else
            @args.state.calendar.set_to_game_start_date
        end
        @args.state.player_party = Party.new()
        @args.state.player_party.set_silver(data[:party][:silver])
        data[:party][:members].each do |m|
            @args.state.player_party.add_member(@args.state.character_generator.from_save_data(m))
        end
        @args.state.player_party.members.count.times do |i|
            @args.state.portraits.render_portrait(i, @args.state.player_party.members[i].appearance)
        end
        data[:party][:inventory].each do |i|
            @args.state.player_party.add_to_inventory(@args.state.item_generator.from_save_data(i))
        end
        @args.state.temp = data
        learned_skill_books = data[:party][:learned_skill_books]
        learned_skill_books = transform_keys_to_symbols(learned_skill_books)
        learned_skill_books.keys.each { |k| learned_skill_books[k] = learned_skill_books[k].map {|b| b.to_sym } }
        @args.state.player_party.set_learned_skill_books(learned_skill_books)
        @args.state.shop.available_in_town(data[:shop_in_town])
        @args.state.shop.set_level(data[:shop_level])
        @args.state.cost_to_raise = data[:cost_to_raise]
        @args.state.cost_to_rest = data[:cost_to_rest]
        @args.state.quests.progress_from_save_file(data[:quest_progress])
        @args.state.events.finished_events_from_save_file(data[:event_completion])
        @args.state.quests.dungeon_id_sequence = data[:dungeon_id_sequence]
        @args.state.quests.quest_id_sequence = data[:quest_id_sequence]
        @args.state.player_maps.from_save_data(data[:dungeon_map_data])
        @args.state.opened_chests = data[:opened_chests]
    end

    def load_slot_summary slot_num
        raise "game_summary#{slot_num}.sav could not be read" unless data = @args.gtk.parse_json_file("game_summary#{slot_num}.sav")
        @args.state.save_slot_summaries[slot_num] = transform_keys_to_symbols(data)
    end

    def load_slot_summaries
        3.times do |i|
            load_slot_summary(i) if file_exist?("game_summary#{i}.sav")
        end
    end

    def delete_save_slot slot_num
        delete_file!("game#{slot_num}.sav")
        delete_file!("game_summary#{slot_num}.sav")
        @args.state.save_slot_summaries[slot_num] = nil
    end

    def delete_file! f
        @args.gtk.delete_file_if_exist(f)
    end
 
    def file_exist? f
        @args.gtk.stat_file(f)
    end

    def serialize
        {}
    end

    def inspect
        self.to_s
    end

    def to_s
        "SaveController"
    end
end