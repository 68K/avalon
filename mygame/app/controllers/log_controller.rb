# Manages the player log
# Note: This is the in-game log, NOT DR's system log.
class LogController

    attr_reader :entries

    def initialize
        @max_log_entries = 9
        @entries = ["Welcome to Avalon!"]
    end

    def log msg
        @entries << msg
        @entries.shift if @entries.length > @max_log_entries
    end

    # Allow "shovelling" a log entry into this class with the << shovel operator
    def <<(msg)
        log msg
    end

    def serialize
        {}
    end

    def inspect
        self.to_s
    end

    def to_s
        "LogController"
    end
end