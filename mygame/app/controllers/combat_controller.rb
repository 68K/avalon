# Class handling all combat state
class CombatController

    attr_reader :character_targets, :choosing_ally, :choosing_target, :execution_queue, :targeted_rank, :turn_queue, :waiting_on_animations, :waiting_on_character_choosing_action
    attr_accessor :character_making_choice, :chosen_ally, :chosen_spell_id, :chosen_target, :enemy_party

    def initialize
        # Queue of creatures waiting to turn.
        # One entry is a hash with:
        # :time             time remaining to take turn
        # :original_time    original time this turn was waiting
        # :actor            the entity waiting to act
        @turn_queue = []
        # Queue of creature turns waiting to execute
        # One entry is a hash with:
        #   :time           remaining to execute
        #   :original_time  the original execution time
        #   :actor          entity acting
        #   :target         the target for the action
        #   :action         action being... erm... actioned
        #   :spell_id       the spell being cast (nil if this action is not a spell)
        @execution_queue = []
        # If the player is currently choosing an action for a character
        @waiting_on_character_choosing_action = false
        # When an action is being selected, the character that is choosing
        @character_making_choice = 0
        # If player is in the process of choosing an enemy to target
        @choosing_target = false
        @chosen_target = 0
        # Array of targets for each character idx
        @character_targets = []
        # Spell ID currently chosen (used for targeting)
        @chosen_spell_id = nil
        # If player is in the process of choosing an ally
        @choosing_ally = false
        @chosen_ally = 0
        @targeted_rank = :front
        @enemy_party = nil
        @xp_this_fight = 0
        @silver_this_fight = 0

        @waiting_on_animations = false
        @pending_enemy_hits = []        # Pending physical hits against enemies
        @pending_enemy_spell_hits = []  # Pending spell cast against enemies
        @pending_party_hits = []        # Pending physical hits against party
        @pending_party_ally_casts = []  # Pending spell casts against party ally
        @pending_party_enemy_casts = [] # Pending enemy spell casts against party
    end

    def begin_combat a
        a.state.in_combat = true
        @silver_this_fight = 0
        @xp_this_fight = 0
        a.state.logs << "Combat!"
        @targeted_rank = :front
        generate_enemy_party a
        a.state.audio.play_combat_intro
        start_timeline a
    end

    # Stop waiting on animations and process pending actions
    def stop_waiting_on_animations args
        @waiting_on_animations = false

        @pending_enemy_hits.each do |p|
            dmg = p[:attacker].attack_damage
            p[:target].apply_damage(dmg)
            killed_it = false
            if !p[:target].alive
                killed_it = true
                enemy_killed args, p[:target]
                if p[:target].spec.type == :skeleton
                    args.state.particle_effects << ParticleEffect.new(args, { emit_x: p[:target_x], emit_y: p[:target_y], sprite: "sprites/particles/bone.png", a: 200, particle_rotate_speed: 6, gravity: 0.3, particle_life: 60 } )
                else
                    args.state.particle_effects << ParticleEffect.new(args, { emit_x: p[:target_x], emit_y: p[:target_y], sprite: "sprites/particles/blob.png", a: 128, g: 0, b: 0, particle_rotate_speed: 4, gravity: 0.2, particle_life: 80 } )
                end
            else
                args.state.particle_effects << ParticleEffect.new(args, { emit_x: p[:target_x], emit_y: p[:target_y], particle_life: 10, length: 16, sprite: "sprites/particles/bang.png", a: 128, b: 0, particle_rotate_speed: 4, spread_angle: 360 } )
            end
            # TODO: Method to update last log entry instead of adding a new one
            args.state.logs << "Hit for #{dmg} damage#{ killed_it ? ' killing it!' : '.' }"
            args.state.calendar.add_seconds(p[:duration].ceil)
            add_to_turn_queue(p[:attacker])
        end
        @pending_enemy_hits = []

        @pending_party_hits.each do |p|
            damage = damage_to_character(p[:target], p[:attacker], p[:capability])
            if p[:target].defending_this_turn
                if damage == 1
                    damage = 0
                else
                    damage = [(damage / 2).round, 1].max
                end
            end
            p[:target].apply_damage(damage)
            generate_player_hitting_particles(args, p[:capability], p[:target])
            verb = case p[:capability]
            when :melee
                "hit"
            when :ranged
                "shoot"
            else
                "attack"
            end
            if damage == 0
                args.state.logs << "#{p[:attacker].name} tries to #{verb} #{p[:target].name} but didn't make a scratch!"
            else
                args.state.logs << "#{p[:attacker].name} #{verb}s #{p[:target].name} for #{damage} damage!"
            end
            args.state.calendar.add_seconds(p[:duration].ceil)
            add_to_turn_queue(p[:attacker])
        end
        @pending_party_hits = []

        @pending_enemy_spell_hits.each do |p|
            killed_it = false
            args.state.spells.cast_on_enemy! p[:spell_id], p[:caster], p[:target]
            killed_it = true unless p[:target].alive
            
            # TODO: Need actual height and width of the enemy to adjust centre position
            target_x = p[:target_x] + 96
            target_y = p[:target_y] + 64
            # TODO: Needs to be a different effect per-spell
            args.state.particle_effects << ParticleEffect.new(args, { length: 20, spread_angle: 360, emit_x: target_x, emit_y: target_y, angle_sprite: true, sprite: "sprites/particles/needle.png", a: 200, particle_rotate_speed: 8, particle_life: 25 } )
            if killed_it
                enemy_killed args, p[:target]
                if p[:target].spec.type == :skeleton
                    args.state.particle_effects << ParticleEffect.new(args, {emit_x: target_x, emit_y: target_y, sprite: "sprites/particles/bone.png", a: 200, particle_rotate_speed: 6, gravity: 0.3, particle_life: 60 } )
                else
                    args.state.particle_effects << ParticleEffect.new(args, {emit_x: target_x, emit_y: target_y, sprite: "sprites/particles/blob.png", a: 128, g: 0, b: 0, particle_rotate_speed: 4, gravity: 0.2, particle_life: 80 } )
                end
            end
            args.state.logs << "#{p[:caster].name} casts #{args.state.spells.spell_name(p[:spell_id])} on #{p[:target].name}#{ killed_it ? ' killing it' : '' }!"
            args.state.calendar.add_seconds(p[:duration].ceil)
            add_to_turn_queue(p[:caster])
        end
        @pending_enemy_spell_hits = []

        @pending_party_ally_casts.each do |p|
            args.state.spells.cast_on_ally! p[:spell_id], p[:caster], p[:target]
            args.state.calendar.add_seconds(p[:duration].ceil)
            add_to_turn_queue(p[:caster])
        end
        @pending_party_ally_casts = []

        @pending_party_enemy_casts.each do |p|
            magic_type = p[:caster].capabilities.select { |s| s.to_s[0..4] == "cast_" }.sample
            raise "Enemy #{p[:caster]} trying to cast magic in combat without any cast_* capabilities" unless magic_type
            # TODO: Elemental effect types with distinct effects and visuals
            damage = damage_to_character(p[:target], p[:caster], magic_type)
            p[:target].apply_damage(damage)
            case magic_type
            when :cast_rock
                args.state.logs << "#{p[:caster].name} shoots rocks at #{p[:target].name} for #{damage} damage!"
            when :cast_fire
                args.state.logs << "#{p[:caster].name} engulfs #{p[:target].name} in flames, #{damage} damage!"
            when :cast_frost
                args.state.logs << "#{p[:caster].name} wraps #{p[:target].name} in frost causing #{damage} damage!"
            else
                raise "Unhandled magic type '#{magic_type}'"
            end
            generate_player_hitting_particles(args, magic_type, p[:target])
            args.state.calendar.add_seconds(p[:duration].ceil)
            add_to_turn_queue(p[:caster])
        end
        @pending_party_enemy_casts = []
    end

    # Generate enemy party
    # First enemy is a random pick then first two ranks are filled with compatible enemies.
    # Third rank is filled with compatible enemies that are ranged
    def generate_enemy_party a
        @enemy_party = EnemyParty.new()
        # Generate quest creature if appropriate
        # TODO: Quest creature should be in the depths of the floor and not the first fight
        if kill_goal = a.state.quests.current_goal_for_dungeon_floor(a.state.dungeon, :kill)
            @enemy_party.add_enemy(a.state.bestiary.generate_creature_for_goal(kill_goal), :front)
            # TODO: Need some supporting minions
        else
            # Generate one front rank enemy
            # Generate 1-2 compatible front rank enemies
            # Generate 0-2 compatible middle rank enemies
            # If there is a middle rank, generate a 3rd rank with ranged abilities
            initial_enemy = a.state.bestiary.generate_creature(a.state.dungeon.level, environ: a.state.dungeon.style)
            initial_faction = initial_enemy.faction
            @enemy_party.add_enemy(initial_enemy, :front)
            (1 + rand(1)).times { @enemy_party.add_enemy(a.state.bestiary.generate_creature(a.state.dungeon.level, environ: a.state.dungeon.style, faction: initial_faction), :front) }
            rand(3).times { @enemy_party.add_enemy(a.state.bestiary.generate_creature(a.state.dungeon.level, environ: a.state.dungeon.style, faction: initial_faction), :middle) }
            if @enemy_party.get_rank(:middle).length > 0
                (1 + rand(1)).times do
                    # Try to add 1-2 enemies with ranged capabilities
                    if enemy = a.state.bestiary.generate_creature(a.state.dungeon.level, environ: a.state.dungeon.style, faction: initial_faction, prefer_ranged: true)
                        @enemy_party.add_enemy(enemy, :rear)
                    end
                end
            end
        end

        # Raise immediately if there are any nils
        raise "Generate party has nil entries" unless @enemy_party.all_enemies.select { |e| e.nil? }.empty?
    end

    def start_timeline a
        @turn_queue = []
        @execution_queue = []
        a.state.player_party.capable_members.each { |c| add_to_turn_queue(c) }
        @enemy_party.all_enemies.each { |e| add_to_turn_queue(e) }
    end

    def add_to_turn_queue actor
        raise "actor #{actor} does not implement time_until_turn" unless actor.respond_to?(:time_until_turn)
        insert_time = actor.time_until_turn
        @turn_queue << { actor: actor, time: insert_time, original_time: insert_time }
    end

    def update a
        return if (@waiting_on_character_choosing_action || @waiting_on_animations)

        if a.state.player_party.capable_members.empty?
            a.state.scenes.game_over a
            return
        end

        if @enemy_party.defeated?
            end_combat a
        end

        @enemy_party.update_ranks

        # TODO: Update the in-game clock
        delta = 1.0/60

        # Remove completed executions
        # Do this first as an actor may have a finished execution and a fresh turn entry - keep the latter
        @execution_queue.reject! { |e| e[:finished] == true }

        # Delete from turn_queue where actor is now in the execution queue
        # Do this before updating turn queue because a player character waiting player input will be sat in both queues
        @turn_queue.reject! { |t| @execution_queue.select { |e| e[:actor] == t[:actor] }.any? }

        # Deduct delta time from turn queue. Move enemies ready to act to the execution queu.
        # If a player character is ready to act, start waiting for player choice
        @turn_queue.each do |t|
            t[:time] = [0.0, t[:time] - delta].max
            if t[:time] == 0.0
                if t[:actor].is_a?(Creature)
                    next unless t[:actor].alive
                    action = t[:actor].priorities[t[:actor].current_priority]
                    possible_target = nil
                    case action
                    when :melee, :ranged
                        rank, position = @enemy_party.rank_and_index_of(t[:actor])
                        possible_target = pick_targetable_character a, rank, position, action
                    when :magic
                        # TODO: Handle targeting of whole party or whole rank
                        possible_target = a.state.player_party.capable_members.sample
                    end
                    action = :nothing unless possible_target
                    delay = t[:actor].time_to_act(action)
                    # Don't call next_priority now in case creature gets stunned and doesn't get to execute. Call it when action executes.
                    @execution_queue << { actor: t[:actor], time: delay, original_time: delay, target: possible_target, action: action }
                else
                    # Begin waiting for player selection
                    # Skip if already waiting for an earlier character reaching 0
                    next if @waiting_on_character_choosing_action
                    @waiting_on_character_choosing_action = true
                    character_idx = a.state.player_party.members.find_index(t[:actor])
                    raise "Actor #{t[:actor]} not in player party" unless character_idx
                    @character_making_choice = character_idx
                    a.state.panels.open_character_combat_options a
                    # Since this character is choosing a new actor, they are no longer defending
                    t[:actor].defending_this_turn = false
                    # We're now waiting on the player, do not process any more combat time
                    return
                end
            end
        end

        # Deduct delta time from execution queue. If an actor is ready, initiate the action and wait on the animations
        @execution_queue.each do |e|
            e[:time] = [0.0, e[:time] - delta].max
            if e[:time] == 0.0 && (e[:finished] != true)
                if e[:actor].is_a?(Character)
                    case e[:action]
                    when "Attack"
                        if e[:target].alive
                            @waiting_on_animations = true
                            rank, idx = @enemy_party.rank_and_index_of e[:target]
                            target_x, target_y = @enemy_party.screen_position idx, rank
                            # TODO: Need actual height and width of the enemy to adjust centre position
                            target_x += 96
                            target_y += 64
                            # TODO: Different animation and log message per weapon type
                            if e[:actor].ranged_weapon_equipped?
                                character_x, character_y = a.state.player_party.viewport_target_position(e[:actor])
                                a.state.animations.start_arrow_animation(character_x, character_y, target_x, target_y)
                            else
                                a.state.animations.start_cut_animation(target_x, target_y)
                            end
                            @pending_enemy_hits << { attacker: e[:actor], target: e[:target], target_x: target_x, target_y: target_y, duration: e[:original_time] }
                            a.state.logs << "#{e[:actor].name} swings at #{e[:target].name}..."
                            e[:finished] = true
                            # Don't process anything more this tick
                            return
                        else
                            a.state.logs << "#{e[:actor].name} sees their foe is already dead!"
                            add_to_turn_queue(e[:actor])
                            e[:finished] = true
                        end
                    when "Defend"
                        e[:actor].defending_this_turn = true
                        add_to_turn_queue(e[:actor])
                        e[:finished] = true
                    when "Spells"
                        player_casting = e[:actor]
                        raise "Spell id was not given in spell execution: #{e}" unless e[:spell_id]
                        
                        if e[:target].is_a? Character
                            @waiting_on_animations = true
                            target_x, target_y = a.state.player_party.centre_of_hud_panel(e[:target])
                            a.state.animations.start_cast_animation(target_x, target_y, combat: true, cast_on_player: true, r: 100, b: 100)
                            @pending_party_ally_casts << { caster: player_casting, target: e[:target], spell_id: e[:spell_id], duration: e[:original_time] }
                            e[:finished] = true
                            # Do nothing more this tick
                            return
                        elsif (e[:target].is_a?(Creature) && e[:target].alive)
                            @waiting_on_animations = true
                            # TODO: A lot of repeated code with the melee case above. But magic might end up differing quite a bit...
                            rank, idx = @enemy_party.rank_and_index_of e[:target]
                            target_x, target_y = @enemy_party.screen_position idx, rank
                            @pending_enemy_spell_hits << { caster: player_casting, target: e[:target], target_x: target_x, target_y: target_y, spell_id: e[:spell_id], duration: e[:original_time] }
                            # TODO: Pick appropriate colouring for cast effect
                            # TODO: Check enemy's size instead of assuming 256x256
                            a.state.animations.start_cast_animation(target_x + 128, target_y + 128, combat: true)
                            e[:finished] = true
                            # Do nothing more this tick
                            return
                        else
                            a.state.logs << "#{player_casting.name} starts to cast but the spell fizzles out..."
                            add_to_turn_queue(e[:actor])
                            e[:finished] = true
                        end
                    end
                elsif e[:actor].is_a?(Creature)
                    next unless e[:actor].alive
                    rank, position = @enemy_party.rank_and_index_of(e[:actor])
                    case e[:action]
                    when :melee
                        # TODO: Target creature in enemy party as well as character in player party
                        if e[:target].alive
                            @waiting_on_animations = true
                            target_x, target_y = a.state.player_party.centre_of_hud_panel(e[:target])
                            @pending_party_hits << { attacker: e[:actor], target: e[:target], target_x: target_x, target_y: target_y, capability: :melee, duration: e[:original_time] }
                            a.state.animations.start_cut_animation(target_x, target_y - 48)
                            a.state.logs << "#{e[:actor].name} attacks #{e[:target].name}!"
                            e[:finished] = true
                            # Do nothing more this tick
                            return
                        else
                            do_nothing a, e[:actor]
                            e[:finished] = true
                        end
                    when :ranged
                        if e[:target].alive
                            @waiting_on_animations = true
                            target_x, target_y = a.state.player_party.centre_of_hud_panel(e[:target])
                            @pending_party_hits << { attacker: e[:actor], target: e[:target], target_x: target_x, target_y: target_y, capability: :ranged, duration: e[:original_time] }
                            view_target_x, view_target_y = a.state.player_party.viewport_target_position(e[:target])
                            view_creature_x, view_creature_y = @enemy_party.screen_position(position, rank)
                            a.state.animations.start_arrow_animation(view_creature_x, view_creature_y, view_target_x, view_target_y)
                            a.state.logs << "#{e[:actor].name} shoots #{e[:target].name}"
                            e[:finished] = true
                            # Do nothing more this tick
                            return
                        else
                            do_nothing a, e[:actor]
                            e[:finished] = true
                        end
                    when :magic
                        # TODO: Handle targeting of whole party or whole rank
                        if e[:target].alive
                            @waiting_on_animations = true
                            target_x, target_y = a.state.player_party.centre_of_hud_panel(e[:target])
                            a.state.animations.start_cast_animation(target_x, target_y, combat: true, cast_on_player: true, g: 100, b: 100)
                            # TODO: Pick a spell instead of just sampling capabilities...
                            @pending_party_enemy_casts << { caster: e[:actor], target: e[:target], duration: e[:original_time] }
                            e[:finished] = true
                            # Do nothing more this tick
                            return
                        else
                            do_nothing a, e[:actor]
                            e[:finished] = true
                        end
                    when :nothing
                        do_nothing a, e[:actor]
                        e[:finished] = true
                    else
                        raise "Don't know what to do with #{e}"
                    end

                    # Creature action has started to occur (or immediately failed) - move the creature to its next priority
                    e[:actor].next_priority
                end
            end
        end
    end

    def end_combat a
        a.state.logs << "You won! You gain #{@xp_this_fight} experience and #{@silver_this_fight} silver!"
        a.state.player_party.add_silver(@silver_this_fight)
        @silver_this_fight = 0
        lvl_display_delay = 0
        a.state.player_party.members.each_with_index do |m, i|
            next unless m.capable?
            lvls = m.add_xp(@xp_this_fight)
            lvls.times do |p|
                a.state.particle_effects << ParticleEffect.new(a, {emit_x: 1080, emit_y: 640 - 84 * i, emit_frequency: 1, particle_life: 8, delay: lvl_display_delay, sprite: "sprites/particles/lentil.png", a: 128, b: 0, particle_rotate_speed: 4, emit_x_drift: 4, spread_angle: 180 } )
                lvl_display_delay += 30
            end
        end
        @xp_this_fight = 0
        a.state.in_combat = false
        a.state.grace_steps_remaining = a.state.grace_steps_between_fights
        a.state.steps_to_next_encounter = 2 + rand(4)
        a.state.audio.play_combat_outro
    end

    def generate_player_hitting_particles args, hit_type, character_hit
        raise "Unsupported hit type '#{hit_type}'" unless [:melee, :ranged, :cast_rock, :cast_fire, :cast_frost].include? hit_type
        particle ="sprites/particles/vang.png"
        a = 200
        r = g = b = 255
        case hit_type
        when :melee
            particle = "sprites/particles/bang.png"
            b = 20
        when :ranged
            particle = "sprites/particles/needle.png"
        when :cast_rock
            particle = "sprites/particles/rock.png"
            r = g = b = 120
        when :cast_fire
            particle = "sprites/particles/flame.png"
            g = b = 30
        when :cast_frost
            particle ="sprites/particles/ice.png"
            r = g = 130
        end

        args.state.particle_effects << ParticleEffect.new(args, {emit_x: 1150, emit_y: 660 - 80 * args.state.player_party.members.find_index(character_hit), particle_life: 10, length: 16, sprite: particle, a: a, r: r, g: g, b: b, particle_rotate_speed: 5, spread_angle: 360 } )
    end

    def do_nothing args, creature
        case creature.spec.type
        when :skeleton
            args.state.logs << "#{creature.name} clatters its teeth!"
        when :rat
            args.state.logs << "#{creature.name} squeeks!"
        when :ratkin
            args.state.logs << "#{creature.name} screeches!"
        when :goblin
            args.state.logs << "#{creature.name} shakes its fist..."
        when :slime
            args.state.logs << "#{creature.name} undulates in a nauseating manner"
        when :snake
            args.state.logs << "#{creature.name} flicks its tongue"
        when :spider
            args.state.logs << "#{creature.name} stares with its many eyes..."
        when :zombie
            args.state.logs << "#{creature.name} stands idle..."
        else
            args.state.logs << "#{creature.name} bides its time."
        end
        add_to_turn_queue(creature)
    end

    # Process the death of an enemy, whether it be by weapon or spell
    def enemy_killed args, target
        @xp_this_fight += target.xp
        @silver_this_fight += rand(4) * target.level
        if (target.tied_goal && target.tied_goal.type == :kill)
            args.state.quests.goal_complete args, target.tied_goal
        end
    end

    def target_nearer_rank a
        case @targeted_rank
        when :middle
            if character_can_target_rank? a.state.player_party.members[@character_making_choice], :front
                @targeted_rank = :front 
                @chosen_target = @enemy_party.front_rank.index { |e| e.alive }
            end
        when :rear
            if character_can_target_rank? a.state.player_party.members[@character_making_choice], :middle
                @targeted_rank = :middle
                @chosen_target = @enemy_party.middle_rank.index { |e| e.alive }
            end
        end
    end

    def target_farther_rank a
        case @targeted_rank
        when :front
            if character_can_target_rank? a.state.player_party.members[@character_making_choice], :middle
                @targeted_rank = :middle
                @chosen_target = @enemy_party.middle_rank.index { |e| e.alive }
            end
        when :middle
            if character_can_target_rank? a.state.player_party.members[@character_making_choice], :rear
                @targeted_rank = :rear
                @chosen_target = @enemy_party.rear_rank.index { |e| e.alive }
            end
        end
    end

    # Confirm chosen command, place it on execute queue
    # @param    String  chosen_command  The chosen command (passed from the UI)
    def confirm_choice a, chosen_command, spell_id = nil
        actor = a.state.player_party.members[@character_making_choice]
        execute_time = actor.time_to_act(chosen_command)
        target = @character_targets[@character_making_choice]
        @execution_queue << { actor: actor, target: target, time: execute_time, original_time: execute_time, action: chosen_command, spell_id: spell_id }
        @choosing_target = false
        @choosing_ally = false
        @waiting_on_character_choosing_action = false
        # No longer selecting a spell
        @chosen_spell_id = nil
        a.state.panels.close_status_message
    end

    # TODO: Better name for this method now that it cannot cancel making a choice and only cancels targeting?
    def cancel_character_combat_choice a
        a.state.panels.close_status_message
        if @choosing_target || @choosing_ally
            # No longer selecting a spell
            @chosen_spell_id = nil
            # TODO: Put this in controller
            a.state.choosing_spell_target = false
            @choosing_target = false
            @choosing_spell_target = false
            @choosing_ally = false
        end
    end

    def begin_choosing_target a
        @choosing_target = true
        @targeted_rank = nearest_targetable_rank a.state.player_party.members[@character_making_choice]
        @chosen_target = @enemy_party.first_alive_in_rank(@targeted_rank)
        a.state.panels.set_status_message("Attack whom?")
    end

    def begin_choosing_ally a
        @choosing_ally = true
        @chosen_ally = 0
        a.state.panels.set_status_message("Which ally?")
    end

    # Attempt to target enemy to left.
    # Do nothing if already at left-most enemy.
    # If enemy to left is dead, either skip over it if possible or do nothing if current target is left-most that is targetable
    def target_enemy_left
        raise "Cannot change enemy target when not choosing a target!" unless @choosing_target
        return if @enemy_party.get_rank(@targeted_rank).length == 1
        return if @chosen_target == 0
        if @chosen_target == 2
            if @enemy_party.get_rank(@targeted_rank)[1].alive
                @chosen_target = 1 
            else
                @chosen_target = 0 if @enemy_party.get_rank(@targeted_rank)[0].alive
            end
        else
            @chosen_target = 0 if @enemy_party.get_rank(@targeted_rank)[0].alive
        end
    end

    # Attempt to target enemy to left.
    # Do nothing if already at right-most enemy.
    # If enemy to right is dead, either skip over it if possible or do nothing if current target is right-most that is targetable
    def target_enemy_right
        raise "Cannot change enemy target when not choosing a target!" unless @choosing_target
        return if @enemy_party.get_rank(@targeted_rank).length == 1
        return if @chosen_target == 2
        if @chosen_target == 0
            if @enemy_party.get_rank(@targeted_rank)[1].alive
                @chosen_target = 1 
            else
                @chosen_target = 2 if @enemy_party.get_rank(@targeted_rank)[2].alive
            end
        else
            # If chosen_target == 1 , select right if there are 3 enemies and 3rd enemy is alive
            @chosen_target = 2 if @enemy_party.get_rank(@targeted_rank).length == 3 && @enemy_party.get_rank(@targeted_rank)[2].alive
        end
    end

    # Confirm target. Point to the object rather than index
    def confirm_target
        if @choosing_target
            @character_targets[@character_making_choice] = @enemy_party.get_rank(@targeted_rank)[@chosen_target]
        else
            @character_targets[@character_making_choice] = $gtk.args.state.player_party.members[@chosen_ally]
        end
    end

    # Combat commands possible at the time of picking commands
    def combat_commands character
        raise "#{character} is not an instance of Character" unless character.is_a? Character
        res = %w{Defend Run}
        res.insert(0, "Spells") if $gtk.args.state.spells.combat_castable_spells(character).any?
        res.insert(0, "Attack") if character_can_target_rank? $gtk.args.state.player_party.members[@character_making_choice], :any
        res
    end

    # Determine if a character can target a rank in the enemy party
    # TODO: Determine ranks targetable for spells instead of all spells being able to hit all ranks
    # TODO: panel_controller doesn't allow rank targeting when spellcasting...
    def character_can_target_rank? character, rank = :any
        raise "#{character} is not an instance of Character" unless character.is_a? Character
        raise "Param rank should be in [:any, front, :middle, :rear]" unless [:any, :front, :middle, :rear].include? rank
        return false if (!@enemy_party) || @enemy_party.defeated?

        front_possible = -> () {
            return false if @enemy_party.front_rank.select { |a| a.alive }.empty?
            return true if @chosen_spell_id
            return false if (character.in_front_rank? || (! $gtk.args.state.player_party.front_rank_alive?)) && character.min_range > 1
            return false if character.in_rear_rank? && $gtk.args.state.player_party.front_rank_alive? && character.max_range < 2
            true
        }
        middle_possible = -> () {
            return false if @enemy_party.middle_rank.select { |a| a.alive }.empty?
            return true if @chosen_spell_id
            return false if (character.in_front_rank? || (! $gtk.args.state.player_party.front_rank_alive?)) && character.max_range < 2
            return false if character.in_rear_rank? && $gtk.args.state.player_party.front_rank_alive? && character.max_range < 3
            true
        }
        rear_possible = -> () {
            return false if @enemy_party.rear_rank.select { |a| a.alive }.empty?
            return true if @chosen_spell_id
            return false if (character.in_front_rank? || (! $gtk.args.state.player_party.front_rank_alive?)) && character.max_range < 3
            return false if character.in_rear_rank? && $gtk.args.state.player_party.front_rank_alive? && character.max_range < 4
            true
        }
        case rank
        when :front
            front_possible.call
        when :middle
            middle_possible.call
        when :rear
            rear_possible.call
        when :any
            front_possible.call || middle_possible.call || rear_possible.call
        end
    end

    def nearest_targetable_rank character
        raise "#{character} is not an instance of Character" unless character.is_a? Character
        return :front if character_can_target_rank? character, :front
        return :middle if character_can_target_rank? character, :middle
        return :rear if character_can_target_rank? character, :rear
        raise 'nearest_targetable_rank should not be called if no rank is targetable'
    end

    # Return damage to a character from a creature
    def damage_to_character character, creature, capability = :melee
        raise "#{character} is not an instance of Character" unless character.is_a? Character
        raise "#{creature} is not an instance of Creature" unless creature.is_a? Creature
        case capability
        when :melee
            character.adjusted_damage(creature.attack, capability)
        when :ranged
            character.adjusted_damage(creature.attack, capability)
        when :cast_rock, :cast_frost, :cast_fire
            # TODO: Need some manner of stats so that creature's offensive magic power can be determined
            base_dmg = creature.level ** 1.5
            dmg = [1, base_dmg - (0.01 * character.stats[:personality])].max
            dmg = [1, dmg - (0.01 * character.stats[:acuity])].max
            dmg.ceil
        else
            raise "Unsupported damage capability #{capability}"
        end
    end

    # Pick a target for a creature at a given position in the enemy party
    # The rank and index are given instead of the Creature object; because the position in the party needs to be known.
    # @param    rank        what rank of the enemy party this creature is in
    # @param    position    what index in the rank this creature is placed at
    # @param    action      what action the creature wants to perform (:melee, :ranged, etc)
    # @return   Character   to target, or nil if none appropriate
    def pick_targetable_character a, rank, position, action

        res = []

        actual_rank = @enemy_party.get_rank(rank)
        raise "#{rank} position #{position} is not a creature" unless actual_rank[position].is_a? Creature
        creature = actual_rank[position]

        case action
        when :melee
            raise "Creature #{creature} is attempting :melee but its capabilities are: #{creature.capabilities}" unless creature.capabilities.include?(:melee)
            # Enemy front rank can hit player front rank
            if rank == :front
                a.state.player_party.front_rank.select { |c| c.alive }.each { |c| res << c }
                # Enemy front rank can also hit player back rank if :long_reach or if the front rank is defeated
                if creature.capabilities.include?(:long_reach) || a.state.player_party.front_rank_defeated?
                    a.state.player_party.back_rank.select { |c| c.alive }.each { |c| res << c }
                end
            end
            # Enemy middle rank can hit player front rank if :long_reach
            if rank == :middle
                if creature.capabilities.include?(:long_reach)
                    a.state.player_party.front_rank.select { |c| c.alive }.each { |c| res << c }
                    # Can hit back rank if front rank is totally defeated
                    if a.state.player_party.front_rank_defeated?
                        a.state.player_party.back_rank.select { |c| c.alive }.each { |c| res << c }
                    end
                end
            end
        when :ranged
            return a.state.player_party.capable_members.sample
        else
            raise "Unsupported action #{action}"
        end

        # If this is still the default empty array, `sample` will return nil
        res.sample
    end

    # Render the combat timeline
    # @param    a       DR args hash
    # @return   Array   Array of render primitives to send to outputs to render the timeline
    def render_primitives_for_timeline a
        res = []
        # Background
        res << { x: 40, y: 600, w: 940, h: 100, r: 40, g: 40, b: 40, a: 80, path: 'sprites/white_square.png', primitive_marker: :sprite}

        # Execution bar
        res << { x: 50, y: 643, w: 2, h: 17, b: 0, g: 0, a: 200, path: 'sprites/white_square.png', primitive_marker: :sprite}
        res << { x: 50, y: 650, w: 220, h: 2, b: 0, g: 0, a: 200, path: 'sprites/white_square.png', primitive_marker: :sprite}

        # Turn bar
        res << { x: 270, y: 643, w: 2, h: 17, b: 0, a: 200, path: 'sprites/white_square.png', primitive_marker: :sprite}
        res << { x: 270, y: 650, w: 700, h: 2, b: 0, a: 200, path: 'sprites/white_square.png', primitive_marker: :sprite}
        res << { x: 970, y: 643, w: 2, h: 17, b: 0, a: 200, path: 'sprites/white_square.png', primitive_marker: :sprite}

        # Turn queue entries
        @turn_queue.each do |t|
            rx = 270 + (t[:time] / t[:original_time]) * 700
            if t[:actor].is_a?(Character)
                next unless t[:actor].capable?
                res << { x: rx - 8, y: 646, w: 16, h: 16, a: 200, path: 'sprites/white_triangle.png', primitive_marker: :sprite}
                cidx = a.state.player_party.members.find_index(t[:actor])
                rt = case cidx
                when 0
                    :portrait1
                when 1
                    :portrait2
                when 2
                    :portrait3
                when 3
                    :portrait4
                end
                res << { x: rx - 16, y: 658, w: 32, h: 32, source_x: 6, source_y: 6, source_width: 32, source_height: 32, a: 200, path: rt, primitive_marker: :sprite}
            else
                next unless t[:actor].alive
                res << { x: rx - 8, y: 638, w: 16, h: 16, a: 200, path: 'sprites/white_triangle.png', flip_vertically: true, primitive_marker: :sprite}
                res << { x: rx - 20, y: 600, w: 42, h: 42, a: 200, path: a.state.creature_sprites[t[:actor].spec.type], primitive_marker: :sprite}
                r, i = @enemy_party.rank_and_index_of(t[:actor])
                if @choosing_target && @targeted_rank == r && @chosen_target == i
                    alpha = 64 + 4 * (a.state.tick_count % 32) 
                    # TODO: Try to silhouette using blend_mode 3 and a render target
                    4.times { res << { path: a.state.creature_sprites[t[:actor].spec.type], x: rx - 20, y: 600, w: 42, h: 42, a: alpha, blendmode_enum: 2, primitive_marker: :sprite } } 
                end
            end
        end

        # Execution queue entries
        @execution_queue.each do |e|
            rx = 50 + (e[:time] / e[:original_time]) * 220
            if e[:actor].is_a?(Character)
                next unless e[:actor].capable?
                res << { x: rx - 8, y: 646, w: 16, h: 16, a: 200, path: 'sprites/white_triangle.png', primitive_marker: :sprite}
                cidx = a.state.player_party.members.find_index(e[:actor])
                rt = case cidx
                when 0
                    :portrait1
                when 1
                    :portrait2
                when 2
                    :portrait3
                when 3
                    :portrait4
                end
                res << { x: rx - 16, y: 658, w: 32, h: 32, source_x: 6, source_y: 6, source_width: 32, source_height: 32, a: 200, path: rt, primitive_marker: :sprite}
            else
                next unless e[:actor].alive
                res << { x: rx - 8, y: 638, w: 16, h: 16, a: 200, path: 'sprites/white_triangle.png', flip_vertically: true, primitive_marker: :sprite}
                res << { x: rx - 20, y: 600, w: 42, h: 42, a: 200, path: a.state.creature_sprites[e[:actor].spec.type], primitive_marker: :sprite}
                r, i = @enemy_party.rank_and_index_of(e[:actor])
                if @choosing_target && @targeted_rank == r && @chosen_target == i
                    alpha = 64 + 4 * (a.state.tick_count % 32) 
                    # TODO: Try to silhouette using blend_mode 3 and a render target
                    4.times { res << { path: a.state.creature_sprites[e[:actor].spec.type], x: rx - 20, y: 600, w: 42, h: 42, a: alpha, blendmode_enum: 2, primitive_marker: :sprite } } 
                end
            end
        end

        res
    end
end