require 'app/event.rb'

# Tracks events in the game
class EventController

    attr_accessor :finished_events

    def initialize
        @finished_events = []
        init_events
    end

    def init_events
        @events = []
        @events << GameEvent.from_spec({ id: 1, title: "Welcome to Avalon!", requirements: {}, text: ["Welcome to the town of Ythanwell in the land of Gem. This is a troubled land but for now, this is your home.",
                                                                                                      "Feel free to explore the town. If you're brave, venture forth to the Underhalls beneath the town.",
                                                                                                      "The townsfolk have been plagued by ratkins creeping out of the Underhalls at night. Perhaps you can help..."]})
        @events << GameEvent.from_spec({ id: 2, title: "A cosy inn", requirements: {}, trigger: :enter_inn, text: ["Come on in! Have a drink or rent a room", "(The inn is basic and smells a bit mouldy... but the innkeep seems trustworthy.)"] })                                                                                       
        @events << GameEvent.from_spec({ id: 3, title: "Ythanwell", requirements: { event_passed: 1 }, text: ["Ythanwell has everything a traveller might need. The inn has food, wine and beds; and the temple can aid those in dire need.",
                                                                                                           "As a simple crossroads town, we might not have everything you'd expect in a citadel or market town..."] })
        @events << GameEvent.from_spec({ id: 4, title: "Help!!", requirements: { quest_complete: 1 }, text: ["A villager rushes up to you, their face distraught.",
                                                                                                           "Please brave heroes, when you killed Azdpar, did you find anyone in there?",
                                                                                                           "Oh... my husband... he is still in there somewhere. Can you find him?"] })                                                    
    end

    # @return GameEvent object for an available event for the given trigger type
    # @return nil              if no event is available for the given trigger type
    def available_event args, trigger_type
        raise "Unsupported trigger type :#{trigger_type}" unless [:enter_dungeon, :enter_inn, :rest_at_inn, :return_to_town].include? trigger_type
        @events.select { |e| e.trigger == trigger_type &&
                             !(@finished_events.include? e.id) &&
                             # No requirements or both event and quest requirements satisfied
                             # TODO: Handle quests failed as well as completed
                             (e.requirements.empty? || ((!e.requirements.has_key?(:event_passed) || @finished_events.include?(e.requirements[:event_passed])) && 
                                                        (!e.requirements.has_key?(:quest_complete) || args.state.quests.progress[e.requirements[:quest_complete]][:state] == :completed)))
        }.first
    end

    def mark_event_passed event_id
        event_id = event_id.id if event_id.is_a? GameEvent
        @finished_events << event_id
    end

    def finished_events_from_save_file data
        raise "Not an array of values" unless data.is_a?(Array)
        raise "Not an array of numerics" unless data.reject { |e| e.is_a?(Numeric) }.empty?
        @finished_events = data
    end

    def serialize
        { finished_events: @finished_events }
    end

    def inspect
        self.to_s
    end

    def to_s
        "EventController"
    end
end