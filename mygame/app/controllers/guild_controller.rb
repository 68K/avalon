# Class that controls everything to do with the adventurers guild
class GuildController

    attr_reader :quests

    def initialize
        # TODO: Should require some quest to unlock
        @available_in_town = true
        @quests = []
    end

    # Needs deferred from initialization because the party doesn't exist at that point
    def generate_quests a
        @quests = a.state.quests.generate_guild_quest_specs(a.state,8)
    end

    # A quest has been accepted, remove it from the list
    # TODO: Need to replenish quest specs
    def quest_accepted quest_id
        @quests = @quests.delete_if { |q| q[:id] == quest_id }
    end

    def available_in_town?
        @available_in_town
    end

    def serialize
        {}
    end

    def inspect
        self.to_s
    end

    def to_s
        "GuildController"
    end
end

# PanelController panels for the guild
class PanelController
    def open_guild_options a
        go = ButtonGroup.new(:guild_options)
        go.x = 1080
        go.y = 580
        go.buttonw = 200
        go.buttonh = 60
        go.set_options ["Quest", "Train", "Talk"]
        go.focused = true
        @button_groups << go
    end

    def open_guild_quests a
        gq = ButtonGroup.new(:guild_quest_list)
        gq.x = 280
        gq.y = 500
        gq.buttonw = 760
        gq.buttonh = 48
        gq.set_options(a.state.guild.quests.map { |q| q[:title] })
        gq.focused = true
        @button_groups << gq
    end

    def open_guild_training a
        gt = ButtonGroup.new(:guild_training_list)
        gt.layout = :panel
        gt.x = 280
        gt.y = 140
        gt.set_options(a.state.player_party.purchasable_skill_upgrades)
        gt.focused = true
        @button_groups << gt
    end
end