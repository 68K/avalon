# Class that represents all things to do with the shop

class ShopController

    attr_reader :inventory, :level

    def initialize state
        # TODO: Shop should not be initially available until main quest is progressed
        @available_in_town = true
        # @level determines the quality of items sold in the shop and can be increased by investing
        @level = 1

        generate_inventory(state,@level) 
    end

    def available_in_town?
        @available_in_town
    end

    def available_in_town true_or_false
        @available_in_town = true_or_false
    end

    def set_level n
        @level = n
    end

    def increase_level levels = 1
        @level += levels
    end

    def generate_inventory(state,level)
        raise "Asked to generate shop inventory without an item generator" unless state.item_generator
        @inventory = []
        20.times { @inventory << state.item_generator.generate_shop_item(level + 5 - rand(level)) }
    end

    # Remove item from shop inventory
    # TODO: What happens if inventory is empty?
    def remove_from_inventory(item)
        raise "Shop inventory does not include #{item}" unless @inventory.include? item
        @inventory.delete(item)
    end

    # Buy price for a given item
    # TODO: Needs to be adjusted according to the skills and stats of the party
      # Note that the wiki suggests there won't be an explicit mercantile skill
      # It will be based on the Adventuring + Leadership skills plus stats
    def buy_price(args, item)
        raise "#{item} is not an Item" unless item.is_a? Item
        base_price = item.value
        # Shop level lowers the price slightly
        (base_price * (3 - [0.05 * @level, 2.5].min)).to_i
    end

    def sell_price(args, item)
        raise "#{item} is not an Item" unless item.is_a? Item
        base_price = item.value
        # Fraction of base value, increasing with shop value
        (base_price / (6 / @level)).to_i
    end

    # Buy an item, move it to party inventory
    # Affordability should already have been checked by UI so raise here if can't afford
    def buy_item(a, item)
        raise "#{item} is not an Item" unless item.is_a? Item
        cost = buy_price(a, item)
        raise "Party cannot afford #{cost} and affordability should be checked by UI" unless cost <= a.state.player_party.silver
        a.state.player_party.add_to_inventory(item)
        a.state.player_party.remove_silver(cost)
        remove_from_inventory(item)
    end

    # Sell an item, remove it from the inventory and add sell price to silver
    def sell_item(a, item)
        raise "#{item} is not an Item" unless item.is_a? Item
        raise "#{item} is not in the player inventory and isn't valid to sell" unless a.state.player_party.inventory.include? item
        sell_value = sell_price(a, item)
        a.state.player_party.remove_from_inventory(item)
        a.state.player_party.add_silver(sell_value)
    end

    def serialize
        {}
    end

    def inspect
        self.to_s
    end

    def to_s
        "ShopController"
    end
end

# PanelController panels for the shop
class PanelController
    def open_shop_options a
        so = ButtonGroup.new(:shop_options)
        so.x = 1080
        so.y = 580
        so.buttonw = 200
        so.buttonh = 60
        so.set_options ["Buy", "Sell", "Talk", "Invest"]
        so.focused = true
        @button_groups << so
    end

    def open_shop_buy_panel a
        sbp = ButtonGroup.new(:shop_buy_panel)
        sbp.layout = :panel
        sbp.caption = "Buy"
        sbp.set_list a.state.shop.inventory
        sbp.x = 150
        sbp.y = 200
        sbp.margin = 2
        sbp.focused = true
        @button_groups << sbp
    end

    def open_shop_sell_panel a
        ssp = ButtonGroup.new(:shop_sell_panel)
        ssp.layout = :panel
        ssp.caption = "Sell"
        ssp.set_list a.state.player_party.sellable_items
        ssp.x = 150
        ssp.y = 200
        ssp.margin = 2
        ssp.focused = true
        @button_groups << ssp
    end

    def open_sell_confirm_panel a, item
        open_choice_dialog a, :confirm_sell_panel, "Sell #{item.name} for #{a.state.shop.sell_price(a, item)} silver?"
        @button_groups.last.item = item
        #TODO: Need to shift Y because the item list will remain open, and overlapping dialogs cannot both render text
        @button_groups.last.y = 90
    end

    def open_buy_confirm_panel a, item
        open_choice_dialog a, :confirm_buy_panel, "Buy #{item.name} for #{a.state.shop.buy_price(a, item)} silver?"
        @button_groups.last.item = item
        #TODO: Need to shift Y because the item list will remain open, and overlapping dialogs cannot both render text
        @button_groups.last.y = 90
    end

    def open_not_enough_silver_to_buy_dialog a, item
        open_ok_dialog a, :ack_not_enough_silver_to_buy_item, "You can't afford #{a.state.shop.buy_price(a, item)} silver!"
    end

    def shop_attempt_to_buy a
        raise "Current UI panel is not a shop buy panel" unless @button_groups.last.type == :shop_buy_panel
        item = @button_groups.last.list[@button_groups.last.selection]
        raise "No selected item" unless item
        raise "Item #{item} is not an Item" unless item.is_a? Item
        cost = a.state.shop.buy_price(a, item)
        if cost <= a.state.player_party.silver
            open_buy_confirm_panel a, item
        else
            open_not_enough_silver_to_buy_dialog a, item
        end
    end
end