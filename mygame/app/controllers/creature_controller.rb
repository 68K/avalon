require 'app/creature.rb'

# Class that defines and generates creatures
class CreatureController

    attr_reader :types_for_capabilities

    def initialize
        @bestiary = []
        @bestiary << { id: 1, name: "Goblin Runt", type: :goblin, level: 1, base_hp: 15, base_xp: 5, attack: 5, defence: 1 }
        @bestiary << { id: 2, name: "Feral Skeleton", type: :skeleton, level: 2, base_hp: 15, base_xp: 5, attack: 5, defence: 2, environs: [:catacombs] }
        @bestiary << { id: 3, name: "Ratkin", type: :ratkin, level: 1, base_hp: 8, base_xp: 2, attack: 3, defence: 1, environs: [:underhalls] }
        @bestiary << { id: 4, name: "Goblin Scout", type: :goblin, level: 3, base_hp: 20, base_xp: 8, attack: 7, defence: 2 }
        @bestiary << { id: 5, name: "Alpha Ratkin", type: :ratkin, level: 4, base_hp: 20, base_xp: 9, attack: 7, defence: 2, environs: [:underhalls] }
        @bestiary << { id: 6, name: "Megabat", type: :bat, level: 3, base_hp: 10, base_xp: 5, attack: 3, defence: 2 }
        @bestiary << { id: 7, name: "Chiropraptor", type: :bat, level: 5, base_hp: 25, base_xp: 15, attack: 5, defence: 3 }
        @bestiary << { id: 8, name: "Giant Rat", type: :rat, level: 1, base_hp: 8, base_xp: 2, attack: 1, defence: 1, environs: [:underhalls, :catacombs] }
        @bestiary << { id: 9, name: "Matriarch Rat", type: :rat, level: 4, base_hp: 16, base_xp: 6, attack: 4, defence: 2, environs: [:underhalls, :catacombs] }
        @bestiary << { id: 10, name: "Redcap Sniper", type: :human_archer, level: 5, base_hp: 15, base_xp: 5, attack: 10, defence: 4 }
        @bestiary << { id: 11, name: "Redcap Roustabout", type: :human_blunt, level: 5, base_hp: 15, base_xp: 5, attack: 10, defence: 5 }
        @bestiary << { id: 12, name: "Redcap Brigadeer", type: :human_sword, level: 5, base_hp: 15, base_xp: 5, attack: 10, defence: 5 }
        @bestiary << { id: 13, name: "Redcap Wizard", type: :human_mage, level: 10, base_hp: 12, base_xp: 10, attack: 5, defence: 2 }
        @bestiary << { id: 14, name: "Redcap Pikeman", type: :human_spear, level: 10, base_hp: 12, base_xp: 10, attack: 5, defence: 6 }
        @bestiary << { id: 15, name: "Common Kobold", type: :kobold, level: 2, base_hp: 8, base_xp: 5, attack: 3, defence: 2 }
        @bestiary << { id: 16, name: "Monster Iguana", type: :lizard, level: 2, base_hp: 15, base_xp: 8, attack: 5, defence: 2, environs: [:underhalls] }
        @bestiary << { id: 17, name: "Moonlake Brute", type: :orc_melee, level: 6, base_hp: 25, base_xp: 15, attack: 6, defence: 3 }
        @bestiary << { id: 18, name: "Moonlake Thrower", type: :orc_thrower, level: 6, base_hp: 25, base_xp: 15, attack: 6, defence: 3 }
        @bestiary << { id: 19, name: "Moonlake Shaman", type: :orc_mage, level: 10, base_hp: 25, base_xp: 18, attack: 4, defence: 23 }
        @bestiary << { id: 20, name: "Cave Gel", type: :slime, level: 6, base_hp: 15, base_xp: 8, attack: 4, defence: 2 }
        @bestiary << { id: 21, name: "Python", type: :snake, level: 2, base_hp: 12, base_xp: 4, attack: 6, defence: 3 }
        @bestiary << { id: 22, name: "Anaconda", type: :snake, level: 8, base_hp: 20, base_xp: 8, attack: 8, defence: 6 }
        @bestiary << { id: 23, name: "Trapdoor Spider", type: :spider, level: 2, base_hp: 10, base_xp: 3, attack: 4, defence: 2, environs: [:underhalls, :catacombs] }
        @bestiary << { id: 24, name: "Coffin-weaver", type: :spider, level: 8, base_hp: 20, base_xp: 6, attack: 7, defence: 5, environs: [:catacombs] }
        @bestiary << { id: 25, name: "Restless Dead", type: :zombie, level: 6, base_hp: 20, base_xp: 4, attack: 4, defence: 2, environs: [:catacombs] }
        @bestiary << { id: 26, name: "Raised Dead", type: :zombie, level: 10, base_hp: 30, base_xp: 8, attack: 6, defence: 3, environs: [:catacombs] }
        # Define default capabilities for creature types
        # TODO: Once these initial caps work, add more e.g. healing, buff, debuff
        @types_for_capabilities = {}
        @types_for_capabilities[:melee] = [:goblin, :skeleton, :ratkin, :bat, :rat, :human_blunt, :human_sword, :human_spear, :kobold, :lizard, :orc_melee, :slime, :snake, :spider, :zombie]
        @types_for_capabilities[:long_reach] = [:human_spear]
        @types_for_capabilities[:ranged] = [:human_archer, :orc_thrower]
        # Most creatures are capable of wanting to flee, except for undead
        @types_for_capabilities[:flee] = [:goblin, :ratkin, :bat, :rat, :human_archer, :human_blunt, :human_mage, :human_spear, :human_sword, :kobold, :lizard, :orc_mage, :orc_melee, :orc_thrower, :slime, :snake, :spider]
        @descriptions = {}
        @descriptions[1] = "Rejected by their own kind, these pitiful runts will try to eke out an existence at the periphery of civilization, like rats. Don't feel bad bashing them."
        @descriptions[2] = "It's taboo, but few magic students went through college without at least trying to raise a skeleton or two. Thus you can't go far without running into a feral skeleton. Generally they aren't even hostile, just chaotic. Best to trash them."
        @descriptions[3] = "The size and temerity of Ratkins in Avalon is limited only by available food. Whereever there is battle, they grow until they end up involved in the battle."
        @descriptions[4] = "While goblin runts are a minor nuisance to a community, a scout is a sure sign of serious troubles ahead - as warriors will follow. Even killing a scout can cause trouble since blunt-minded goblins are likely to despatch warriors when a scout is missing."
        @descriptions[5] = "Ratkins have complex group and social habits, and part of that is the need for 'alphas'. Bigger, tougher, and much more aggressive."
        @descriptions[6] = "A giant bat that will opportunistically try to eat flesh."
        @descriptions[7] = "An enormous bat that attacks anything in its territory."
        @descriptions[8] = "Overgrown rats are found throughout the land and desperation causes them to attack frequently."
        @descriptions[9] = "At the centre of a rat nest is a matriarch - she's bigger, tougher and her alpha status requires her to never run from a fight."
        @descriptions[10] = "An archer for the Redcaps. Will try to kill or cripple from range before you ever see them."
        @descriptions[11] = "The Redcaps are largely comprised of common thugs and degenerates, without formal armour or weapons."
        @descriptions[12] = "Foot soldier of the Redcaps. Has some amount of experience and a respectable blade."
        @descriptions[13] = "The most dangerous members of the Redcaps are its wizards. While they are amongst the poorest wizards in the land and there is generally a reason why they are reduced to nothing more than banditry, the presence of a wizard is underestimated at your peril!"
        @descriptions[14] = "Former professional infantry. Redcap bosses will send out a Pikeman when they don't trust their usual thugs to get the job done."
        @descriptions[15] = "The most common form of kobold is the least mystical; and there is no special custom or ritual to banishing them - they just need physically beaten."
        @descriptions[16] = "A giant iguana. Not inherently aggressive, but if cornered it will fight to the death."
        @descriptions[17] = "Powerful warrior of the orc house Moonlake."
        @descriptions[18] = "Moonlake combat units are often supported by ranged units. Do not underestimate the fact that they sling rocks - they are arguably more dangerous than archers."
        @descriptions[19] = "Orc magic was refined on hunting and battle grounds rather than in academia. While the spells are basic, they are fierce and swift."
        @descriptions[20] = "A gelatinous mass. Quite aggresive, since it lacks the capacity to judge a potential food source as too dangerous."
        @descriptions[21] = "A giant snake driven hostile by hunger."
        @descriptions[22] = "An enormous snake that isn't afraid of a few cuts and bruises to get its next meal."
        @descriptions[23] = "Giant spider that can appear suddenly."
        @descriptions[24] = "Giant spider that learned to take advantage of lone grave visitors."
        @descriptions[25] = "A recently dead corpse that has risen due to some strange influence."
        @descriptions[26] = "A corpse deliberately risen with dark magic."

        # Assign factions to enemies
        # TODO: Need "compatible factions" that can share a party
        @bestiary.each do |b|
            b[:faction] = case b.type
            when :goblin
                :goblin
            when :kobold
                :goblin
            when :skeleton, :zombie
                :undead
            when :ratkin
                :ratkin
            when :bat, :rat, :lizard, :slime, :snake, :spider
                :monster
            end
            b[:faction] = :redcap if [10,11,12,13,14].include? b[:id]
            b[:faction] = :moonlake if [17,18,19].include? b[:id]

        end
    end

    def serialize
        {}
    end

    def inspect
        self.to_s
    end

    def to_s
        "CreatureController"
    end

    def generate_creature level, environ: :any, faction: :any, prefer_ranged: false
        possibles = @bestiary.select { |x| x[:level] <= level &&
                        (environ == :any || !x.key?(:environs) || x[:environs].include?(environ)) &&
                        (faction == :any || !x.key?(:faction) || x[:faction] == faction) &&
                        (prefer_ranged == false || ([:ranged, :cast_fire, :cast_frost, :cast_rock] & capabilities_for_bestiary_id(x[:id])).any? ) }
        selection = possibles[rand(possibles.count)]
        unless selection
            # if *any* creature is OK, it's an exception to not pick one. Else nil is returned to indicate that another pick is needed.
            if environ == :any && faction == :any
                raise "No possible bestiary entries for level #{level} and environ #{environ}. Possibles are: #{possibles}"
            else
                return nil
            end
        end
        creature = Creature.from_spec selection
        creature.set_capabilities(capabilities_for_bestiary_id(selection.id))
        creature.set_priorities(priorities_for_bestiary_id(selection.id))
        creature.set_stats(stats_for_bestiary_id(selection.id))
        creature
    end

    # Generate a creature meeting a quest goal e.g.
    # {
    #     phase: 1,
    #     goal_id: 1,
    #     type: :kill,
    #     desc: "Find and kill the ratkin in the underhalls",
    #     dungeon_id: 1, 
    #     floor: 1,
    #     base_level: 5,
    #     bestiary_id: 5,
    #     name: "Azdpar the biting"
    # }
    def generate_creature_for_goal goal
        raise "goal #{goal} is not a hash" unless goal.is_a? Hash
        spec = @bestiary.select { |x| x.id == goal[:bestiary_id] }.first
        raise "No creature spec for goal[:bestiary_id] == #{goal[:bestiary_id]}" unless spec
        res = Creature.from_spec spec
        res.level_up! while res.level < goal[:base_level]
        res.name = goal[:name]
        res.tied_goal = goal
        res.bossify!
        res.set_capabilities(capabilities_for_bestiary_id(spec.id))
        res.set_priorities(priorities_for_bestiary_id(spec.id))
        res.set_stats(stats_for_bestiary_id(spec.id))
        res
    end

    # Return the capabilities for a given bestiary ID
    # Many capabilities will be determined by @types_for_capabilities but there will be lots of exceptions to add or subtract - those are handled here
    def capabilities_for_bestiary_id id
        res = []
        entry = @bestiary.select { |e| e[:id] == id }.first
        raise "No bestiary entry with id #{id}" unless entry

        # Add generic capabilities
        @types_for_capabilities.keys.each do |c|
            res << c if @types_for_capabilities[c].include?(entry[:type]) 
        end

        # Add spell capabilities
        case id
        when 13 # Human mage
            res << :cast_fire
            res << :cast_frost
        when 19 # Orc mage
            res << :cast_fire
            res << :cast_rock
        end

        res
    end

    # Return the priorities for a creature during combat
    # @return Array of priorities, that can be cycled through across combat turns
    def priorities_for_bestiary_id id
        res = []
        caps = capabilities_for_bestiary_id id
        # TODO: Need a lot more logic here including sequences of priorities to cycle through
        if caps.select { |c| c.to_s[0..4] == "cast_" }.first
            res << :magic
        end
        if caps.include?(:ranged)
            res << :ranged
        end
        if caps.include?(:melee)
            res << :melee
        end
        # Flee if nothing else
        if res.empty? && caps.include?(:flee)
            res << :flee
        end

        # TODO: Allow for the :tactical priority
        res
    end

    # @return hash of stats for a creature based on its bestiary id
    def stats_for_bestiary_id id
        entry = @bestiary.select { |e| e[:id] == id }.first
        raise "No bestiary entry with id #{id}" unless entry
        res = { might: 5, physicality: 5, speed: 5, personality: 5, mind: 5, acuity: 5 }

        delta = [1,Math.log(entry[:level]).ceil].max 

        case entry[:type]
        when :human_archer
            res[:speed] += delta
            res[:personality] += delta
            res[:mind] += delta
            res[:acuity] += delta
        when :human_blunt
            res[:might] += delta
            res[:physicality] += delta
            res[:personality] += delta
            res[:mind] += delta
            res[:acuity] += delta
        when :human_mage
            res[:personality] += delta * 2
            res[:mind] += delta * 2
            res[:acuity] += delta * 2
        when :human_spear
            res[:speed] += delta
            res[:mind] += delta
            res[:acuity] += delta
        when :human_sword
            res[:might] += delta
            res[:physicality] += delta
            res[:mind] += delta
            res[:acuity] += delta
        when :orc_mage
            res[:might] += delta
            res[:physicality] += delta
            res[:personality] += delta * 2
            res[:mind] += delta * 2
            res[:acuity] += delta * 2
        when :orc_melee
            res[:might] += delta * 2
            res[:physicality] += delta * 2
        when :orc_thrower
            res[:might] += delta
            res[:physicality] += delta
            res[:speed] += delta
        when :skeleton
            res[:personality] = 2
            res[:mind] = 3
            res[:acuity] = 3
        when :zombie
            res[:speed] = 2
            res[:personality] = 2
            res[:mind] = 2
            res[:acuity] = 2
        end

        res
    end
end