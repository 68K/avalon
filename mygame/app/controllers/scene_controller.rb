# Controls which scene the game is in, and transitioning between them
class SceneController

    attr_reader :current

    def initialize
        @current = :title_scene
    end

    def change_scene args, from, to, opts = {}
        if to == :dungeon_scene
            # TODO: the method should be passed a dungeon symbol, not the name
            # This will require the code for panels to give panel options more properties than just the display label.
            raise "Need to provide opts :dungeon_name if transitioning to dungeon" unless opts[:dungeon_name]
            
            dungeon_def = args.state.dungeons.find { |d| d[:name] == opts[:dungeon_name] }
            raise "No dungeon definition for dungeon name #{opts[:dungeon_name]}" unless dungeon_def

            # Preparing the dungeon will take some time and number crunching, mute the audio now to stop it crackling while frames are dropped
            args.state.audio.stop_all

            # If party is empty, generate one
            # This will need several ticks...
            if args.state.player_party.empty?
                args.state.player_party.begin_generating_default_party
            end
    
            args.state.step_count = 0
            args.state.steps_to_next_encounter = 4

            args.state.dungeon_spec = dungeon_def
            # Generate dungeon if either a dungeon is not loaded or if player has asked to enter a different one
            if args.state.dungeon.nil? || dungeon_def[:id] != args.state.dungeon.id
                args.state.dungeon = Dungeon.new(args, args.state.dungeon_map_generator, 
                                                 depth: dungeon_def[:depth], level: dungeon_def[:base_level], id: dungeon_def[:id], title: dungeon_def[:name], style: dungeon_def[:style])
            end

            # The dungeon will generate over several ticks and cannot immediately be interacted with.
            # But when being re-entered things can be immediately updated
            if args.state.dungeon.ready?
                # Every time the dungeon is entered, check if the floor being entered should have a quest POI
                args.state.dungeon.update_quest_pois_on_floor(args, args.state.dungeon.current_floor_number - 1)

                # If re-entering the dungeon, the player will still be facing the exit
                args.state.dungeon.current_floor.flip_direction if args.state.dungeon.current_floor.exit_ahead?

                # Play dungeon audio, but only if other loading is complete (else crackling might happen)
                args.state.audio.play_dungeon_ambience if args.state.player_party.ready

                # The render target will still show the last view, re-render the viewport
                args.state.dungeon.current_floor.update_render args

                # Initial map update
                args.state.dungeon.update_player_map args
            end

            # TODO: Variable dungeon travel times based on the dungeon, the party ability etc
            args.state.calendar.add_hours(3)

            @current = :dungeon_scene
        elsif to == :town_scene
            args.state.audio.play_village_music
            @current = :town_scene
            args.state.panels.open_town_options args
            update_available_dungeons(args)
            if from == :party_creation_scene
                args.state.calendar.set_to_game_start_date
            else
                # TODO: Variable dungeon travel times based on the dungeon, the party ability etc
                args.state.calendar.add_hours(3)
            end
        elsif to == :title_scene
            args.state.audio.stop_all
            @current = :title_scene
        elsif to == :save_slot_scene
            @current = :save_slot_scene
            args.state.save_controller.load_slot_summaries
        elsif to == :party_creation_scene
            @current = :party_creation_scene
            args.state.panels.open_choice_dialog(args, :confirm_quick_start, "Quick start?")
        elsif to == :game_over_scene
            args.state.audio.play_game_over_music
            args.state.ticks_on_game_over_screen = 0
            @current = :game_over_scene
        else
            raise "Unknown 'to' scene '#{to}'"
        end
    end

    def switch_to a, to
        change_scene a, @current, to
    end

    def in_dungeon?
        @current == :dungeon_scene
    end

    def in_town?
        @current == :town_scene
    end

    def game_over a
        switch_to a, :game_over_scene
        a.state.in_combat = false
        a.state.player_party.clear_members
        a.state.panels.clear_all
    end

    # Prune dungeons that belong to a quest that is no longer current
    # e.g. if switching to town when transported at the end of a quest
    def update_available_dungeons a
        # Delete dungeons
        a.state.dungeons.delete_if { |d| d[:quest] && a.state.quests.progress[d[:quest]][:state] == :completed }
        # Delete chest data for non-existent dungeons
        current_dungeon_ids = a.state.dungeons.map { |d| d[:id] }
        a.state.opened_chests.delete_if { |c| !current_dungeon_ids.include?(c[:dungeon]) }
        a.state.player_maps.update_dungeon_id_list!(current_dungeon_ids)
    end

    def serialize
        {}
    end

    def inspect
        self.to_s
    end

    def to_s
        "SceneController"
    end
end