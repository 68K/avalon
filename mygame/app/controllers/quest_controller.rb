require 'app/quest.rb'

# Class that manages quests. Stores the list of all quests and tracks player progress
# Progress enum is:
# :not_started - default starting state, player cannot view this quest
# :started - quest has started and appears in player's journal
# :completed - player has completed this quest
# :failed - player ended this quest unsuccesfully
class QuestController

    attr_accessor :dungeon_id_sequence, :quest_id_sequence
    attr_reader :progress, :quest_list

    def initialize
        init_quests
        init_progress
    end

    def init_quests
        @quest_list = []
        @quest_list << Quest.from_spec({ id: 1, title: "Kill the big ratkin", requirements: {event_passed: 1},
            goals: [
                      {
                        phase: 1,
                        goal_id: 1,
                        type: :kill,
                        desc: "Find and kill the ratkin in the Underhalls",
                        dungeon_id: 1, 
                        floor: 2,
                        base_level: 5,
                        bestiary_id: 5,
                        name: "Azdpar the biting"
                      },
                      {
                        phase: 2,
                        goal_id: 1,
                        type: :speak_to_innkeep,
                        desc: "Let the innkeep know that Azdpar is dead",
                        goal_complete_lines: ["You killed that ratkin? Gods be praised!", "We can only offer you a modest reward, but you have our eternal thanks."]
                      }
                   ],
            rewards: { silver: 50, xp: 100 }
        })
        @quest_list << Quest.from_spec({ id: 2, title: "Rescue the villager", requirements: {event_passed: 4, quest_complete: 1},
            goals: [
                {
                    phase: 1,
                    goal_id: 1,
                    type: :reach_poi,
                    desc: "Find the lost villager deep in the Underhalls",
                    dungeon_id: 1, 
                    floor: 3,
                    name: "Lost Villager",
                    goal_complete_lines: ["Oh you saved me! I think I can make it back above ground myself now...", "Thank you! Come see me in town, I run the town shop!"]
                }
            ],
            rewards: { silver: 100, xp: 150 }
        })
    end

    def init_progress
        @progress = {}
        @quest_list.each do |quest|
            @progress[quest.id] = { state: :not_started, current_phase: 1, current_goal: 1 }
        end
        # Track of max quest ID that's ever been used. Persisted in save file.
        # TODO: Tidy this a little - on loading a save, reset this to the lowest value that doesn't exist in the current quest set
        @quest_id_sequence = 2
        @dungeon_id_sequence = 10
    end

    def next_quest_id
        @quest_id_sequence += 1
        return @quest_id_sequence
    end

    def next_dungeon_id
        @dungeon_id_sequence += 1
        return @dungeon_id_sequence
    end

    # @return Array of quest spec hashes for quests available at the guild
    def generate_guild_quest_specs state, number_of_quests
        res = []
        # TODO: Generate some random quests that aren't skillbook quests
        possible_next_skills = state.player_party.next_skill_grades
        raise "No possible next skills" unless possible_next_skills && possible_next_skills.any?
        number_of_quests.times do |i|
            skill = possible_next_skills.keys.sample
            res << generate_skillbook_quest_spec(skill, possible_next_skills[skill])
        end

        res
    end

    # @return Hash being the quest spec for a quest to 
    def generate_skillbook_quest_spec skill, grade
        res = {}
        res[:id] = next_quest_id
        res[:is_skill_promotion_quest] = true
        res[:dungeon] = {}
        res[:dungeon][:id] = next_dungeon_id
        res[:dungeon][:style] = Dungeon.random_style
        res[:dungeon][:name] = Dungeon.random_dungeon_name(res[:dungeon][:style])
        res[:dungeon][:depth] = 2
        res[:dungeon][:level] = 10
        res[:dungeon][:available] = true
        res[:dungeon][:quest] = res[:id]
        res[:title] = "Acquire the #{grade.to_s.capitalize} #{skill.to_s.capitalize} tome from #{res[:dungeon][:name]}"
        # No requirements; it shouldn't be generated if player isn't ready
        res[:requirements] = {}
        res[:goals] = [
            {
                phase: 1,
                goal_id: 1,
                type: :reach_poi,
                return_to_town: true,
                desc: "Find the tome on floor #{res[:dungeon][:depth]} of #{res[:dungeon][:name]}",
                dungeon_id: res[:dungeon][:id], 
                floor: res[:dungeon][:depth],
                name: "#{grade.to_s.capitalize} #{skill.to_s.capitalize} tome",
                goal_complete_lines: ["You found the tome! Now the Adventurers Guild can train #{skill.to_s.capitalize} to #{grade.to_s.capitalize}."]
            }
        ]
        res[:rewards] = { tome: { skill: skill, grade: grade } }
        res
    end

    # Accept the guild quest at index idx; add it to quest_list and remove the spec from guild quest list
    def accept_guild_quest a, idx
        quest_spec = a.state.guild.quests[idx]
        quest_id = quest_spec[:id]
        @quest_list << Quest.from_spec(quest_spec)
        @progress[quest_id] = { state: :started, current_phase: 1, current_goal: 1 }
        a.state.guild.quest_accepted(quest_id)
        a.state.dungeons << quest_spec[:dungeon]
    end

    def progress_from_save_file data
        # JSON has string keys that will have been changed to e.g. `:"1"`, cast to a numeric key here
        @progress = data.map { |k,v| [k.to_s.to_i, v] }.to_h
        # Symbol for `state` will have been stringified, symbolise it again
        @progress.keys.each do |p|
            @progress[p][:state] =  @progress[p][:state].to_sym
        end
    end

    # Get current goal for a given quest
    def current_goal_of_quest q
        q = quest_for_id(q) if q.is_a? Numeric
        raise "#{q} is not a quest" unless q.is_a? Quest
        raise "Quest #{q} has no id" unless q.id
        raise "Quest with ID #{q.id} has no progress entry" unless @progress[q.id]
        res = q.goals.select { |g| g[:phase] == @progress[q.id][:current_phase] && g[:goal_id] == @progress[q.id][:current_goal] }.first
        raise "No goal of quest matches current progress" unless res
        # Add a reference to the quest so it can be referenced back
        res[:quest_id] = q.id
        res
    end

    # Get goal for the current dungeon
    # @param  dungeon   Instance of Dungeon to compare the dungeon ID and current floor with
    # @param  goal_type What type of goal to find (e.g. need to find :kill when forming an enemy party)
    # @return Hash      The first current goal of a current quest found that is tied to the current dungeon floor
    # @return nil       If no current goal of any current quest is tied to the current dungeon floor
    def current_goal_for_dungeon_floor dungeon, goal_type = :any
        current_quests.each do |q|
            goal = current_goal_of_quest q
            if ((goal_type == :any || goal[:type] == goal_type) && goal.has_key?(:dungeon_id) && dungeon.id == goal.dungeon_id && dungeon.current_floor_number == goal.floor)
                goal[:quest_id] = q.id
                return goal
            end
        end
        return nil
    end

    # Get goal for speaking to innkeep
    # @return Hash    The first current goal of a current quest found that requires speaking to the innkeep
    # @return nil     If no current goal of any current quest is tied to speaking to the innkeep
    def current_goal_for_innkeep
        current_quests.each do |q|
            goal = current_goal_of_quest q
            if (goal[:type] == :speak_to_innkeep)
                # Add a reference to the quest so it can be referenced back
                goal[:quest_id] = q.id
                return goal
            end
        end
        return nil
    end

    def goal_complete args, goal
        raise "Given goal has no quest_id" unless goal.has_key?(:quest_id)
        current_quests.each do |q|
            if q.id == goal[:quest_id]
                # Next goal in current phase
                if next_goal = q.goals.select { |g| g[:goal_id] == (goal[:goal_id] + 1) }.first
                    @progress[q.id][:current_goal] = next_goal[:goal_id]
                    return true
                elsif next_phase = q.goals.select { |g| g[:phase] == (goal[:phase] + 1) && g[:goal_id] == 1 }.first
                    # Or goal 1 of next phase
                    @progress[q.id][:current_goal] = 1
                    @progress[q.id][:current_phase] = next_phase[:phase]
                    return true
                else
                    # Else quest is complete
                    quest_complete args, q
                    return true
                end
            end
        end
        return false
    end

    def quest_complete args, q
        # TODO: Need character particle effect if level up from quest
        @progress[q.id][:state] = :completed
        args.state.player_party.add_silver(q.rewards[:silver]) if q.rewards[:silver]
        args.state.player_party.capable_members.each { |c| c.add_xp(q.rewards[:xp]) } if q.rewards[:xp]
        args.state.player_party.add_skill_book(q.rewards[:tome][:skill],q.rewards[:tome][:grade]) if q.rewards[:tome]
    end

    # Update the state of quests.
    # @return true if the update caused a change to quest progress
    def update a
        res = false
        @quest_list.each do |q|
            if @progress[q.id][:state] == :not_started
                if q.requirements.empty?
                    @progress[q.id][:state] = :started
                    res = true
                else
                    # TODO: Also start quest based on :quest_complete and :quest_ended
                    if a.state.events.finished_events.include?(q.requirements[:event_passed])
                        @progress[q.id][:state] = :started
                        res = true
                    end
                end
            end
        end
        res
    end

    def current_quests
        @quest_list.select { |q| @progress[q.id][:state] == :started }
    end

    def quest_for_id i
        c = @quest_list.select { |q| q.id == i }
        raise "Unable to get a single quest for quest ID #{i}. Got these quests: #{c}" unless c.length == 1
        c.first
    end

    def already_doing_skill_promotion_quest?
        current_quests.select { |q| q.is_skill_promotion_quest }.any?
    end

    def serialize
        {}
    end

    def inspect
        self.to_s
    end

    def to_s
        "QuestController"
    end
end