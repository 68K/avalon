# Class that handles simple animations on screen

# Each animation is a hash with these properties:
# type              type of animation. Primarily used to determine the path of the sprite to draw.
# combat            true if this is a combat animation and CombatController needs to wait for the animation to end for combat to flow
# x                 x to render at
# y                 y to render at
# w                 render width
# h                 render height
# frame             current frame of animation
# length            number of frames of animation
# static            true if this animation does not cycle through animation frames; in which case frame_tick and frame_duration are n/a and each frame is 1 tick long
# frame_tick        current tick for current frame of animation
# frame_duration    number of ticks each frame of animation lasts
# rest_duration     number of ticks the animation should rest after frames complete
# rest              number of ticks the animation has rested
# moving            true if this animation moves across the screen
# dx                delta applied to x each frame if moving is true
# dy                delta applied to y each frame if moving is true
# r,g,b,a           r,g,b,a for the render
# angle             angle for the render
class AnimationController

    # exposed for debugging
    attr_reader :anims

    def initialize
        @anims = []
    end

    def start_cut_animation x, y
        @anims << { x: x - 128, y: y, w: 256, h: 128,
                    frame: 0, frame_tick: 0, frame_duration: 3, length: 6,
                    rest: 0, rest_duration: 20,
                    type: :cut, combat: true,
                    r: 255, g: 255, b: 80, a: 255, angle: 0
                }
    end

    def start_bash_animation x, y
        @anims << { x: x, y: y, w: 128, h: 128,
                    frame: 0, frame_tick: 0, frame_duration: 3, length: 7,
                    rest: 0, rest_duration: 20,
                    type: :bash, combat: true,
                    r: 255, g: 255, b: 80, a: 255, angle: 0
                }
    end

    def start_cast_animation x, y, opts = {}
        raise "Need to specify :combat in opts" unless opts.has_key?(:combat)
        if opts[:cast_on_player]
            w = 128
            h = 64
        else
            w = 128
            h = 256
        end
        @anims << { x: x - w / 2, y: y - h / 2, w: w, h: h,
                    frame: 0, frame_tick: 0, frame_duration: 4, length: 11,
                    rest: 0, rest_duration: 20,
                    type: :cast, combat: opts[:combat],
                    r: opts[:r] || 255, g: opts[:g] || 255, b: opts[:b] || 255, a: opts[:a] || 255, angle: 0
                }
    end

    def start_arrow_animation sx, sy, tx, ty
        angle = Math.atan2(ty - sy, tx - sx) * 180 / Math::PI + 90
        distance = $gtk.args.geometry.distance([sx,sy],[tx,ty]).abs
        duration = distance/20      # animation should last around 1 frame per 20 pixels needed to travel
        step = distance/duration    # each frame, step by the distance to travel divided by the number of frames duration
        dx = Math.sin(angle * Math::PI / 180) * step
        dy = -1 * Math.cos(angle * Math::PI / 180) * step
        @anims << { x: sx, y: sy, w: 128, h: 128,
                    frame: 0, length: duration, rest: 0, rest_duration: 20,
                    combat: true, static: true, moving: true,
                    dx: dx, dy: dy, type: :arrow,
                    r: 255, g: 255, b: 255, a: 255, angle: angle
                }
    end

    def get_sprites
        @anims.select { |a| a[:frame] < a[:length] }.map do |a|
            path = a[:static] ? "sprites/effects/#{a[:type]}.png" : "sprites/effects/#{a[:type]}_#{a[:frame]}.png"
            { x: a[:x], y: a[:y], w: a[:w], h: a[:h], path: path, r: a[:r], g: a[:g], b: a[:b], a: a[:a], angle: a[:angle], primitive_marker: :sprite }
        end
    end

    def update
        @anims.each do |a|
            if a[:frame] >= a[:length]
                a[:rest] += 1
                next
            end
            if a[:static]
                a[:frame] += 1
            else
                a[:frame_tick] += 1
                if a[:frame_tick] >= a [:frame_duration]
                    a[:frame] += 1
                    a[:frame_tick] = 0
                end
            end
            if a[:moving]
                a[:x] += a[:dx]
                a[:y] += a[:dy]
            end
        end
        
        @anims.delete_if { |a| a[:rest] >= a[:rest_duration] }
    end

    # The combat system needs to know if combat-related animations are all done
    def combat_animations_done?
        @anims.select { |a| a[:combat] == true }.empty?
    end
end