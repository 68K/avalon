# Class managing magic spells in the game
# Holds definitions for spells and handles the details of how they work
class MagicController

    attr_reader :spell_definitions

    def initialize
        init_spells
    end

    # Definitions of spells
    # :target is the general targeting type: :ally, :enemy or :world
    def init_spells
        @spell_definitions = []
        @spell_definitions << { id: 0, name: "Heal", requirements: { bio: :novice, energy: :novice }, target: :ally, base: 4, cost: 4 }
        @spell_definitions << { id: 1, name: "Harm", requirements: { bio: :novice, void: :novice }, target: :enemy, base: 4, cost: 4 }
        @spell_definitions << { id: 2, name: "Mana Spear", requirements: { energy: :novice }, target: :enemy, base: 4, cost: 4 }
        @spell_definitions << { id: 3, name: "Fire Bolt", requirements: { energy: :novice }, target: :enemy, base: 4, cost: 4 }
        @spell_definitions << { id: 4, name: "Freeze", requirements: { energy: :novice, void: :novice }, target: :enemy, base: 4, cost: 4 }
        @spell_definitions << { id: 5, name: "Rock", requirements: { energy: :novice, kinetics: :novice }, target: :enemy, base: 4, cost: 4 }
        @spell_definitions << { id: 6, name: "Bless", requirements: { neural: :novice }, target: :ally, base: 4, cost: 4 }
    end

    def serialize
        {}
    end

    # Return initial set of spells for a character
    # @param  character     the Character to provide initial spells for
    # @return Array         array of spell IDs
    def initial_spells character
        raise "character should be of class Character" unless character.is_a? Character
        res = []
        @spell_definitions.each do |s|
            valid = true
            s[:requirements].each do |k,v|
                valid = false if !character.skills.has_key?(k) || greater_grade?(v, character.skills[k][:grade])
            end
            res << s[:id] if valid
        end
        res
    end

    # Get spell name for ID
    def spell_name spell_id
        spell = @spell_definitions.select { |sd| sd.id == spell_id }.first
        raise "No such spell definition with ID #{spell_id}" unless spell
        spell.name
    end

    # Get spell ID for name
    def spell_id spell_name
        spell = @spell_definitions.select { |sd| sd.name == spell_name }.first
        raise "No such spell definition with name #{spell_name}" unless spell
        spell.id
    end

    # Return a character's combat castable spells
    # @param  Character         character
    # @return Array[Numeric]    Array of spell IDs this character can cast in combat
    def combat_castable_spells character
        raise "character should be of class Character" unless character.is_a? Character
        res = []
        character.known_spells.each do |s|
            res << s if combat_castable? s
        end
        res
    end

    def combat_castable? spell_id
        # TODO: Update with any spells NOT applicable in combat
        ! [].include?(spell_id)
    end

    # Return targeting type of a given spell for a character
    # @return   :single The spell can target a single party member
    # @return   :rank   The spell can target a party rank
    # @return   :party  The spell can target the entire party 
    def target_type spell_id, character
        raise "No such spell ID #{spell_id}" unless @spell_definitions.map { |s| s[:id] }.include?(spell_id)
        raise "character should be of class Character" unless character.is_a? Character
        case spell_id
        when 0,1,2,3,4,5
            :single
        when 6
            character.skills[:neural] == :master ? :party : :member
        end
    end

    # Check if a spell can be cast from the spell list
    def can_cast_from_spell_list? id
        raise "No spell with id #{id}" unless spell = @spell_definitions.select { |s| s.id == id }.first
        return spell[:target] != :enemy
    end

    def can_cast? casting_character, spell_id
        raise "No such spell ID #{spell_id}" unless spell = @spell_definitions.select { |s| s[:id] == spell_id }.first
        raise "casting_character should be of class Character" unless casting_character.is_a? Character
        casting_character.mp >= spell.cost
    end

    def cast_on_ally! spell_id, casting_character, ally_character
        raise "No such spell ID #{spell_id}" unless spell = @spell_definitions.select { |s| s[:id] == spell_id }.first
        raise "casting_character should be of class Character" unless casting_character.is_a? Character
        raise "ally_character should be of class Character" unless ally_character.is_a? Character
        casting_character.spend_mp(spell.cost)
        cx,cy = $gtk.args.state.player_party.centre_of_hud_panel(ally_character)
        case spell_id
        when 0 # Heal
            heal_amount = 4 + (casting_character.skills[:bio][:level] * (1 + 0.01 * casting_character.stats[:mind])).ceil
            heal_amount *= 1.3 if [:skilled, :expert, :master].include?(casting_character.skills[:bio][:grade])
            heal_amount *= 1.3 if [:expert, :master].include?(casting_character.skills[:bio][:grade])
            heal_amount *= 1.3 if [:master].include?(casting_character.skills[:bio][:grade])
            ally_character.heal(heal_amount.ceil)
            $gtk.args.state.logs << "#{casting_character.name} surrounds #{ally_character.name} in healing light!"
            $gtk.args.state.particle_effects << ParticleEffect.new($gtk.args, {length: 30, spread_angle: 360, emit_x: cx, emit_y: cy, sprite: "sprites/particles/cross.png", a: 128, r: 0, b: 0, gravity: 0.2, particle_life: 30 } )
        when 6 # Bless
            # TODO: Add bless buff
            $gtk.args.state.logs << "#{casting_character.name} fills #{ally_character.name} with positive energy!"
            $gtk.args.state.particle_effects << ParticleEffect.new($gtk.args, {length: 30, spread_angle: 360, emit_x: cx, emit_y: cy, angle_sprite: true, sprite: "sprites/particles/needle.png", a: 128, b: 0, particle_life: 10 } )
        else
            raise "Don't know how to cast spell ID #{spell_id}"
        end
    end

    def cast_on_enemy! spell_id, casting_character, enemy
        raise "No such spell ID #{spell_id}" unless spell = @spell_definitions.select { |s| s[:id] == spell_id }.first
        raise "casting_character should be of class Character" unless casting_character.is_a? Character
        raise "enemy should be of class Creature" unless enemy.is_a? Creature
        casting_character.spend_mp(spell.cost)
        # TODO: Have spells work differently
        spell_dmg = spell[:base] * (amplify(spell, casting_character)).ceil
        enemy.apply_damage(spell_dmg)
    end

    # @return   true    if a is a greater skill grade than b
    def greater_grade? a, b
        raise "Param #{a} not a valid skill grade" unless [:novice, :skilled, :expert, :master].include? a
        raise "Param #{b} not a valid skill grade" unless [:novice, :skilled, :expert, :master].include? b
        case b
        when :novice
            return a != :novice
        when :skilled
            return [:expert, :master].include? a
        when :expert
            return a == :master
        when :master
            return false
        end
    end

    def grade_diff a,b
        raise "Param #{a} not a valid skill grade" unless [:novice, :skilled, :expert, :master].include? a
        raise "Param #{b} not a valid skill grade" unless [:novice, :skilled, :expert, :master].include? b
        case a
        when :novice
            return 0 if b == :novice
            return 1 if b == :skilled
            return 2 if b == :expert
            return 3 if b == :master
        when :skilled
            return -1 if b == :novice
            return 0 if b == :skilled
            return 1 if b == :expert
            return 2 if b == :master
        when :expert
            return -2 if b == :novice
            return -1 if b == :skilled
            return 0 if b == :expert
            return 1 if b == :master
        when :master
            return -3 if b == :novice
            return -2 if b == :skilled
            return -1 if b == :expert
            return  0 if b == :master
        else
            raise "Cannot compare grades #{a} , #{b}"
        end
    end

    # Amount to amplify a spell power
    # @return amount to amplify the base power 
    def amplify spell, caster
        # TODO: this needs to be more detailed and model individual spells
        res = 1
        res += 0.01 * caster.stats[:mind]
        res += 0.01 * caster.stats[:personality]
        spell[:requirements].keys.each do |r|
            raise "Character #{caster} does not have spell requirement skill #{r}" unless caster.skills.has_key?(r)
            d = grade_diff(spell[:requirements][r], caster.skills[r][:grade])
            res += ((0.02 * caster.skills[r][:level]) * (d + 2))
        end

        res
    end

    def inspect
        self.to_s
    end

    def to_s
        "MagicController"
    end
end