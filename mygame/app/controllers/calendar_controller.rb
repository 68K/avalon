# Class controlling the calendar in the game
# Time passes as:
# - 60 seconds in a minute
# - 60 minutes in an hour
# - 24 hours in a day
# - 6 days in a week
# - 36 days in a month
# - 10 months in a year
#
# Suggested time increments in game:
# - Enter or leave dungeon - 1 hour * dungeon index
# - Teleport in or out of dungeon - 1 minute
# - Rest at inn - 6am next morning (or 8 hours if resting after midnight or before 6PM)
# - Step forward in dungeon - 12 seconds
# - Open chest in dungeon - 5 minutes
# - 1 active combat turn - 20 seconds
# - Enter or leave a town service - 2 minutes
# - Upgrade a skill at the guild - 1 week (free if additional characters upgrade at the same time)
class CalendarController

    attr_reader :seconds, :minutes, :hours, :day_of_month, :month, :year

    def initialize
        @day_names = %w[Albu Niei Saei Johi Ganf Roechs]
        @month_names = %w[Alrone Beltrim Lantrim Bacepecore Dungown Craigagh Hotare Atheath Baligo Vahuru]
        @seconds = 0
        @minutes = 0
        @hours = 0
        @day_of_month = 0    # 0 indexed
        @month = 0           # 0 indexed
        @year = 0
    end

    def ordinal n 
        suffix = "th"
        last_digit = n % 10
        suffix = "st" if last_digit == 1 && n != 11
        suffix = "nd" if last_digit == 2 && n != 12
        suffix = "rd" if last_digit == 3 && n != 13

        n.to_s + suffix
    end

    def set_to_game_start_date
        @hours = 12
        @minutes = 30
        @seconds = 30
        @day_of_month = 22
        @month = 3
        @year = 327
    end

    def move_forward_to_dawn
        # TODO: For the purposes of resting, might want option of 8 hours instead of until dawn
        add_days(1)
        @hours = 6
        @minutes = @seconds = 0
    end

    def to_timestamp
        @seconds + @minutes * 60 + @hours * 3600 + @day_of_month * 86400 + @month * 3110400 + @year * 31104000
    end

    def serialize
        { seconds: @seconds, minutes: @minutes, hours: @hours, day_of_month: @day_of_month, month: @month, year: @year }
    end

    def from_save_data data
        # TODO: Not safe - verify each required field
        data.keys.each do |k|
            instance_variable_set("@#{k}", data[k])
        end
    end

    def to_s
        date_time_string
    end

    def date_time_string
        "#{@hours}:#{@minutes.to_s.rjust(2, "0")} #{date_string}"
    end

    def day_time_string
        "#{@hours}:#{@minutes.to_s.rjust(2, "0")} #{month_of_year_string} #{ordinal(@day_of_month + 1)}"
    end

    def date_string
        # @day_of_month is zero-indexed so add 1
        "#{day_of_week_string}, #{ordinal(@day_of_month + 1)} of #{month_of_year_string} #{@year}"
    end

    # @return Current second timestamp / 86400 (Seconds in a day) % 6 (number of days in the week)
    def day_of_week
        to_timestamp / 86400 % 6
    end

    def day_of_week_string
        @day_names[day_of_week]
    end

    def month_of_year_string
        @month_names[@month]
    end

    def add_time amount = {}
        add_years(amount[:years]) if amount[:years]
        add_months(amount[:months]) if amount[:months]
        add_days(amount[:days]) if amount[:days]
        add_hours(amount[:hours]) if amount[:hours]
        add_minutes(amount[:minutes]) if amount[:minutes]
        add_seconds(amount[:seconds]) if amount[:seconds]
    end

    def add_years amount
        @year += amount
    end

    def add_months amount
        @month += amount
        if @month >= 10
            @year += (@month / 10)
            @month %= 10
        end
    end

    def add_days amount
        @day_of_month += amount
        if @day_of_month >= 36
            add_months((@day_of_month / 36).to_i)
            @day_of_month %= 36
        end
    end

    def add_hours amount
        #$gtk.log "Initially #{@hours} hours and #{@day_of_month} day of month"
        @hours += amount
        if @hours >= 24
            add_days((@hours / 24).to_i)
            @hours %= 24
        end
        #$gtk.log "Finally #{@hours} hours and #{@day_of_month} day of month"
    end

    def add_minutes amount
        @minutes += amount
        if @minutes >= 60
            add_hours((@minutes / 60).to_i)
            @minutes %= 60
        end
    end

    def add_seconds amount
        raise "Cannot add nil seconds" unless amount
        @seconds += amount
        if @seconds >= 60
            add_minutes((@seconds / 60).to_i)
            @seconds %= 60
        end
    end
end