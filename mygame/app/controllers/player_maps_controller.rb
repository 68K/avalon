# Class that manages the player maps i.e. what the player has uncovered.
# This data is not stored within Dungeon or DungeonMap because those classes are temporary
# i.e. are disposed when the dungeon is not the current dungeon. Whereas this map data needs to persist.
class PlayerMaps

    # Use the same logic in a dungeon map - a tile can be &'d with these values to check if a wall is known
    # 1 - North
    # 2 - West
    # 4 - South
    # 8 - East

    # Also use the following bits for data:
    # 16  - Tile has a chest
    # 32  - Tile has a quest POI
    # 64  - Tile has an unknown point of interest
    # 128 - Tile is undiscovered

    def initialize
        # @data is a hash keyed with dungeon_id, mapped to a hash keyed with floor number, mapped to: 
        #       :tiles              2D array of known tiles
        #       :exit_x             x ordinate of exit, nil if not discovered or not present
        #       :exit_y             y ordinate of exit, nit if not discovered or not present
        #       :exit_dir           dir of exit in its given tile
        #       :stairs_down_x      x ordinate of stairs down, nil if not discovered or not present
        #       :stairs_down_y      y ordinate of stairs down, nil if not discovered or not present
        #       :stairs_down_dir    dir of stairs down in its given tile
        #       :stairs_up_x        x ordinate of stairs up, nil if not discovered or not present
        #       :stairs_up_y        y ordinate of stairs up, nil if not discovered or not present
        #       :stairs_up_dir      dir of stairs up in its given tile
        @data = {}
    end

    def has_dungeon_floor? id, floor_number
        return false unless @data[id]
        return @data[id][floor_number]
    end

    # Add a new entry for a floor number (or reset that floor to an unknown map)
    def new_dungeon_floor id, floor_number, width, height
        @data[id] ||= {}
        @data[id][floor_number] = {}
        @data[id][floor_number][:tiles] = []
        empty_row = []
        width.times { |_| empty_row << 128 }
        height.times { |_| @data[id][floor_number][:tiles] << empty_row.dup }
    end

    # Return dungeon floor tiles. It is assumed these may be edited in-place by another class.
    def get_dungeon_floor_map id, floor_number
        raise "No map data for dungeon id #{id}" unless @data.has_key?(id)
        raise "No map data for floor #{floor_number} in dungeon #{id}" unless @data[id].has_key?(floor_number)
        @data[id][floor_number]
    end

    # If a dungeon is removed from the game, delete its player map data
    def forget_dungeon
        @data.delete(id)
    end

    def update_dungeon_id_list! ids
        @data.delete_if { |k,v| !ids.include?(k) }
    end

    def to_s
        "PlayerMaps"
    end

    def serialize
        @data
    end

    def to_save_data
        # Cannot have int keys so cast the keys for dungeon ID and for floor number to strings
        @data.map { |k,v| [k.to_s, v.map { |fk,fv| [fk.to_s, fv] }.to_h] }.to_h
    end

    def from_save_data data
        # TODO: Need error handling
        return {} unless data
        # The int (dungeon id) keys needed to be changed to string, which are then changed to symbol, change them back now.
        # Same with the floor number keys inside the dungeon data.
        @data = data.map { |k,v| [k.to_s.to_i, v.map { |fk,fv| [fk.to_s.to_i, fv] }.to_h ] }.to_h
    end
end