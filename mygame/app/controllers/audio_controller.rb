class AudioController
    def initialize args
        @audio = args.audio
        @muted = false
        @playing_combat_intro = false
        @playing_combat_outro = false
        @gains = {}
    end

    def play_dungeon_ambience
        @audio.delete(:music_combat)
        @audio.delete(:village)
        gain = 1.0
        if @muted
            @gains[:music_dungeon_ambience] = 1.0
            gain = 0
        end
        @audio[:music_dungeon_ambience] = {
            input: 'sounds/ambient_emptiness.ogg',
            looping: true,
            gain: gain
        }
    end

    def play_combat_intro
        @audio.delete(:music_dungeon_ambience)
        # It's possible to get back into combat so quickly that the outro from the last fight might still be playing
        @audio.delete(:music_combat_end)
        @playing_combat_outro = false
        gain = 1.0
        if @muted
            @gains[:music_combat_intro] = 1.0
            gain = 0
        end
        @audio[:music_combat_intro] = {
            input: 'sounds/combat_start_000.ogg',
            looping: false,
            gain: gain
        }
        @playing_combat_intro = true
    end

    def play_combat_outro
        @audio.delete(:music_combat)
        gain = 1.0
        if @muted
            @gains[:music_combat_end] = 1.0
            gain = 0
        end
        @audio[:music_combat_end] = {
            input: 'sounds/combat_end_000.ogg',
            looping: false,
            gain: gain
        }
        @playing_combat_outro = true
    end

    def play_combat_music
        @audio.delete(:music_dungeon_ambience)
        gain = 1.0
        if @muted
            @gains[:music_combat] = 1.0
            gain = 0
        end
        @audio[:music_combat] = {
            input: 'sounds/combat_000.ogg',
            looping: true,
            gain: gain
        }
    end

    def play_game_over_music
        @audio.delete(:music_combat)
        @audio.delete(:music_dungeon_ambience)
        gain = 1.0
        if @muted
            @gains[:game_over] = 1.0
            gain = 0
        end
        @audio[:game_over] = {
            input: 'sounds/game_over_000.ogg',
            looping: false,
            gain: gain
        }
    end

    def play_village_music
        @audio.delete(:music_combat)
        @audio.delete(:music_dungeon_ambience)
        gain = 1.0
        if @muted
            @gains[:village] = 1.0
            gain = 0
        end
        @audio[:village] = {
            input: 'sounds/village_000.ogg',
            looping: true,
            gain: gain
        }
    end

    def stop_all
        @audio.keys.each { |a| @audio.delete(a) }
    end

    def mute
        @muted = true
        @gains = {}
        @audio.keys.each { |t| @gains[t] = @audio[t][:gain] }
        @audio.keys.each { |t| @audio[t][:gain] = 0.0 }
    end

    def unmute
        @muted = false
        @gains.keys.each do |t|
            @audio[t][:gain] = @gains[t] if @audio.has_key?(t)
        end
    end

    def toggle_mute
        @muted ? unmute : mute
    end

    # Per-tick update of the audio controller
    # Used to automatically start tracks that follow another one e.g. combat intro sting
    def update
        # There is a noticeable pause when playing an ogg for the first time.
        # For tracks that need to start continuously after another track, work around
        # this by playing the track with 0 gain at the start of the game
        if $gtk.args.state.tick_count == 0
            @audio[:fix] = {
                input: 'sounds/combat_000.ogg',
                gain: 0.0
            }
        end
        if $gtk.args.state.tick_count == 2
            @audio.delete(:fix)
        end

        if $gtk.args.inputs.keyboard.has_focus
            unmute if @muted_from_focus_loss
            @muted_from_focus_loss = false
        else
            @muted_from_focus_loss = true if (!@muted_from_focus_loss && !@muted)
            mute unless @muted
        end

        # If the combat intro sting is ended, play the combat music
        if @playing_combat_intro && @audio[:music_combat_intro].nil?
            @playing_combat_intro = false
            play_combat_music
        end

        # If the combat outro has ended, play dungeon ambience
        if @playing_combat_outro && @audio[:music_combat_end].nil?
            @playing_combat_outro = false
            play_dungeon_ambience
        end
    end
end