class ParticleEffect
    def initialize(a, opts= {})
        @args = a
        @particles = []
        @a = opts[:a] || 255
        @r = opts[:r] || 255
        @g = opts[:g] || 255
        @b = opts[:b] || 255
        @particle_rotate_speed = opts[:particle_rotate_speed] || 0
        @delay = opts[:delay] || 0
        @length = opts[:length] || 40
        @tick = 0 - @delay
        @particle_life = opts[:particle_life] || 20
        @sprite = opts[:sprite] || "sprites/white_square.png"
        @emit_x = opts[:emit_x] || 0
        @emit_y = opts[:emit_y] || 0
        @sound = opts[:sound]
        @emit_angle = opts[:emit_angle] || 0.0          # The angle to emit at
        @angle_sprite = opts[:angle_sprite]             # Whether to angle the sprite with the emit angle
        # TODO: also drift vertical, or at any angle
        @emit_x_drift = opts[:emit_x_drift] || 0
        @emit_frequency = opts[:emit_frequency] || 2
        @spread_angle = opts[:spread_angle] || 40.0
        # Velocity at the emitted angle
        @velocity = opts[:velocity] || 6
        # Pull down per frame
        @gravity = opts[:gravity] || 0
    end

    def done?
        @particles.empty? && @tick > @length
    end

    def update
        # Note that tick starts at 0 - delay so the effect starts when tick increase to 0
        @args.outputs.sounds << @sound if(@sound && (@tick == 0))
        @particles.each do |p|
            p.py -= @gravity * p.age
            p.px += Math.sin(p.angle * Math::PI / 180) * p.velocity
            p.py += Math.cos(p.angle * Math::PI / 180) * p.velocity
            p.render_angle += @particle_rotate_speed
            p.age += 1
        end
        @particles.map! { |p| p.age < @particle_life ? p : nil }
        @particles.compact!
        if @tick >= 0 && @tick < @length
            if (@tick % @emit_frequency == 0)
                emit_angle = (@emit_angle - @spread_angle / 2 + rand(@spread_angle))
                # If the particles do not rotate over time or rotate with the emit angle on emit, render at angle 0
                render_angle = 0
                if @particle_rotate_speed != 0
                    # If this particle will rotate, start either at the emit angle or a random angle
                    render_angle = @angle_sprite ? emit_angle : rand(360)
                else
                    # If this particle will not rotate, angle is either 0 or the emit angle
                    render_angle = @angle_sprite ? emit_angle : 0
                end
                # TODO: Why is render_angle negated?
                @particles << { px: @emit_x, py: @emit_y, velocity: @velocity, angle: emit_angle, age: 0, rotate_speed: @particle_rotate_speed, render_angle: -render_angle }
            end
            @emit_x = @emit_x + @emit_x_drift
        end
        @tick += 1
    end

    def render
        res = []
        @particles.each do |p|
            res << { x: p.px, y: p.py, w: 32, h: 32, path: @sprite, angle: p.render_angle, a: @a, r: @r, g: @g, b: @b, primitive_marker: :sprite }
        end
        res
    end
end