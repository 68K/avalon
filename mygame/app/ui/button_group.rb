# Class representing a set of buttons in the UI
# TODO: This should be renamed panels because it also handles panels without button groups...

class ButtonGroup

    attr_accessor :x, :y, :buttonw, :buttonh, :current_text_index, :event,
                  :input_string, :input_string_max_length, :input_string_chars_entered,
                  :layout, :margin, :caption, :focused, :quest_goal, :scroll_length, :selection, :item, :selected_slot
    attr_reader :list, :options, :length, :text_pages, :type

    def initialize(type)
        @type = type
        @layout = :vertical
        @caption = nil
        @x = 0
        @y = 0
        @buttonw = 80
        @buttonh = 20
        @options = {}        # Hash storing options in the button list, mapping from label strings to whether the button is enabled
        @list = nil          # Points to a collection of items responding to :name
        @selected_item = nil # Points to the item being considered e.g. the buy confirmation dialog has a pointer to the item considered for purchase
        @scroll_head = 0     # If the panel is a scrolling view, this stores the topmost value to render
        @scroll_length = 15  # Number of items to render
        @length = 0
        @focused = false
        @selection = 0
        @selected_slot = :helmet # Specific to the equipment panel, the slot currently selected
        @event = -1
        @quest_goal = nil
        @input_string = nil             # String being input, if this is a string editor
        @input_string_max_length = 5    # Max length of string being input
        @input_string_chars_entered = 0 # Number of chars entered so far (user can scroll through already-set characters without confirming input)

        # Text to display
        @current_text_index = 0
        @current_text = [""]
        @chars_width = 40
        @text_pages = []

        # Style properties
        @@valid_style_opts = [:border_r, :border_b, :border_g, :border_a, :border_w, :bg_r, :bg_b, :bg_g, :bg_a, :margin, :text_size]
        @border_r = @border_g = @border_b = 255
        @border_a = 255
        @border_w = 2
        @bg_r = @bg_g = @bg_b = 128
        @bg_a = 255
        @margin = 0 # Margin between buttons and panel edges and other buttons
        @text_size = 3
    end

    def set_text t
        raise "t needs to be an array of strings, one per page for this dialog" unless t.is_a? Array
        @text_pages = t
        set_current_text
    end

    def set_current_text page = @current_text_index
        @current_text = $gtk.args.string.wrapped_lines @text_pages[page], @chars_width
        @current_text_index = page
    end

    def select_previous
        return unless @selection > 0
        @selection -= 1
        @scroll_head -= 1 if @selection < @scroll_head
    end

    def select_next
        return unless @selection < @length - 1
        @selection += 1
        @scroll_head += 1 if @selection >= @scroll_head + @scroll_length
    end

    def set_style opts
        raise "opts should be an array of style opts" unless opts && opts.respond_to?(:each)
        opts.keys.each do |o|
            raise "Unsupported style opt #{o.to_s}" unless @@valid_style_opts.include?(o)
        end
        opts.keys.each do |o|
            var_name = "@#{o.to_s}"
            self.instance_variable_set(var_name, opts[o])
        end
    end

    def set_options opts
        raise "opts should be an array of label strings or a hash of strings to enabled state" unless opts.respond_to?(:each)
        @options = {}
        @selection = 0
        @length = opts.length
        if opts.respond_to?(:key)
            opts.keys.each { |o| @options[o] = opts[o] }
        else
            # If just an array, assume all are enabled
            opts.each { |o| @options[o] = true }
        end
    end

    def set_list list
        raise "list should be an array" unless (list.respond_to?(:each) && (! list.respond_to?(:keys)))
        @list = list
        @length = list.length
    end

    # If the game may have changed the list, update the length
    def reset_length
        @length = list.length
        # If the last item disappeared, move the selection back
        if list.length > 0 && @selection >= list.length
            select_previous
            # Scroll head needs to move 1 back, to get the empty slot out of view
            @scroll_head -= 1 if @scroll_head >= 0
        end
    end

    # TODO: Enter character should only be possible on last character
    def roll_string_character
        @input_string[@selection] = case c = @input_string[@selection].ord
        when 48..56         # 0-9
            c + 1
        when 57             # 9 goes to * character if on last character, else A
            (@selection == @input_string_chars_entered && (@input_string_chars_entered >0)) ? 42 : 65
        when 42             # * character goes to A
            65
        when 65..89         # A-Z
            c + 1
        when 90             # Z goes to a
            97
        when 97..121        # a-z
            c + 1
        when 122            # z goes to 0
            48
        end.chr
    end

    # TODO: Enter character should only be possible on last character
    def rollback_string_character
        char_code = @input_string[@selection].ord
        case char_code
        when 49..57 # 0-9
            char_code -= 1
        when 48     # 0 goes to z
            char_code = 122
        when 98..122 # a-z
            char_code -= 1
        when 97 # a goes to Z
            char_code = 90
        when 66..90 # A-Z
            char_code -= 1
        when 65 # A goes to * character on last character, else 9
            char_code = (@selection == @input_string_chars_entered && (@input_string_chars_entered >0)) ? 42 : 57
        when 42 # * character goes to 9
            char_code = 57
        end
        @input_string[@selection] = char_code.chr
    end

    # increment selection if user is within already-entered characters
    def next_input_character
        return if @selection > (@input_string_max_length - 1)
        return if @selection > (@input_string_chars_entered - 1)
        @selection += 1
    end

    def previous_input_character
        @selection -= 1 if @selection > 0
    end

    # @return true if this confirmation indicates the user has completed the string
    def confirm_input_character
        # Return true if we're confirming the last possible character
        # TODO: Allow 1 more character that can ONLY be ⏎ at the end
        return true if @selection == @input_string_max_length
        # Return true if the character being confirmed is ⏎
        # TODO: Only allow ↵ on the last character
        return true if @input_string[@selection] == '*'
        # Push the number of input chars if the current char is next char not already input
        if @selection == @input_string_chars_entered
            if @input_string_chars_entered < @input_string_max_length
                @input_string_chars_entered += 1 
                @input_string += 'a'
            end
        end
        # Push the cursor
        next_input_character
        false
    end

    # Cancel the current character
    # @return true if user is indicating cancelling of the entire string
    def cancel_input_character
        return true if @selection == 0
        # Delete the current character, and move selection back if we're now outside the string
        @input_string = @input_string[0...@selection] + @input_string[@selection+1..-1]
        @selection -= 1 if @selection == @input_string.length
        # Number of input characters is now one less
        @input_string_chars_entered -= 1
        false
    end

    def should_return_to_town?
        return false unless quest_goal
        quest_goal[:return_to_town]
    end

    def primitives_to_render a, outs
        res = []
        case @layout
        when :vertical
            # By default, buttons take up the entire panel
            width = @buttonw
            yoffset = 0
            # If there is a caption, adjust the width of the dialog, render the caption, and y offset the buttons to appear underneath
            if @caption
                cw, ch = a.gtk.calcstringbox(@caption, @text_size)
                width = [width, cw + 32].max
                # TODO: have a property for text margin instead of hardcoding 16 pixels
                yoffset = 32 + ch 
                res << { x: @x, y: @y - yoffset, w: width, h: yoffset, path: 'sprites/white_square.png', angle: 0, a: @bg_a, r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite }
                res << { x: @x + width / 2, y: @y, text: @caption, size_enum: @text_size, alignment_enum: 1, primitive_marker: :label } 
            end
            @options.keys.each_with_index do |o,i|
                res << { x: @x, y: @y - yoffset - (@buttonh * i), w: width, h: @buttonh, path: 'sprites/white_square.png', angle: 0, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
                res << { x: @x + @border_w, y: @y - (@buttonh * i) - yoffset + @border_w, w: width - (2 * @border_w), h: @buttonh - (2 * @border_w), path: 'sprites/white_square.png', angle: 0, a: @bg_a, r: @bg_r, b: @bg_g, g: @bg_b, primitive_marker: :sprite }
                if @focused && i == @selection
                    #a.state.logs << "Rendering selection" unless a.state.logs.entries.last == "Rendering selection"
                    res <<  { x: @x, y: @y - (@buttonh * i) - yoffset,  w: width, h: @buttonh, path: 'sprites/white_square.png', angle: 0, a: 32 * (a.state.tick_count % 8), r: 255, g: 255, b: 255, primitive_marker: :sprite }
                end
                w, h = a.gtk.calcstringbox(o.to_s, @text_size)
                res << { x: @x + width / 2, y: @y - (@buttonh * i) - yoffset + @buttonh / 2 + h / 2 - @border_w, text: o, size_enum: @text_size, alignment_enum: 1, primitive_marker: :label }
            end
        when :horizontal 
            # Rendering horizontal dialog panel

            # Calculate width and height
            cw, ch = a.gtk.calcstringbox(@caption, @text_size) if @caption
            w = @margin + (@buttonw + @margin) * @options.count 
            h = @buttonh + @margin * 2
            if @caption
                h += (ch + @margin)
                w = [w, @margin + cw + @margin].max 
            end

            # Render main panel
            res << { x: @x - @border_w, y: @y - @border_w, w: w + @border_w * 2, h: h + @border_w * 2, path: 'sprites/white_square.png', angle: 0, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
            res << { x: @x, y: @y, w: w, h: h, path: 'sprites/white_square.png', angle: 0, a: @bg_a, r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite }

            # Render caption
            res << { x: @x + w/2, y: @y + h - @margin, text: @caption, size_enum: @text_size, alignment_enum: 1, primitive_marker: :label } if @caption && @focused

            # Render buttons
            # The dialog is at least as wide as the number of options but will be wider if the caption is long.
            # Therefore the spacing per-option is:
            width = (w - @margin - @margin) / @options.count
            # The button should be centred in its space, therefore an x offset of:
            xoffset = (width - @buttonw) / 2
            @options.keys.each_with_index do |o,i|
                res << { x: @x + @margin + width * i + xoffset, y: @y + @margin, w: @buttonw, h: @buttonh, path: 'sprites/white_square.png', angle: 0, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
                res << { x: @x + @border_w + @margin + width * i + xoffset, y: @y + @margin + @border_w, w: @buttonw - (2 * @border_w), h: @buttonh - (2 * @border_w), path: 'sprites/white_square.png', angle: 0, a: @bg_a, r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite }
                if @focused && i == @selection
                    res << { x: @x + @margin + width * i + xoffset, y: @y + @margin, w: @buttonw, h: @buttonh, path: 'sprites/white_square.png', angle: 0, a: 32 * (a.state.tick_count % 8), r: 255, g: 255, b: 255, primitive_marker: :sprite }
                end
                lw, lh = a.gtk.calcstringbox(o, @text_size)
                res << { x: @x + @margin + width * i  + xoffset + @buttonw / 2, y: @y + @margin + @buttonh / 2 + lh / 2, text: o, size_enum: @text_size, alignment_enum: 1, primitive_marker: :label } if @focused
            end
        when :string_editor
            # Render caption
            res << { x: 500, y: 370, text: @caption, size_enum: 2, alignment_enum: 0, primitive_marker: :label}
            # Render a character block up to the max string length
            @input_string_max_length.times do |i|
                res << { x: 500 + 20 * i, y: 300, w: 20, h: 40, path: :pixel, a: @bg_a, r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite}
                if i < @input_string.length
                    # To keep code using ASCII, '*' is a special end-of-string character. Render as an arrow.
                    char = @input_string[i] == '*' ? '⏎' : @input_string[i]
                    res << { x: 500 + 20 * i, y: 330, text: char, size_enum: @text_size, alignment_enum: 0, primitive_marker: :label}
                end
            end
            res << { x: 500, y: 290, w: 20 * @input_string_max_length, h: 2, path: :pixel, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite}
            res << { x: 500, y: 340, w: 20 * @input_string_max_length, h: 2, path: :pixel, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite}
            # Render input arrows over the block for current selection
            res << { x: 500 + 20 * @selection, y: 300, w: 20, h: 40, path: :pixel, a: 32 * (a.state.tick_count % 8), r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite }
            res << { x: 500 + 20 * @selection, y: 300, text: "▲", r: 250, g: 250, b: 250 }
            res << { x: 500 + 20 * @selection, y: 350, text: "▼", r: 250, g: 250, b: 250 }

        when :panel
            case @type
            when :character_status_panel
                # TODO: make w and h params instead of hardcoding. Or this *might be* OK?
                w = 700
                h = 480
                cw, ch = a.gtk.calcstringbox(@caption, @text_size) if @caption
                 # Render main panel
                res << { x: @x - @border_w, y: @y - @border_w, w: w + @border_w * 2, h: h + @border_w * 2, path: 'sprites/white_square.png', angle: 0, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
                res << { x: @x, y: @y, w: w, h: h, path: 'sprites/white_square.png', angle: 0, a: @bg_a, r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite }

                # Render caption
                outs.labels << [@x + w/2, @y + h - @margin, @caption, @text_size, 1] if @caption

                character = a.state.player_party.members[a.state.selected_character_for_status]
                portrait_size = 100

                portrait_rt = case a.state.selected_character_for_status
                when 0
                    :portrait1
                when 1
                    :portrait2
                when 2
                    :portrait3
                when 3
                    :portrait4
                else
                    raise "Cannot render character panel, invalid character index #{a.state.selected_character_for_status}"
                end

                # Render portrait
                res << { x: @x + @margin + 20, y: @y + h - @margin - portrait_size - 40, w: portrait_size, h: portrait_size, path: portrait_rt, primitive_marker: :sprite }
                # Render HP, MP, level
                outs.labels << [ @x + portrait_size + @margin * 2 + 40, @y + h - @margin - 50, "Health #{character.hp}/#{character.max_hp}", @text_size, 0]
                outs.labels << [ @x + portrait_size + @margin * 2 + 40, @y + h - @margin - 80, "Magic #{character.mp}/#{character.max_mp}", @text_size, 0]
                outs.labels << [ @x + portrait_size + @margin * 2 + 40, @y + h - @margin - 110, "Level #{character.level} (Next:#{character.xp_next})", @text_size, 0]

                # Render equipped items and armour
                outs.labels << [ @x + portrait_size + @margin * 2 + 300, @y + h - @margin - 50, character.equipped_mainhand ? character.equipped_mainhand.name : "---", @text_size, 0]
                outs.labels << [ @x + portrait_size + @margin * 2 + 300, @y + h - @margin - 80, character.equipped_offhand ? character.equipped_offhand.name : "---", @text_size, 0]
                outs.labels << [ @x + portrait_size + @margin * 2 + 300, @y + h - @margin - 110, character.equipped_armour ? character.equipped_armour.name : "---", @text_size, 0]

                # Render stats
                character.stats.keys.each_with_index do |s, i|
                    outs.labels << [ @x + @margin + 20, @y + h - portrait_size - 50 - i * 20, "#{s.to_s.capitalize.ljust(14)} #{character.stats[s]}", @text_size - 1 ]
                end

                # Render skills
                selected_skill = nil
                character.skills.keys.each_with_index do |s, i|
                    outs.labels << [ @x + @margin + 280, @y + h - 150 - i * 18, "#{character.skills[s][:grade].to_s.capitalize} #{s.to_s.tr('_', ' ').capitalize.ljust(15)} #{character.skills[s][:level]}", @text_size - 3 ]
                    if i == @selection
                        selected_skill = s
                        res << { x: @x + @margin + 270, y: @y + h - 170 - i * 18, w: 260, h: 20, path: 'sprites/white_square.png', angle: 0, a: 32 * (a.state.tick_count % 8), r: 255, g: 255, b: 255, primitive_marker: :sprite }
                    end
                end

                # Render skill points
                skill_point_string = ""
                if character.vested_skill_points > 0
                    skill_point_string = "Skill points: #{character.vested_skill_points}"
                    skill_point_string += " (Rest to earn #{character.unvested_skill_points} more)" if character.unvested_skill_points > 0
                else
                    skill_point_string = "Rest to earn #{character.unvested_skill_points} skill points" if character.unvested_skill_points > 0
                end
                outs.labels << [ @x + @margin + 20, @y + 30, skill_point_string, @text_size - 3 ]
                upgrade_prompt = ""
                if character.can_afford_skill_increase?(selected_skill)
                    upgrade_prompt = "Increase #{selected_skill.to_s.capitalize} for #{character.cost_to_increase_skill(selected_skill)} skill points?"
                else
                    upgrade_prompt = "Need #{character.cost_to_increase_skill(selected_skill)} skill points to increase #{selected_skill.to_s.capitalize}"
                end
                outs.labels << [ @x + @margin + 20, @y + 52, upgrade_prompt, @text_size - 3 ]
            when :event_dialog
                w = 500
                h = 300
                cw, ch = a.gtk.calcstringbox(@caption, @text_size) if @caption
                 # Render main panel
                res << { x: @x - @border_w, y: @y - @border_w, w: w + @border_w * 2, h: h + @border_w * 2, path: 'sprites/white_square.png', angle: 0, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
                res << { x: @x, y: @y, w: w, h: h, path: 'sprites/white_square.png', angle: 0, a: @bg_a, r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite }

                # Render caption
                outs.labels << [@x + w/2, @y + h - @margin, @caption, @text_size, 1] if @caption

                # Render text
                outs.labels << @current_text.map_with_index do |s, i|
                    { x: @x + @margin + 30, y: @y + h - @margin - 40 - (i * 20), text: s }
                end
            when :innkeeper_chat, :quest_update_dialog
                w = 500
                h = 300
                cw, ch = a.gtk.calcstringbox(@caption, @text_size) if @caption
                 # Render main panel
                res << { x: @x - @border_w, y: @y - @border_w, w: w + @border_w * 2, h: h + @border_w * 2, path: 'sprites/white_square.png', angle: 0, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
                res << { x: @x, y: @y, w: w, h: h, path: 'sprites/white_square.png', angle: 0, a: @bg_a, r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite }

                # Render caption
                outs.labels << [@x + w/2, @y + h - @margin, @caption, @text_size, 1] if @caption

                # Render text
                outs.labels << @current_text.map_with_index do |s, i|
                    { x: @x + @margin + 30, y: @y + h - @margin - 40 - (i * 20), text: s }
                end
            when :inventory_panel, :shop_buy_panel, :shop_sell_panel
                # TODO: If scrollable list panels are all common code, they should be a :scrollable_list type and not caught as a specific panel type
                w = 700
                h = 450
                 # Render main panel
                res << { x: @x - @border_w, y: @y - @border_w, w: w + @border_w * 2, h: h + @border_w * 2, path: 'sprites/white_square.png', angle: 0, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
                res << { x: @x, y: @y, w: w, h: h, path: 'sprites/white_square.png', angle: 0, a: @bg_a, r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite }
                # Render list of items
                @list[@scroll_head...@scroll_head + @scroll_length].each_with_index do |x, i|
                    res << { x: @x + @margin, y: @y + h - i * 30, text: x.name, primitive_marker: :label }
                    if @type == :shop_buy_panel
                        res << { x: @x + w - 200, y: @y + h - 21 - i * 30, w: 24, h: 24, path: 'sprites/icons/silver.png', primitive_marker: :sprite }
                        res << { x: @x + w - 170, y: @y + h - i * 30, text: a.state.shop.buy_price(a, x), primitive_marker: :label }
                        res << { x: @x + w - 100, y: @y + h - 18 - i * 30, w: 16, h: 16, path: 'sprites/icons/weight.png', primitive_marker: :sprite }
                        res << { x: @x + w - 75, y: @y + h - i * 30, text: x.weight, primitive_marker: :label }
                    end
                    if @type == :inventory_panel || @type == :shop_sell_panel
                        res << { x: @x + w - 200, y: @y + h - 21 - i * 30, w: 24, h: 24, path: 'sprites/icons/silver.png', primitive_marker: :sprite }
                        res << { x: @x + w - 170, y: @y + h - i * 30, text: a.state.shop.sell_price(a, x), primitive_marker: :label }
                        res << { x: @x + w - 100, y: @y + h - 18 - i * 30, w: 16, h: 16, path: 'sprites/icons/weight.png', primitive_marker: :sprite }
                        res << { x: @x + w - 75, y: @y + h - i * 30, text: x.weight, primitive_marker: :label }
                    end
                    if @focused && i + @scroll_head == @selection
                        res << { x: @x + @margin, y: @y + h - 28 - i * 30, w: 655, h: 32, path: 'sprites/white_square.png', a: 32 * (a.state.tick_count % 8), r: 255, g: 255, b: 255, primitive_marker: :sprite }
                    end
                end

                # Render scroll bar
                # TODO: Colors need to be style parms
                if @list.any?
                    ## Render bar
                    res << { x: @x + w - 40, y: @y, w: 40, h: h, path: 'sprites/white_square.png', angle: 0, a: @bg_a, r: 0, g: 0, b: 0, primitive_marker: :sprite }
                    ## Render scroll tab
                    ## TODO: the tab is just proportional to the number of items. It should represent the proportion of the current "viewport" in the total list
                    scrolltab_y_offset = @list.empty? ? 0 : @selection * (h / @list.length)
                    scrolltab_height = @list.empty? ? h : h / @list.length
                    res << { x: @x + w - 40, y: @y + h - scrolltab_height - scrolltab_y_offset, w: 40, h: scrolltab_height, path: 'sprites/white_square.png', angle: 0, a: @bg_a, r: 255, g: 255, b: 255, primitive_marker: :sprite }
                end
                
                # If the list concerns player held items, display their silver and carry weight
                if [:inventory_panel, :shop_buy_panel, :shop_sell_panel].include? @type
                    # Draw a small sub-panel to ensure readability
                    res << { x: @x + w, y: @y + h - 82, w: 140 + @border_w * 2, h: 80 + @border_w * 2, path: 'sprites/white_square.png', angle: 0, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
                    res << { x: @x + w + @border_w, y: @y + h - 80, w: 140, h: 80, path: 'sprites/white_square.png', angle: 0, a: @bg_a, r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite }
                    res << { x: @x + w + 2 * @margin, y: @y + h - @margin - 28, w: 32, h: 32, path: 'sprites/icons/silver.png', primitive_marker: :sprite }
                    outs.labels << [ @x + w + 2 * @margin + 40, @y + h - @margin, a.state.player_party.silver]
                    res << { x: @x + w + 2 * @margin, y: @y + h - @margin - 68, w: 32, h: 32, path: 'sprites/icons/weight.png', primitive_marker: :sprite }
                    outs.labels << [ @x + w + 2 * @margin + 40, @y + h - @margin - 40, "#{a.state.player_party.carried_weight}/#{a.state.player_party.max_carry_weight}"]
                    # Draw a comparator panel
                    if [:weapon, :armour, :shield].include?(list[@selection].type) && a.state.player_party.members.any?
                        res << { x: @x + w, y: @y - 2, w: 120 + @border_w * 2, h: 150 + @border_w * 2, path: 'sprites/white_square.png', angle: 0, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
                        res << { x: @x + w + @border_w, y: @y, w: 120, h: 150, path: 'sprites/white_square.png', angle: 0, a: @bg_a, r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite }
                        a.state.player_party.members.each_with_index do |m, i|
                            rt = case i
                            when 0
                                :portrait1
                            when 1
                                :portrait2
                            when 2
                                :portrait3
                            when 3
                                :portrait4
                            end
                            res << { x: @x + w + @border_w + 2 * @margin, y: @y + 110 - i * (32 + @margin), w: 32, h: 32, path: rt, primitive_marker: :sprite }
                            compare = a.state.player_party.members[i].compare_equipment(list[@selection])
                            if compare
                                if compare > 0
                                    outs.labels << { x: @x + w + 2 * @margin + 40, y: @y + 130 - i * (32 + @margin), text: "▲▲", r: 80, g: 200, b: 80 }
                                elsif compare < 0
                                    outs.labels << { x: @x + w + 2 * @margin + 40, y: @y + 130 - i * (32 + @margin), text: "▼▼", r: 200, g: 80, b: 80 }
                                else
                                    outs.labels << { x: @x + w + 2 * @margin + 40, y: @y + 130 - i * (32 + @margin), text: "==", r: 80, g: 80, b: 200 }
                                end
                            else
                                # Cannot equip
                                outs.labels << { x: @x + w + 2 * @margin + 40, y: @y + 130 - i * (32 + @margin), text: " --", r: 120, g: 80, b: 80 }
                            end
                        end
                    end
                    # Draw an item detail panel
                    res.push(*draw_item_detail_panel(a, 400, 136, list[@selection]))
                end
            when :party_panel
                # TODO: make w and h params instead of hardcoding. Or this *might be* OK?
                w = 700
                h = 450
                cw, ch = a.gtk.calcstringbox(@caption, @text_size) if @caption
                 # Render main panel
                res << { x: @x - @border_w, y: @y - @border_w, w: w + @border_w * 2, h: h + @border_w * 2, path: 'sprites/white_square.png', angle: 0, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
                res << { x: @x, y: @y, w: w, h: h, path: 'sprites/white_square.png', angle: 0, a: @bg_a, r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite }

                # Render caption
                outs.labels << [@x + w/2, @y + h - @margin, @caption, @text_size, 1] if @caption

                # Render the party members
                a.state.player_party.members.each_with_index do |m, i|
                    # Character portrait
                    portrait_rt = case i
                    when 0
                        :portrait1
                    when 1
                        :portrait2
                    when 2
                        :portrait3
                    when 3
                        :portrait4
                    end
                    res << { x: @x + @margin + 60, y: @y + h - @margin - 60 - (84) * (i + 1), w: 80, h: 80, path: portrait_rt, primitive_marker: :sprite }

                    # Rank indicator
                    # TODO: Smaller text size here needs to be a style prop
                    res << { x: @x + @margin + 144, y: @y + h - @margin - (84) * (i + 1), w: 20, h: 20, path: 'sprites/white_square.png', r: 0, g: 0, b: 0, primitive_marker: :sprite }
                    if m.in_front_rank?
                        res << { x: @x + @margin + 144, y: @y + h - @margin - (84) * (i + 1), w: 20, h: 20, path: "sprites/icons/player_pegs_2_front.png", primitive_marker: :sprite }
                        outs.labels << [@x + @margin + 144, @y + h - @margin - (84) * (i + 1), "Front", -3, 0]
                    else
                        res << { x: @x + @margin + 144, y: @y + h - @margin - (84) * (i + 1), w: 20, h: 20, path: "sprites/icons/player_pegs_2_back.png", primitive_marker: :sprite }
                        outs.labels << [@x + @margin + 144, @y + h - @margin - (84) * (i + 1), "Rear", -3, 0]
                    end
                    outs.labels << [@x + @margin + 144, @y + h - @margin - 14 - (84) * (i + 1), "Rank", -3, 0]

                    # Basic details
                    outs.labels << [@x + @margin + 190, @y + h - @margin + 20 - (84) * (i + 1), "#{m.name}, level #{m.level} #{m.cclass_name}", @text_size, 0]

                    if @focused && i == a.state.selected_character_in_party
                        res << { x: @x + @margin + 142, y: @y + h - @margin - 60 - (84) * (i + 1), w: 556, h: 80, path: 'sprites/white_square.png', a: 4 * (a.state.tick_count % 32), r: 255, g: 255, b: 255, primitive_marker: :sprite }
                    end

                end
                # Input hint
                outs.labels << [@x + @margin + 4, @y + @margin + 18, "Up, Down: Select Character / Left: Move character up / Right: Move character down", -2, 0]
            when :quests_panel
                # TODO: make w and h params instead of hardcoding. Or this *might be* OK?
                w = 700
                h = 450
                cw, ch = a.gtk.calcstringbox(@caption, @text_size) if @caption
                 # Render main panel
                res << { x: @x - @border_w, y: @y - @border_w, w: w + @border_w * 2, h: h + @border_w * 2, path: 'sprites/white_square.png', angle: 0, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
                res << { x: @x, y: @y, w: w, h: h, path: 'sprites/white_square.png', angle: 0, a: @bg_a, r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite }

                # Render caption
                outs.labels << [@x + w/2, @y + h - @margin, @caption, @text_size, 1] if @caption

                if a.state.quests.current_quests.empty?
                    outs.labels << [@x + 240, @y + h / 2, "No Current Quests", @text_size, 1]
                else
                    a.state.quests.current_quests.each_with_index do |q,i|
                        outs.labels << [@x + 40, @y + h - 30 - (i * 65), q.title, @text_size, 0]
                        outs.labels << [@x + 40, @y + h - 60 - (i * 65), " - " + a.state.quests.current_goal_of_quest(q).desc, @text_size - 3, 0]
                    end
                end
            when :spell_list
                # TODO: make w and h params instead of hardcoding. Or this *might be* OK?
                w = 700
                h = 450

                character = a.state.player_party.members[a.state.selected_character_for_status]

                @caption = "Spells - " + character.name
                cw, ch = a.gtk.calcstringbox(@caption, @text_size) if @caption
                # Render main panel
                res << { x: @x - @border_w, y: @y - @border_w, w: w + @border_w * 2, h: h + @border_w * 2, path: 'sprites/white_square.png', angle: 0, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
                res << { x: @x, y: @y, w: w, h: h, path: 'sprites/white_square.png', angle: 0, a: @bg_a, r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite }

                # Render caption
                outs.labels << [@x + w/2, @y + h - @margin, @caption, @text_size, 1] if @caption

                
                if character.known_spells.empty?
                    outs.labels << [@x + w/2, @y + h/2 - @margin, "No spells known", @text_size, 1]
                else
                    character.known_spells.each_with_index do |s_id, i|
                        outs.labels << [@x + w/2, @y + h - @margin - 60 - 30 * i, a.state.spells.spell_definitions.select { |s| s.id == s_id }.first[:name], @text_size, 1]
                        if i == a.state.selected_spell && @focused
                            tint = a.state.spells.can_cast_from_spell_list?(s_id) ? 8 * (a.state.tick_count % 16) : 128
                            res << { x: @x + @margin*3, y: @y + h - @margin - 90 - 30 * i, w: w - @margin*6, h: 30, path: 'sprites/white_square.png', a: tint, r: 255, g: 255, b: 255, primitive_marker: :sprite }
                        end
                    end
                end
            when :equip_panel

                # TODO: make w and h params instead of hardcoding. Or this *might be* OK?
                w = 700
                h = 520

                character = a.state.player_party.members[a.state.chosen_character_to_equip]

                # Render main panel
                res << { x: @x - @border_w, y: @y - @border_w, w: w + @border_w * 2, h: h + @border_w * 2, path: 'sprites/white_square.png', angle: 0, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
                res << { x: @x, y: @y, w: w, h: h, path: 'sprites/white_square.png', angle: 0, a: @bg_a, r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite }

                if a.state.choosing_character_to_equip
                    res << { x: 500, y: 500, text: "Choose who to equip...", size_enum: 1, alignment_enum: 1, primitive_marker: :label }
                else
                    # Character name
                    res << { x: @x + 20, y: @y + h - 20, text: "#{character.name} the #{character.cclass_name}", size_enum: 1, alignment_enum: 0, primitive_marker: :label }

                    # Draw the doll
                    res << { x: 372, y: @y + 150, w: 256, h: 256, path: 'sprites/doll.png'}

                    # Draw item slots
                    ## Draw helmet label
                    res << { x: 500, y: 610, text: "Helmet", size_enum: -1, alignment_enum: 1, primitive_marker: :label }
                    ## Draw helmet slot
                    res.push(*draw_icon(a, 500, 568, 48, 500, 510, :helmet, character.equipped_helmet, @selected_slot == :helmet))
                    ## Draw amulet label
                    res << { x: 345, y: 560, text: "Amulet", size_enum: -1, alignment_enum: 1, primitive_marker: :label }
                    ## Draw amulet slot
                    res.push(*draw_icon(a, 346, 512, 48, 500, 480, :amulet, character.equipped_amulet, @selected_slot == :amulet))
                    ## Draw mainhand label
                    res << { x: 345, y: 486, text: "Main Hand", size_enum: -1, alignment_enum: 1, primitive_marker: :label }
                    ## Draw mainhand slot
                    hand_type = :junk
                    if character.equipped_mainhand
                        hand_type = character.equipped_mainhand.variant if character.equipped_mainhand.type == :weapon
                        hand_type = :shield if character.equipped_mainhand.type == :shield
                    end
                    res.push(*draw_icon(a, 346, 442, 48, 430, 430, hand_type, character.equipped_mainhand, @selected_slot == :mainhand))
                    ## Rings label
                    res << { x: 345, y: 406, text: "Rings", size_enum: -1, alignment_enum: 1, primitive_marker: :label }
                    ## Ring slots
                    res.push(*draw_icon(a, 320, 360, 48, 430, 430, :ring, character.equipped_ring1, @selected_slot == :ring1))
                    res.push(*draw_icon(a, 374, 360, 48, 430, 430, :ring, character.equipped_ring2, @selected_slot == :ring2))
                    res.push(*draw_icon(a, 320, 310, 48, 562, 430, :ring, character.equipped_ring3, @selected_slot == :ring3))
                    res.push(*draw_icon(a, 374, 310, 48, 562, 430, :ring, character.equipped_ring4, @selected_slot == :ring4))
                    ## Gauntlets label
                    res << { x: 660, y: 560, text: "Gauntlets", size_enum: -1, alignment_enum: 1, primitive_marker: :label }
                    ## Gauntlets slot
                    res.push(*draw_icon(a, 660, 512, 48, 562, 430, :gauntlets, character.equipped_gauntlets, @selected_slot == :gauntlets))
                    ## Offhand label
                    res << { x: 660, y: 486, text: "Off Hand", size_enum: -1, alignment_enum: 1, primitive_marker: :label }
                    ## Offhand slot
                    hand_type = :junk
                    if character.equipped_offhand
                        hand_type = character.equipped_offhand.variant if character.equipped_offhand.type == :weapon
                        hand_type = :shield if character.equipped_offhand.type == :shield
                    end
                    res.push(*draw_icon(a, 660, 442, 48, 562, 430, hand_type, character.equipped_offhand, @selected_slot == :offhand))
                    ## Armour label
                    res << { x: 660, y: 412, text: "Armour", size_enum: -1, alignment_enum: 1, primitive_marker: :label }
                    ## Armour slot
                    res.push(*draw_icon(a, 660, 366, 48, 500, 456, :armour, character.equipped_armour, @selected_slot == :armour))
                    ## Footwear label
                    res << { x: 660, y: 338, text: "Footwear", size_enum: -1, alignment_enum: 1, primitive_marker: :label }
                    ## Footwear slot
                    res.push(*draw_icon(a, 660, 292, 48, 532, 280, :footwear, character.equipped_footwear, @selected_slot == :footwear))

                    # Draw item detail panel
                    if item = character.item_at_slot(@selected_slot)
                        res.push(*draw_item_detail_panel(a, 400, 80, item))
                    end
                end
            when :guild_training_list
                # TODO: make w and h params instead of hardcoding. Or this *might be* OK?
                w = 700
                h = 520

                # Render main panel
                res << { x: @x - @border_w, y: @y - @border_w, w: w + @border_w * 2, h: h + @border_w * 2, path: 'sprites/white_square.png', angle: 0, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
                res << { x: @x, y: @y, w: w, h: h, path: 'sprites/white_square.png', angle: 0, a: @bg_a, r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite }
            
                if @options.length == 0
                    res << { x: @x + w/2, y: @y + h/2, text: "No training available!", size_enum: 2, alignment_enum: 1, primitive_marker: :label }
                else
                    @options.keys.each_with_index do |s,i|
                        unless s[:skill_level_met]
                            res << { x: @x + @margin + 150, y: @y + h - 184 - i * 40, w: 420, h: 30, path: 'sprites/white_square.png', angle: 0, a: 128, r: 255, g: 80, b: 80, primitive_marker: :sprite }
                        end
                        character = a.state.player_party.members[s[:character_idx]]
                        res << { x: @x + w/2, y: 500 - 40 * i, text: "Train #{character.name} to #{s[:next_grade].to_s.capitalize} #{s[:skill].to_s.capitalize}", size_enum: 1, alignment_enum: 1, primitive_marker: :label }
                        if i == @selection
                            res << { x: @x + @margin + 150, y: @y + h - 184 - i * 40, w: 420, h: 30, path: 'sprites/white_square.png', angle: 0, a: 4 * (a.state.tick_count % 32), r: 255, g: 255, b: 255, primitive_marker: :sprite }
                        end
                    end
                end
            when :party_creation_panel
                # TODO: make w and h params instead of hardcoding. Or this *might be* OK?
                w = 900
                h = 580

                # Render main panel
                res << { x: @x - @border_w, y: @y - @border_w, w: w + @border_w * 2, h: h + @border_w * 2, path: :pixel, angle: 0, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
                res << { x: @x, y: @y, w: w, h: h, path: :pixel, angle: 0, a: @bg_a, r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite }
 
                res << { x: 640, y: 620, text: "CREATE YOUR PARTY", size_enum: 5, alignment_enum: 1, primitive_marker: :label }
                # Render 4 character slots
                a.state.player_party.members.each_with_index do |c,i|
                    res << { x: @x + 80 - @border_w, y: @y + 380 - 100 * i - @border_w, w: w - 160 + @border_w * 2, h: 100 + @border_w * 2, path: :pixel, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
                    res << { x: @x + 80, y: @y + 380 - 100 * i, w: w - 160, h: 100, path: :pixel, angle: 0, a: @bg_a, r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite }
                    if c.nil?
                        res << { x: @x + 100, y: @y + 400 - 100 * i - @border_w, w: 64, h: 64, path: "sprites/icons/silhouette.png", a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
                        res << { x: 640, y: @y + 440 - 100 * i, text: "CREATE A CHARACTER", size_enum: 3, alignment_enum: 1, primitive_marker: :label }
                    else
                        portrait_rt = case i
                        when 0
                            :portrait1
                        when 1
                            :portrait2
                        when 2
                            :portrait3
                        when 3
                            :portrait4
                        end
                        res << { x: @x + 100, y: @y + 400 - 100 * i - @border_w, w: 64, h: 64, path: portrait_rt, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
                        res << { x: 640, y: @y + 440 - 100 * i, text: "#{a.state.player_party.members[i].name} the level #{a.state.player_party.members[i].level} #{a.state.player_party.members[i].cclass_name}", size_enum: 3, alignment_enum: 1, primitive_marker: :label }
                    end
                    if i == a.state.chosen_character_to_create && @focused && !a.state.confirming_party
                        res << { x: @x + 80 - @border_w, y: @y + 380 - 100 * i - @border_w, w: w - 160 + @border_w * 2, h: 100 + @border_w * 2, path: :pixel, a: 32 + 2 * a.tick_count % 64, r: 255, g: 255, b: 255, primitive_marker: :sprite }
                    end
                end
                # Render the Start button
                if a.state.player_party.ready?
                    res << { x: 640 - 80 - @border_w, y: 100 - @border_w, w: 160 + @border_w * 2, h: 50 + @border_w * 2, path: :pixel, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
                    res << { x: 640 - 80, y: 100, w: 160, h: 50, path: :pixel, angle: 0, a: @bg_a, r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite }
                    res << { x: 640, y: 140, text: "BEGIN!", size_enum: 3, alignment_enum: 1, primitive_marker: :label }
                    if a.state.confirming_party
                        res << { x: 640 - 80 - @border_w, y: 100 - @border_w, w: 160 + @border_w * 2, h: 50 + @border_w * 2, path: :pixel, a: 2 + 2 * a.tick_count % 64, r: 255, g: 255, b: 255, primitive_marker: :sprite }
                    end
                end
            when :character_edit_panel
                # TODO: make w and h params instead of hardcoding. Or this *might be* OK?
                w = 900
                h = 580

                # Render main panel
                res << { x: @x - @border_w, y: @y - @border_w, w: w + @border_w * 2, h: h + @border_w * 2, path: :pixel, angle: 0, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
                res << { x: @x, y: @y, w: w, h: h, path: :pixel, angle: 0, a: @bg_a, r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite }

                # Render name
                name_label = "Name:             " + (a.state.creating_character_name.nil? ? "?????" : a.state.creating_character_name)
                res << { x: 220, y: 600, text: name_label, size_enum: 2, alignment_enum: 0, primitive_marker: :label }
                # Highlight name if it is the selected field
                if @focused && @options.keys[@selection] == "Edit Name"
                    res << { x: 220 - @border_w, y: 576 - @border_w, w: 400 + @border_w * 2, h: 30 + @border_w * 2, path: :pixel, a: 32 + 2 * a.tick_count % 64, r: 255, g: 255, b: 255, primitive_marker: :sprite }
                end

                # Render class
                class_label = "Class:            " + (a.state.creating_character_class.nil? ? "?????" : a.state.creating_character_class.to_s.capitalize)
                res << { x: 220, y: 520, text: class_label, size_enum: 2, alignment_enum: 0, primitive_marker: :label }
                # Highlight class if it is the selected field
                if @focused && @options.keys[@selection] == "Choose Class"
                    res << { x: 220 - @border_w, y: 496 - @border_w, w: 400 + @border_w * 2, h: 30 + @border_w * 2, path: :pixel, a: 32 + 2 * a.tick_count % 64, r: 255, g: 255, b: 255, primitive_marker: :sprite }
                end

                # Render portrait
                res << { x: 220, y: 420, text: "Appearance:", size_enum: 2, alignment_enum: 0, primitive_marker: :label }
                res << { x: 500, y: 300, w: 128, h: 128, path: ("portrait" + (a.state.chosen_character_to_create + 1).to_s).to_sym, primitive_marker: :sprite}
                # Highlight portrait if it is the selected field
                if @focused && @options.keys[@selection] == "Edit Appearance"
                    res << { x: 220 - @border_w, y: 394 - @border_w, w: 200 + @border_w * 2, h: 30 + @border_w * 2, path: :pixel, a: 32 + 2 * a.tick_count % 64, r: 255, g: 255, b: 255, primitive_marker: :sprite }
                end

                # Render confirm button (disabled if name or class not selected)
                if ready? a
                    res << { x: 450 - @border_w, y: 200 - @border_w, w: 300 + @border_w * 2, h: 60 + @border_w * 2, path: :pixel, angle: 0, a: @border_a, r: @border_r, g: @border_g, b: @border_b, primitive_marker: :sprite }
                    res << { x: 450, y: 200, w: 300, h: 60, path: :pixel, angle: 0, a: @bg_a, r: @bg_r, g: @bg_g, b: @bg_b, primitive_marker: :sprite }
                    res << { x: 600, y: 245, text: "Ready!", size_enum: 3, alignment_enum: 1, primitive_marker: :label }
                    if @options.keys[@selection] == "Ready!"
                        res << { x: 450 - @border_w, y: 200 - @border_w, w: 300 + @border_w * 2, h: 60 + @border_w * 2, path: :pixel, a: 32 + 2 * a.tick_count % 64, r: 255, g: 255, b: 255, primitive_marker: :sprite }
                    end
                end
            end
        else
            a.gtk.log_error "Unhandled dialog type to render: #{@layout.to_s}"
        end
        res
    end

    # Draw an icon on the screen
    # @param  a         DR args
    # @param  cx        Numeric Centre X of the icon
    # @param  cy        Numeric Centre Y of the icon
    # @param  size      Numeric Size (both width and height) of the icon
    # @param  sx        Numeric Source X of where this icon refers to (-1 for n/a. Also n/a if `selected` is false)
    # @param  sy        Numeric Source Y of where this icon refers to (-1 for n/a. Also n/a if `selected` is false)
    # @param  type      Icon    type e.g Sword, fireball spell
    # @param  populated boolean true if this slot has an item, false if empty
    # @param  selected  boolean true if this slot is currently selected
    # @return Array of primitives to send to args.outputs.primitives
    def draw_icon a, cx, cy, size, sx, sy, type, populated = false, selected = false
        res = []

        # Highlighting (if highlighted)
        if selected
            res << { x: cx - size/2 - 3, y: cy - size/2 - 3, w: size + 6, h: size + 6, r: 200, g: 200, b: 200, path: :sprite, primitive_marker: :border }
            res << { x: cx - size/2 - 2, y: cy - size/2 - 2, w: size + 4, h: size + 4, r: 200, g: 200, b: 200, path: :sprite, primitive_marker: :border }
            res << { x: cx, y: cy, x2: sx, y2: sy, r: 200, g: 200, b: 200, path: :sprite, primitive_marker: :line }
        end

        # Background
        res << { x: cx - size/2, y: cy - size/2, w: size, h: size, r: 0, g: 0, b: 0, path: :pixel, primitive_marker: :sprite }

        return res unless populated
        # Icon (if populated)
        res << { x: cx - size/2, y: cy - size/2, w: size, h: size, path: "sprites/icons/background.png", primitive_marker: :sprite }
        icon = case type
        when :ring1, :ring2, :ring3, :ring4
            "ring"
        else
            type.to_s
        end
        res << { x: cx - size/2, y: cy - size/2, w: size, h: size, path: "sprites/icons/item_#{icon}.png", primitive_marker: :sprite }
        
        res
    end

    def draw_item_detail_panel a, cx, cy, item
        res = []
        # Draw main panel
        ## TODO: Pass styles including colour from main panel 
        res << { x: cx - 100, y: cy - 40, w: 400, h: 100, r: 80, g: 80, b: 80, path: :pixel, primitive_marker: :sprite }
        # Draw icon
        type = item.type == :weapon ? item.variant : item.type
        res.push(*draw_icon(a, cx - 60, cy + 10, 48, -1, -1, type, true, false))
        # Draw item name
        res << { x: cx + 20, y: cy + 58, text: item.name, size_enum: 0, alignment_enum: 0, primitive_marker: :label }
        # Draw stats
        basic_detail = case item.type
        when :weapon
            "#{item.power} attack"
        when :armour, :shield
            "#{item.power} defense"
        else
            ""
        end
        res << { x: cx + 20, y: cy + 30, text: basic_detail, size_enum: -1, alignment_enum: 0, primitive_marker: :label }

        res
    end

    def serialize
        {}
    end

    def inspect
        self.to_s
    end

    def to_s
        "ButtonGroup"
    end

end