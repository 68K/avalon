#!/bin/bash
./dragonruby mygame --eval tests/tests.rb --no-tick
if [ -f 'mygame/TEST_FAILURES' ]; then
    cat 'mygame/TEST_FAILURES'
    exit 1
else
    echo "All good!"
fi