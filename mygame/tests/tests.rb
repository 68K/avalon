
def test_character args, assert
    require 'app/character.rb'
    require 'app/classes.rb'
    require 'app/item_generator.rb'

    classes = ClassDefinitions.new()
    jimmy = Character.new("Jimmy", classes.classes[:cutpurse])
    assert.true! jimmy.name == "Jimmy", "Jimmy isn't named Jimmy"
    assert.true! jimmy.cclass_name == "Cutpurse", "Jimmy's character class is not named Cutpurse"

    items = ItemGenerator.new()
    basic_sword = items.generate_weapon(1, :sword)
    jimmy.equip(basic_sword, :mainhand)
    assert.false! jimmy.equipped_mainhand.nil?, "Jimmy is empty handed after attempting to equip a sword"
    assert.true! jimmy.equipped_mainhand.type == :weapon, "Jimmy is not holding the weapon he was equipped"
    assert.true! jimmy.equipped_mainhand.variant == :sword, "The weapon Jimmy is holding is not a sword"

    better_sword = items.generate_weapon(2, :sword)
    assert.true! jimmy.compare_equipment(better_sword) > 0, "Better sword for Jimmy did not have better comparison"

    spear = items.generate_weapon(1, :long_arms)
    assert.false! jimmy.can_equip?(spear), "Jimmy should not be able to equip a spear"
end

def test_dungeon_map_generator args, assert
    require 'app/dungeon_map_generator.rb'

    # Dungeon generator needs to consider re-generating an existing dungeon that already has chests opened
    args.state.opened_chests = []

    dmg = DungeonMapGenerator.new(5)
    dungeon_floor = dmg.generate_dungeon_floor(args)
    assert.true! dungeon_floor.is_a?(DungeonMap), "Generated object was not a dungeon map"
    assert.true! dungeon_floor.tiles.is_a?(Array), "Generated dungeon map does not have an Array for tiles"
    assert.true! dungeon_floor.width > 0, "Generated dungeon map has 0 width"
    assert.true! dungeon_floor.height > 0, "Generated dungeon map has 0 height"
    assert.true! dungeon_floor.tiles.length == dungeon_floor.height, "Generated tile array height '#{dungeon_floor.tiles.length}' is not the DungeonMap height '#{dungeon_floor.height}'"
    assert.true! dungeon_floor.tiles[0].length == dungeon_floor.width, "Generated tile array width '#{dungeon_floor.tiles[0].length}' is not the DungeonMap width '#{dungeon_floor.width}'"
    assert.true! dungeon_floor.points_of_interest.length > 0, "Generated dungeon floor has no points of interest"
    assert.true! dungeon_floor.points_of_interest[0].has_key?(:x), "First generated point of interest has no :x"
    assert.true! dungeon_floor.points_of_interest[0].x.is_a?(Numeric), "First generated point of interest has non-Numeric :x #{dungeon_floor.points_of_interest[0].x}"
    assert.true! dungeon_floor.points_of_interest[0].has_key?(:y), "First generated point of interest has no :y"
    assert.true! dungeon_floor.points_of_interest[0].y.is_a?(Numeric), "First generated point of interest has non-Numeric :y #{dungeon_floor.points_of_interest[0].y}"
    assert.true! dungeon_floor.points_of_interest[0].has_key?(:type), "First generated point of interest has no :type"
    assert.false! dungeon_floor.points_of_interest[0].nil?, "First generated point of interest has nil :type"
    puts "test_dungeon_map_generator succeeded!"
end

def test_dungeon_floor_raycast args, assert
    require 'app/dungeon_map_generator.rb'

    # Dungeon generator needs to consider re-generating an existing dungeon that already has chests opened
    args.state.opened_chests = []

    dmg = DungeonMapGenerator.new(5)
    dungeon_floor = dmg.generate_dungeon_floor(args)
    assert.true! dungeon_floor.is_a?(DungeonMap), "Generated object was not a dungeon map"
    rays = dungeon_floor.get_raycast_sprites(args)
    assert.true! rays.is_a?(Array), "Result of get_raycast_sprites was not an array"
    assert.true! rays.any?, "Result of get_raycast_sprites was an empty array"
    assert.true! rays.reject { |r| r.has_key?(:x) }.empty?, "Not all ray sprites have an :x property"
    rays.each_with_index do |r, i|
        assert.true! r[:x] >= 0, "Ray sprite #{i} has a negative :x #{r[:x]}"
    end
end

def test_shop args, assert
    require 'app/item_generator.rb'
    require 'app/controllers/shop_controller.rb'

    args.state.item_generator = ItemGenerator.new()
    shop = ShopController.new(args.state)

    assert.true! shop.inventory.length > 0, "Shop has 0 inventory"
    shop.inventory.each do |i|
        assert.true! i.is_a?(Item), "Inventory entry #{i} is not an Item"
    end
end

def test_spells args, assert
    require 'app/controllers/magic_controller.rb'

    spells = MagicController.new()
    assert.true! spells.spell_definitions.length > 0, 'MagicController initialised with 0 spells'
    spell_ids = spells.spell_definitions.map { |s| s[:id] }
    assert.true! spell_ids.length == spell_ids.uniq.length, "MagicController spell IDs #{spell_ids} are not all unique"
    spells.spell_definitions.each do |s|
        [:id, :name, :requirements, :target, :cost].each { |k| assert.true! s.has_key?(k), "Spell #{s} is missing key #{k}" }
        assert.true! s[:requirements].is_a?(Hash), "Spell #{s} requirements are not a Hash"
        assert.true! [:ally, :enemy, :world].include?(s[:target]), "Spell #{s} does not have a valid targeting type"
    end
end

def test_portraits args, assert
    require 'app/controllers/portrait_controller.rb'
    portraits = PortraitController.new(args)

    appearance = portraits.random_appearance()
    assert.true! appearance.is_a?(Hash), "random_appearance did not return a Hash, got: #{appearance}"

    (0..3).each do |i|
        portraits.render_portrait(i, appearance)
        rt = case i
        when 0
            :portrait1
        when 1
            :portrait2
        when 2
            :portrait3
        when 3
            :portrait4
        end
        assert.true! args.render_target(rt).sprites.length > 0
    end
end

def test_audio_controller args, assert
    require 'app/controllers/audio_controller.rb'
    audio = AudioController.new(args)
    audio.play_dungeon_ambience
    assert.true! args.audio.has_key?(:music_dungeon_ambience), "Audio does not have the expected dungeon ambience track"
    assert.true! args.audio[:music_dungeon_ambience][:gain] == 1.0, "Audio does not have expected gain 1.0"

    audio.toggle_mute
    assert.true! args.audio.has_key?(:music_dungeon_ambience), "Audio still has expected dungeon ambience track"
    assert.true! args.audio[:music_dungeon_ambience][:gain] == 0.0, "Audio does not have expected gain 0.0 after muting"

    audio.toggle_mute
    assert.true! args.audio.has_key?(:music_dungeon_ambience), "Audio still has expected dungeon ambience track"
    assert.true! args.audio[:music_dungeon_ambience][:gain] == 1.0, "Audio does not have expected gain 1.0 after unmuting"
end

def test_calendar args, assert
    #require 'app/controllers/calendar_controller.rb'
    calendar = CalendarController.new()
    assert.true! calendar.day_of_week_string == "Albu", "Day of week should be Albu not #{calendar.day_of_week_string}"
    calendar.set_to_game_start_date
    assert.true! calendar.year == 327, "Game should start at year 327 not #{calendar.year}"
    assert.true! calendar.day_of_week_string == "Ganf", "Game should start at day of week Ganf not #{calendar.day_of_week_string}"
    calendar.add_days(1)
    assert.true! calendar.day_of_week_string == "Roechs", "After adding 1 day, day of week should be Roechs not #{calendar.day_of_week_string}"
    calendar.add_hours(24)
    assert.true! calendar.day_of_week_string == "Albu", "After adding 24 hours, day of week should be Albu not #{calendar.day_of_week_string}"
    assert.true! calendar.minutes == 30, "Game should start at 30 minutes past the hour, not #{calendar.minutes}"
    current_hour = calendar.hours
    calendar.add_minutes(20)
    assert.true! calendar.minutes == 50, "After adding 20 minutes, time should be 50 past the hour not #{calendar.minutes}"
    assert.true! calendar.hours == current_hour, "Hour should not have incremented"
    calendar.add_minutes(20)
    assert.true! calendar.minutes == 10, "After adding 20 more minutes, time should be 10 past the hour not #{calendar.minutes}"
    assert.true! calendar.hours == (current_hour + 1) % 24, "Hour should have incremented 1 past #{current_hour} but is #{calendar.hours}"
    current_minute = calendar.minutes
    assert.true! calendar.seconds == 30, "Game should start at 30 seconds past the minute, not #{calendar.seconds}"
    calendar.add_seconds(20)
    assert.true! calendar.seconds == 50, "After adding 20 seconds, time should be at 50 seconds past the minute, not #{calendar.seconds}"
    assert.true! calendar.minutes == current_minute, "After adding 20 seconds, minute should still be #{current_minute}, not #{calendar.minutes}"
    calendar.add_seconds(20)
    assert.true! calendar.seconds == 10, "After adding 20 more seconds, time should be at 10 seconds past the minute, not #{calendar.seconds}"
    assert.true! calendar.minutes == (current_minute + 1) % 60, "After adding 20 more seconds, minute should have incremented 1 past #{current_minute}, not #{calendar.minutes}"
    assert.true! calendar.month == 3, "Game starts at month 3, not #{calendar.month}"
    calendar.add_days(20)
    assert.true! calendar.month == 4, "Month should have incremented to 4 but is #{calendar.month}"
    calendar.add_months(5)
    assert.true! calendar.month == 9, "Month should have increased to 9 but is #{calendar.month}"
    assert.true! calendar.year == 327, "Calendar should still be at year 327 not #{calendar.year}"
    calendar.add_months(1)
    assert.true! calendar.month == 0, "Month should have rolled over to 0 but is #{calendar.month}"
    assert.true! calendar.year == 328, "Year should have rolled over to 328 but is  #{calendar.year}"
end

def test_name_generator args, assert
    require 'app/name_generator.rb'
    name_generator = NameGenerator.new()
    gv = name_generator.goblinspeak_syllables.validate_table
    assert.true! gv == "", "Goblinspeak syllable table has issue: #{gv}"
    100.times do |_|
        g = name_generator.goblin_name
        assert.true! g.length > 2, "Goblin name shorter than 2 characters: #{g}"
    end
    gv = name_generator.basicspeak_syllables.validate_table
    assert.true! gv == "", "Basicspeak syllable table has issue: #{gv}"
    100.times do |_|
        g = name_generator.basic_name
        assert.true! g.length > 2, "Basicspeak name shorter than 2 characters: #{g}"
    end
    gv = name_generator.commonspeak_syllables.validate_table
    assert.true! gv == "", "Commonspeak syllable table has issue: #{gv}"
    100.times do |_|
        g = name_generator.common_name
        assert.true! g.length > 2, "Commonspeak name shorter than 2 characters: #{g}"
    end
end

puts "running tests"
$gtk.delete_file_if_exist "TEST_FAILURES"
$gtk.reset 100
$gtk.log_level = :on
$gtk.tests.start
unless $gtk.tests.failed.empty?
    $gtk.write_file "TEST_FAILURES", $gtk.tests.failed.to_s
    exit(1)
end